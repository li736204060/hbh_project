package com.lzx.hbh_system.controller.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.UserstatsInfoFilter;
import com.lzx.hbh_system.service.user.UserstatsInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userstatsinfo")
public class UserStatsInfoController {
    @Autowired
    private UserstatsInfoService userstatsInfoService;
    @PostMapping("/getstatsinfoFilter")
    public ResponseView getstatsinfoFilter(@RequestBody RequestView requestView){
        // 启用请求头携带的请求模式
        String queryType = requestView.getReqInMsgHeader().getReqModles();
        //转对象
        UserstatsInfoFilter userstatsInfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UserstatsInfoFilter.class);
        return userstatsInfoService.getStatsinfo(queryType,userstatsInfoFilter);
    }
    // 数据统计 通过‘监听’或者在相应逻辑实现时‘注入’并实现数据的添加
    // 方案1 通过AOP的方式进行前后环绕监听 √
    // 方案2 传统手动注入方式

}
