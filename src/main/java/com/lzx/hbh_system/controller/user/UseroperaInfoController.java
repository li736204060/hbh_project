package com.lzx.hbh_system.controller.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.UserinfoFilter;
import com.lzx.hbh_system.bo.filter.UseroperaInfoFilter;
import com.lzx.hbh_system.service.user.UseroperaInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/useroperainfo")
public class UseroperaInfoController {
    @Autowired
    private UseroperaInfoService useroperaInfoService;
    @PostMapping("/getList")
    public ResponseView getList(@RequestBody RequestView requestView){
        //转对象
        UseroperaInfoFilter useroperaInfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseroperaInfoFilter.class);
        return useroperaInfoService.getSomeListByFilter(useroperaInfoFilter);
    }
}
