package com.lzx.hbh_system.controller.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.UseracivityInfoFilter;
import com.lzx.hbh_system.log.SysLog;
import com.lzx.hbh_system.service.user.UseracivityInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/useractivityinfo")
public class UseractivityInfoController {
    @Autowired
    private UseracivityInfoService useracivityInfoService;
    @PostMapping("/getAllActivityinfoPage")
    public ResponseView getAllPage(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.queryAllByFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-留言板信息增加")
    @PostMapping("/addOneUserActivityinfo")
    public ResponseView addOne(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.addOneByFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-留言板信息修改")
    @PostMapping("/modifyOneUserActivityinfo")
    public ResponseView modifyOne(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.modifyByFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-留言板信息删除")
    @PostMapping("/dropOneUserActivityinfo")
    public ResponseView dropOne(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.deleteByFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-留言板信息置顶操作")
    @PostMapping("/setTopOneUserActivityinfo")
    public ResponseView setTopOne(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.handleByFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-留言板信息取消置顶操作")
    @PostMapping("/setUnTopOneUserActivityinfo")
    public ResponseView setUnTopOne(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.handleByFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-留言板信息喜欢操作")
    @PostMapping("/setLikeOneUserActivityinfo")
    public ResponseView setLikeOne(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.handleByFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-留言板信息不喜欢操作")
    @PostMapping("/setdisLikeOneUserActivityinfo")
    public ResponseView setdisLikeOne(@RequestBody RequestView requestView){
        //转对象
        UseracivityInfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UseracivityInfoFilter.class);
        return useracivityInfoService.handleByFilter(userinfoFilter);
    }
}
