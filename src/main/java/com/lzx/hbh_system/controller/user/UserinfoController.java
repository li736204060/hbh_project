package com.lzx.hbh_system.controller.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.UserinfoFilter;
import com.lzx.hbh_system.log.SysLog;
import com.lzx.hbh_system.service.user.UserinfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userinfo")
public class UserinfoController {
    @Autowired
    private UserinfoService userinfoService;

    @PostMapping("/getAll")
    public ResponseView getAll(@RequestBody  RequestView requestView){
        //转对象
        UserinfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UserinfoFilter.class);
        return userinfoService.getAllUserinfoFilter(userinfoFilter);
    }
    @PostMapping("/getOne")
    public ResponseView getOne(@RequestBody RequestView requestView){
        //转对象
        UserinfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UserinfoFilter.class);
        return userinfoService.getOneUserinfoFilter(userinfoFilter);
    }
    @PostMapping("/uploadUserAvatarInfo")
    public ResponseView uploadUserAvatarnfo(@RequestBody RequestView requestView){
        //转对象
        UserinfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UserinfoFilter.class);
        return userinfoService.modifyOneUserAvartarInfoFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-用户信息新增")
    @PostMapping("/addOne")
    public ResponseView addOne(@RequestBody RequestView requestView){
        //转对象
        UserinfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UserinfoFilter.class);
        return userinfoService.addOneUserinfoFilter(userinfoFilter);
    }
    @SysLog(desc = "用户模块-用户信息修改")
    @PostMapping("/modifyOne")
    public ResponseView modifyOne(@RequestBody RequestView requestView){
        //转对象
        UserinfoFilter userinfoFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UserinfoFilter.class);
        return userinfoService.modifyOneUserinfoFilter(userinfoFilter);
    }
}

