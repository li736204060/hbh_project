package com.lzx.hbh_system.controller.yytygbmgl.bmgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentInfoFilter;
import com.lzx.hbh_system.service.yytygbmgl.DepartmentInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bmxxgl")
public class DepartmentInfoController {
    @Autowired
    private DepartmentInfoService departmentInfoService;
    // bmxxgl
    @PostMapping("/getALLDepInfoPageFilter")
    public ResponseView getALLDepInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentInfoFilter.class);
        return departmentInfoService.queryAllDepPageFilter(filter);
    }
    @GetMapping("/getALLDepInfo")
    public ResponseView getALLDepInfoPageFilter() {
        return departmentInfoService.queryAllDep();
    }
    @PostMapping("/dropBatchDepInfoFilter")
    public ResponseView dropBatchDepInfoFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentInfoFilter.class);
        return departmentInfoService.dropBatchDepFilter(filter);
    }
    @PostMapping("/addOneDepInfoFilter")
    public ResponseView addOneDepInfoFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentInfoFilter.class);
        return departmentInfoService.addOneDepFilter(filter);
    }
    @PostMapping("/modifyOneDepInfoFilter")
    public ResponseView modifyOneDepInfoFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentInfoFilter.class);
        return departmentInfoService.modifyOneDepFilter(filter);
    }

}
