package com.lzx.hbh_system.controller.yytygbmgl.qjgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.yytygbmgl.VacationInfoFilter;
import com.lzx.hbh_system.service.yytygbmgl.VacationInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/vacationinfo")
public class VacationInfoController {
    @Autowired
    private VacationInfoService vacationInfoService;
    @PostMapping("/getAllVacationInfoPageFilter")
    public ResponseView getAllVacationInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        VacationInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VacationInfoFilter.class);
        return vacationInfoService.queryAllVacationInfoPageFilter(filter);
    }
    @GetMapping("/getAllVacationInfo")
    public ResponseView getAllVacationInfo(){
        return vacationInfoService.queryAllVacationInfo();
    }
    @PostMapping("/getOneVacationInfo")
    public ResponseView getOneVacationInfo(@RequestBody RequestView requestView){
        //转对象
        VacationInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VacationInfoFilter.class);
        return vacationInfoService.queryOneVavationInfo(filter);
    }
    @PostMapping("/addOneVacationInfoPageFilter")
    public ResponseView addOneVacationInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        VacationInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VacationInfoFilter.class);
        return vacationInfoService.addOneVacationInfoFilter(filter);
    }
    @PostMapping("/modifyOneVacationInfoPageFilter")
    public ResponseView modifyOneVacationInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        VacationInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VacationInfoFilter.class);
        return vacationInfoService.modifyOneVacationInfoFilter(filter);
    }
    @PostMapping("/dropBatchVacationInfoPageFilter")
    public ResponseView dropBatchVacationInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        VacationInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VacationInfoFilter.class);
        return vacationInfoService.dropBatchVacationInfoFilter(filter);
    }
}
