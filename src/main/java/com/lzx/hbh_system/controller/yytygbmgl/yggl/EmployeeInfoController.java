package com.lzx.hbh_system.controller.yytygbmgl.yggl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeInfoFilter;
import com.lzx.hbh_system.service.yytygbmgl.EmployeeInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ygxxgl")
public class EmployeeInfoController {
    @Autowired
    private EmployeeInfoService employeeInfoService;
    @PostMapping("/getAllEmpInfoPageFilter")
    public ResponseView getAllEmpInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeInfoFilter.class);
        return employeeInfoService.queryAllEmpInfoPageFilter(filter);
    }
    @GetMapping("/getAllEmpInfoList")
    public ResponseView getAllEmpInfoList(){
        return employeeInfoService.queryAllEmpInfo();
    }
    @PostMapping("/addOneEmpInfoFilter")
    public ResponseView addOneEmpInfoFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeInfoFilter.class);
        return employeeInfoService.addOneEmpInfoFilter(filter);
    }
    @PostMapping("/modifyOneEmpInfoFilter")
    public ResponseView modifyOneEmpInfoFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeInfoFilter.class);
        return employeeInfoService.modifyOneEmpInfoFilter(filter);
    }
    @PostMapping("/dropBatchEmpInfoFilter")
    public ResponseView dropBatchEmpInfoFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeInfoFilter.class);
        return employeeInfoService.dropBatchEmpInfoFilter(filter);
    }
}
