package com.lzx.hbh_system.controller.yytygbmgl.bmgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentInfoFilter;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentTypeFilter;
import com.lzx.hbh_system.service.yytygbmgl.DepartmentTypeService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bmlxgl")
public class DepartmentTypeController {
    @Autowired
    private DepartmentTypeService departmentTypeService;
    @PostMapping("/queryAllDepTypePageFilter")
    public ResponseView queryAllDepTypePageFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentTypeFilter.class);
        return departmentTypeService.queryAllDepTypePageFilter(filter);
    }
    @GetMapping("/queryAllDepType")
    public ResponseView queryAllDepTypeFilter(){
        return departmentTypeService.queryAllDepType();
    }
    @PostMapping("/addOneDepTypeFilter")
    public ResponseView addOneDepTypeFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentTypeFilter.class);
        return departmentTypeService.addOneDepTypeFilter(filter);
    }
    @PostMapping("/dropBatchDepTypeFilter")
    public ResponseView dropBatchDepTypeFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentTypeFilter.class);
        return departmentTypeService.dropBatchDepTypeFilter(filter);
    }
    @PostMapping("/modifyOneDepTypeFilter")
    public ResponseView modifyOneDepTypeFilter(@RequestBody RequestView requestView){
        //转对象
        DepartmentTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), DepartmentTypeFilter.class);
        return departmentTypeService.modifyOneDepTypeFilter(filter);
    }
}
