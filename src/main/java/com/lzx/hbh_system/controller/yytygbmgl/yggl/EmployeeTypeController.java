package com.lzx.hbh_system.controller.yytygbmgl.yggl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeTypeFilter;
import com.lzx.hbh_system.service.yytygbmgl.EmployeeTypeService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/yglxgl")
public class EmployeeTypeController {
    @Autowired
    private EmployeeTypeService employeeTypeService;
    @PostMapping("/queryAllEmpTypePageFilter")
    public ResponseView queryAllEmpTypePageFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeTypeFilter.class);
        return employeeTypeService.queryAllEmpTypePageFilter(filter);
    }
    @GetMapping("/queryAllEmpType")
    public ResponseView addOneEmpTypeFilter(){
        return employeeTypeService.queryAllEmpType();
    }
    @PostMapping("/addOneEmpTypeFilter")
    public ResponseView addOneEmpTypeFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeTypeFilter.class);
        return employeeTypeService.addOneEmpTypeFilter(filter);
    }
    @PostMapping("/modifyOneEmpTypeFilter")
    public ResponseView modifyOneEmpTypeFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeTypeFilter.class);
        return employeeTypeService.modifyOneEmpTypeFilter(filter);
    }
    @PostMapping("/dropBatchEmpTypeFilter")
    public ResponseView dropBatchEmpTypeFilter(@RequestBody RequestView requestView){
        //转对象
        EmployeeTypeFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), EmployeeTypeFilter.class);
        return employeeTypeService.dropBatchEmpTypeFilter(filter);
    }
}
