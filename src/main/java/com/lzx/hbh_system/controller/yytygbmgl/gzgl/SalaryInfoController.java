package com.lzx.hbh_system.controller.yytygbmgl.gzgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.yytygbmgl.AttendenceInfoFilter;
import com.lzx.hbh_system.bo.filter.yytygbmgl.SalaryInfoFilter;
import com.lzx.hbh_system.service.yytygbmgl.SalaryInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/salaryinfo")
public class SalaryInfoController {
    @Autowired
    private SalaryInfoService salaryInfoService;
    @PostMapping("/getAllSalaryInfoPageFilter")
    public ResponseView getAllSalaryInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        SalaryInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), SalaryInfoFilter.class);
        return salaryInfoService.queryAllSalaryInfoPageFilter(filter);
    }
    @GetMapping("/getAllSalaryInfo")
    public ResponseView getAllSalaryInfo(){
        return salaryInfoService.queryAllSalaryInfo();
    }
    @PostMapping("/addOneSalaryInfoPageFilter")
    public ResponseView addOneSalaryInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        SalaryInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), SalaryInfoFilter.class);
        return salaryInfoService.addOneSalaryInfoFilter(filter);
    }
    @PostMapping("/modifyOneSalaryInfoPageFilter")
    public ResponseView modifyOneSalaryInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        SalaryInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), SalaryInfoFilter.class);
        return salaryInfoService.modifyOneSalaryInfoFilter(filter);
    }
    @PostMapping("/dropBatchSalaryInfoPageFilter")
    public ResponseView dropBatchSalaryInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        SalaryInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), SalaryInfoFilter.class);
        return salaryInfoService.dropBatchSalaryInfoFilter(filter);
    }
}
