package com.lzx.hbh_system.controller.yytygbmgl.cqgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.yytygbmgl.AttendenceInfoFilter;
import com.lzx.hbh_system.service.yytygbmgl.AttendanceService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/attendanceinfo")
public class AttendanceInfoController {
    @Autowired
    private AttendanceService attendanceService;
    @PostMapping("/getAllAttendanceInfoPageFilter")
    public ResponseView getAllAttendanceInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        AttendenceInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), AttendenceInfoFilter.class);
        return attendanceService.queryAllAttendanceInfoPageFilter(filter);
    }
    @GetMapping("/getAllAttendanceInfo")
    public ResponseView getAllAttendanceInfo(){
        return attendanceService.queryAllAttendanceInfo();
    }
    @PostMapping("/getOneAttendanceInfo")
    public ResponseView getOneAttendanceInfo(@RequestBody RequestView requestView){
        //转对象
        AttendenceInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), AttendenceInfoFilter.class);
        return attendanceService.queryOneAttendanceInfo(filter);
    }
    @PostMapping("/addOneAttendanceInfoFilter")
    public ResponseView addOneAttendanceInfoFilter(@RequestBody RequestView requestView){
        //转对象
        AttendenceInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), AttendenceInfoFilter.class);
        return attendanceService.addOneAttendanceInfoFilter(filter);
    }
    @PostMapping("/modifyOneAttendanceInfoFilter")
    public ResponseView modifyOneAttendanceInfoFilter(@RequestBody RequestView requestView){
        //转对象
        AttendenceInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), AttendenceInfoFilter.class);
        return attendanceService.modifyOneAttendanceInfoFilter(filter);
    }
    @PostMapping("/dropBatchAttendanceInfoFilter")
    public ResponseView dropBatchAttendanceInfoFilter(@RequestBody RequestView requestView){
        //转对象
        AttendenceInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), AttendenceInfoFilter.class);
        return attendanceService.dropBatchAttendanceInfoFilter(filter);
    }
}
