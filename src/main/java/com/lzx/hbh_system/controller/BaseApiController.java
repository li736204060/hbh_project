package com.lzx.hbh_system.controller;

import com.lzx.hbh_system.util.JWTUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

public class BaseApiController {
    //获取当前登录对象

    /**
     * 只能在使用shiro注解的情况下拿到当前登陆的名字
     * @return
     */
    protected String getCurrentUserName() {
        Subject subject = SecurityUtils.getSubject();
        String token = (String) subject.getPrincipal();
        String username = JWTUtil.getUsername(token);
        return username;
    }

    /**
     * 获取当前登陆用户Subject对象
     * @return
     */
    protected Subject getCurrentUserSubject() {
        Subject subject = SecurityUtils.getSubject();
        return subject;
    }
}
