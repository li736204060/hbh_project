package com.lzx.hbh_system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.PermissionFilter;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.service.PermissionService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/permission")
public class PermissionController extends BaseApiController{
    @Autowired
    private PermissionService permissionService;
    @RequiresAuthentication//需要登陆验证token，但不校验角色权限
    @PostMapping("/getPermissionByUsername")
    public ResponseView getPermissionByUsername(@RequestBody RequestView requestView){
        //转对象
        Userfilter userfilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), Userfilter.class);
        return permissionService.getPermissionByUsernameFilter(userfilter);
    }
    @PostMapping("/getAllPermissionFilter")
    //@RequiresRoles(value = {"超级管理员"},logical = Logical.OR) // 需要 角色为 “超级管理员” 才能访问
    public ResponseView getAllPermissionFilter(@RequestBody RequestView requestView){
        //转对象
        PermissionFilter permissionFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PermissionFilter.class);
        return permissionService.getAllPermissionFilter(permissionFilter);
    }
}
