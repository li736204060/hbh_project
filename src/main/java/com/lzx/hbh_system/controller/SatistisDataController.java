package com.lzx.hbh_system.controller;

import com.lzx.hbh_system.service.SatistisDataService;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/satistisdata")
public class SatistisDataController {
    @Autowired
    private SatistisDataService service;
    @GetMapping("/getAllData")
    public ResponseView getAllData(){
        return service.getDashBoardData();
    }
}
