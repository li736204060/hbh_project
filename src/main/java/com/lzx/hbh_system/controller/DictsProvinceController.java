package com.lzx.hbh_system.controller;

import com.lzx.hbh_system.service.ProvinceService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/province")
public class DictsProvinceController {
    @Autowired
    private ProvinceService provinceService;
    @PostMapping("/getAll")
    public ResponseView getAll(@RequestBody RequestView requestView){
        return provinceService.getAll();
    }
}
