package com.lzx.hbh_system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.MenuFilter;
import com.lzx.hbh_system.service.MenuService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;
    @PostMapping("/getAll")
    public ResponseView getAllMenu(@RequestBody RequestView requestView){
        //转对象
        MenuFilter menuFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), MenuFilter.class);
        return menuService.getAllMenu(menuFilter);
    }
    @GetMapping("/getAllMenuDto")
    public ResponseView getAllMenu(){
        return menuService.getAllMenuDto();
    }
    @PostMapping("/getByRoleCode")
    public ResponseView getMenuByRoleCode(@RequestBody RequestView requestView){
        //转对象
        MenuFilter menuFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), MenuFilter.class);
        return menuService.getMenuByFilter(menuFilter);
    }
    @PostMapping("/getOneMenuInfo")
    public ResponseView getOneMenuInfo(@RequestBody RequestView requestView){
        //转对象
        MenuFilter menuFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), MenuFilter.class);
        return menuService.getOneMenuInfo(menuFilter);
    }
    @PostMapping("/addOneMenuInfo")
    public ResponseView addOneMenuInfo(@RequestBody RequestView requestView){
        //转对象
        MenuFilter menuFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), MenuFilter.class);
        return menuService.addOneMenuInfo(menuFilter);
    }
    @PostMapping("/modifyOneMenuInfo")
    public ResponseView modifyOneMenuInfo(@RequestBody RequestView requestView){
        //转对象
        MenuFilter menuFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), MenuFilter.class);
        return menuService.modifyOneMenuInfo(menuFilter);
    }
    @PostMapping("/dropBatchMenuInfo")
    public ResponseView dropBatchMenuInfo(@RequestBody RequestView requestView){
        //转对象
        MenuFilter menuFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), MenuFilter.class);
        return menuService.dropBatchMenuInfo(menuFilter);
    }
}
