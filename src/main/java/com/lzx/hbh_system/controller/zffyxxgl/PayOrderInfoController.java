package com.lzx.hbh_system.controller.zffyxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.zffyxxgl.PayOrderInfoFilter;
import com.lzx.hbh_system.service.zffyxxgl.PayOrderInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payorderinfo")
public class PayOrderInfoController {
    @Autowired
    private PayOrderInfoService payOrderInfoService;
    @PostMapping("/getAllPayOrderInfoPageFilter")
    public ResponseView getAllPayOrderInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        PayOrderInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayOrderInfoFilter.class);
        return payOrderInfoService.queryAllPayOrderInfoPageFilter(filter);
    }
    @PostMapping("/addOnePayOrderInfo")
    public ResponseView addOnePayOrderInfo(@RequestBody RequestView requestView){
        //转对象
        PayOrderInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayOrderInfoFilter.class);
        return payOrderInfoService.addOnePayOrderInfo(filter);
    }
    @PostMapping("/modifyBatchPayOrderInfo")
    public ResponseView modifyBatchPayOrderInfo(@RequestBody RequestView requestView){
        //转对象
        PayOrderInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayOrderInfoFilter.class);
        return payOrderInfoService.modifyBatchPayOrderInfo(filter);
    }
}
