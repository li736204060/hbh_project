package com.lzx.hbh_system.controller.zffyxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.zffyxxgl.PayInfoFilter;
import com.lzx.hbh_system.service.zffyxxgl.PayInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payinfo")
public class PayInfoController {
    @Autowired
    private PayInfoService payInfoService;
    @PostMapping("/getAllPayInfoPageFilter")
    public ResponseView getAllPayInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        PayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayInfoFilter.class);
        return payInfoService.queryAllPayInfoPageFilter(filter);
    }
    @PostMapping("/getAllQuarterPayInfoByHouseCode")
    public ResponseView getAllQuarterPayInfoByHouseCode(@RequestBody RequestView requestView){
        //转对象
        PayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayInfoFilter.class);
        return payInfoService.queryAllCurrentQuarterPayInfoByHouseHoldCode(filter);
    }
    @PostMapping("/getAllHasPaidPayInfoByHouseCode")
    public ResponseView getAllHasPaidPayInfoByHouseCode(@RequestBody RequestView requestView) {
        //转对象
        PayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayInfoFilter.class);
        return payInfoService.queryAllHasPaidPayInfoByHouseHoldCode(filter);
    }
    @PostMapping("/processingPay")
    public ResponseView processingPay(@RequestBody RequestView requestView){
        //转对象
        PayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayInfoFilter.class);
        return payInfoService.processingPay(filter);
    }
    @PostMapping("/addBatchPayInfo")
    public ResponseView addBatchPayInfo(@RequestBody RequestView requestView){
        //转对象
        PayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayInfoFilter.class);
        return payInfoService.addBatchPayInfo(filter);
    }
    @PostMapping("/modifyBatchPayInfo")
    public ResponseView modifyBatchPayInfo(@RequestBody RequestView requestView){
        //转对象
        PayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayInfoFilter.class);
        return payInfoService.modifyBatchPayInfo(filter);
    }
    @PostMapping("/dropBatchPayInfo")
    public ResponseView dropBatchPayInfo(@RequestBody RequestView requestView){
        //转对象
        PayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), PayInfoFilter.class);
        return payInfoService.dropBatchPayInfo(filter);
    }
}
