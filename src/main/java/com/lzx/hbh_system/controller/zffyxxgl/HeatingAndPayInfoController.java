package com.lzx.hbh_system.controller.zffyxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.zffyxxgl.HeatingAndPayInfoFilter;
import com.lzx.hbh_system.service.zffyxxgl.HeatingAndPayInfoSerivce;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/heatingandpay")
public class HeatingAndPayInfoController {
    @Autowired
    private HeatingAndPayInfoSerivce heatingAndPayInfoSerivce;
    @PostMapping("/getHeatingAndPayTodaySatisifyData")
    public ResponseView getHeatingAndPayTodaySatisifyDataPageFilter(@RequestBody RequestView requestView){
        //转对象
        HeatingAndPayInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HeatingAndPayInfoFilter.class);
        return heatingAndPayInfoSerivce.getAllHeatingAndPayInfo(filter);
    }
}
