package com.lzx.hbh_system.controller.zffyxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.VillageInfoFilter;
import com.lzx.hbh_system.bo.filter.zffyxxgl.ExpanseInfoFilter;
import com.lzx.hbh_system.service.zffyxxgl.ExpanseInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/expanseinfo")
public class ExpanseInfoController {
    @Autowired
    private ExpanseInfoService expanseInfoService;
    @PostMapping("/getAllExpanseInfoPageFilter")
    public ResponseView getAllExpanseInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        ExpanseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ExpanseInfoFilter.class);
        return expanseInfoService.queryAllExpanseInfoPageFilter(filter);
    }
    @GetMapping("/getAllExpanseInfo")
    public ResponseView getAllExpanseInfo(){
        return expanseInfoService.queryAllExpanseInfo();
    }
    @PostMapping("/getAllExpanseInfoByHousehold")
    public ResponseView getAllExpanseInfoByHousehold(@RequestBody RequestView requestView){
        //转对象
        ExpanseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ExpanseInfoFilter.class);
        return expanseInfoService.queryAllExpanseInfoByHousehold(filter);
    }
    @PostMapping("/addOneExpanseInfo")
    public ResponseView addOneExpanseInfo(@RequestBody RequestView requestView){
        //转对象
        ExpanseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ExpanseInfoFilter.class);
        return expanseInfoService.addOneExpanseInfo(filter);
    }
    @PostMapping("/modifyOneExpanseInfo")
    public ResponseView modifyOneExpanseInfo(@RequestBody RequestView requestView){
        //转对象
        ExpanseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ExpanseInfoFilter.class);
        return expanseInfoService.modifyOneExpanseInfo(filter);
    }
    @PostMapping("/dropBatchExpanseInfo")
    public ResponseView dropBatchExpanseInfo(@RequestBody RequestView requestView){
        //转对象
        ExpanseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ExpanseInfoFilter.class);
        return expanseInfoService.modifyOneExpanseInfo(filter);
    }
}
