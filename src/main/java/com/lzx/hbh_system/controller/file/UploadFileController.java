package com.lzx.hbh_system.controller.file;

import com.lzx.hbh_system.log.SysLog;
import com.lzx.hbh_system.util.GitHubBucketUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/files")
@Slf4j
public class UploadFileController {
    @Autowired
    private GitHubBucketUtil gitHubBucketUtil;
    @SysLog(desc = "文件模块-单上传仓库")
    @PostMapping("/uploadFile")
    public ResponseView uploadFile(@RequestParam (value = "file",required = true ) MultipartFile file,@RequestParam(value = "userCode") String userCode){
        ResponseView responseView = new ResponseView();
        try {
            return gitHubBucketUtil.uploadUImg(file,userCode);
        } catch (IOException e) {
            e.printStackTrace();
            log.error("图片上传失败，{}",file.getOriginalFilename());
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"图片上传失败，file.getOriginalFilename()",500));
        return responseView;
    }
}
