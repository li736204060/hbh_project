package com.lzx.hbh_system.controller.xqzhxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.VillageInfoFilter;
import com.lzx.hbh_system.service.xqzhxxgl.VillageInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/villageinfo")
public class VillageInfoController {
    @Autowired
    private VillageInfoService villageInfoService;
    @PostMapping("/getAllVillageInfoPage")
    public ResponseView getAllVillageInfoPage(@RequestBody RequestView requestView){
        //转对象
        VillageInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VillageInfoFilter.class);
        return villageInfoService.queryAllVillageInfoPageFilter(filter);
    }
    @GetMapping("/getAllVillageInfoList")
    public ResponseView getAllVillageInfoList(){
        return villageInfoService.queryAllVillageInfoList();
    }
    @PostMapping("/addOneVillageInfo")
    public ResponseView addOneVillageInfo(@RequestBody RequestView requestView){
        //转对象
        VillageInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VillageInfoFilter.class);
        return villageInfoService.addOneVillageInfo(filter);
    }
    @PostMapping("/modifyOneVillageInfo")
    public ResponseView modifyOneVillageInfo(@RequestBody RequestView requestView){
        //转对象
        VillageInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VillageInfoFilter.class);
        return villageInfoService.modifyOneVillageInfo(filter);
    }
    @PostMapping("/dropBatchVillageInfo")
    public ResponseView dropBatchVillageInfo(@RequestBody RequestView requestView){
        //转对象
        VillageInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), VillageInfoFilter.class);
        return villageInfoService.dropBatchVillageInfo(filter);
    }
}
