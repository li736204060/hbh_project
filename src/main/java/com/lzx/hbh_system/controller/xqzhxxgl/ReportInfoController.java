package com.lzx.hbh_system.controller.xqzhxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.ReportInfoFilter;
import com.lzx.hbh_system.service.xqzhxxgl.ReportInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/reportinfo")
public class ReportInfoController {
    @Autowired
    private ReportInfoService reportInfoService;
    @PostMapping("/getAllReportInfoPageFilter")
    public ResponseView getAllReportInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        ReportInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ReportInfoFilter.class);
        return reportInfoService.quertAllReportInfoPageFilter(filter);
    }
    @PostMapping("/addOneReportInfo")
    public ResponseView addOneReportInfo(@RequestBody RequestView requestView){
        //转对象
        ReportInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ReportInfoFilter.class);
        return reportInfoService.addOneReportInfo(filter);
    }
    @PostMapping("/modifyOneReportInfo")
    public ResponseView modifyOneReportInfo(@RequestBody RequestView requestView){
        //转对象
        ReportInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ReportInfoFilter.class);
        return reportInfoService.modifyOneReportInfo(filter);
    }
    @PostMapping("/dropBatchReportInfo")
    public ResponseView dropBatchReportInfo(@RequestBody RequestView requestView){
        //转对象
        ReportInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), ReportInfoFilter.class);
        return reportInfoService.dropBatchReportInfo(filter);
    }

}
