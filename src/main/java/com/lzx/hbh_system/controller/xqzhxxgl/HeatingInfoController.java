package com.lzx.hbh_system.controller.xqzhxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HeatingInfoFilter;
import com.lzx.hbh_system.service.xqzhxxgl.HeatingInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/heatinginfo")
public class HeatingInfoController {
    @Autowired
    private HeatingInfoService heatingInfoService;
    @PostMapping("/getAllHeatingInfoPageFilter")
    public ResponseView getAllHeatingInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        HeatingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HeatingInfoFilter.class);
        return heatingInfoService.queryAllHeatingInfoPageFilter(filter);
    }
    @PostMapping("/addOneHeatingInfo")
    public ResponseView addOneHeatingInfo(@RequestBody RequestView requestView){
        //转对象
        HeatingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HeatingInfoFilter.class);
        return heatingInfoService.addOneHeatingInfo(filter);
    }
    @PostMapping("/modifyOneHeatingInfo")
    public ResponseView modifyOneHeatingInfo(@RequestBody RequestView requestView){
        //转对象
        HeatingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HeatingInfoFilter.class);
        return heatingInfoService.modifyOneHeatingInfo(filter);
    }
    @PostMapping("/dropBatchHeatingInfo")
    public ResponseView dropBatchHeatingInfo(@RequestBody RequestView requestView){
        //转对象
        HeatingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HeatingInfoFilter.class);
        return heatingInfoService.dropBatchHeatingInfo(filter);
    }

}
