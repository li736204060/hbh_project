package com.lzx.hbh_system.controller.xqzhxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HouseInfoFilter;
import com.lzx.hbh_system.service.xqzhxxgl.HouseInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/houseinfo")
public class HouseInfoController {
    @Autowired
    private HouseInfoService houseInfoService;
    @PostMapping("/getAllHouseInfoPageFilter")
    public ResponseView getAllHouseInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        HouseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseInfoFilter.class);
        return houseInfoService.queryAllHouseInfoPageFilter(filter);
    }
    @PostMapping("/addOneHouseInfo")
    public ResponseView addOneHouseInfo(@RequestBody RequestView requestView){
        //转对象
        HouseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseInfoFilter.class);
        return houseInfoService.addOneHouseInfo(filter);
    }
    @PostMapping("/modifyOneHouseInfo")
    public ResponseView modifyOneHouseInfo(@RequestBody RequestView requestView){
        //转对象
        HouseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseInfoFilter.class);
        return houseInfoService.modifyOneHouseInfo(filter);
    }
    @PostMapping("/dropBatchHouseInfo")
    public ResponseView dropBatchHouseInfo(@RequestBody RequestView requestView){
        //转对象
        HouseInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseInfoFilter.class);
        return houseInfoService.dropBatchHouseInfo(filter);
    }

}
