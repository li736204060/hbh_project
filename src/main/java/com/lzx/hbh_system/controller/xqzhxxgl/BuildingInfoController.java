package com.lzx.hbh_system.controller.xqzhxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.BuildingInfoFilter;
import com.lzx.hbh_system.service.xqzhxxgl.BuildingInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/buildingInfo")
public class BuildingInfoController {
    @Autowired
    private BuildingInfoService buildingInfoService;
    @PostMapping("/getAllBuildingInfoPageFilter")
    public ResponseView getAllBuildingInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        BuildingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), BuildingInfoFilter.class);
        return buildingInfoService.queryAllBuilldingInfoPageFilter(filter);
    }
    @PostMapping("/getAllBuildingInfoByVillageCode")
    public ResponseView getAllBuildingInfoByVillageCode(@RequestBody RequestView requestView){
        //转对象
        BuildingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), BuildingInfoFilter.class);
        return buildingInfoService.queryAllBuilldingInfoByVillageCode(filter);
    }
    @PostMapping("/addOneBuildingInfo")
    public ResponseView addOneBuildingInfo(@RequestBody RequestView requestView){
        //转对象
        BuildingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), BuildingInfoFilter.class);
        return buildingInfoService.addOneBuilldingInfo(filter);
    }
    @PostMapping("/modifyOneBuildingInfo")
    public ResponseView modifyOneBuildingInfo(@RequestBody RequestView requestView){
        //转对象
        BuildingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), BuildingInfoFilter.class);
        return buildingInfoService.modifyOneBuildingInfo(filter);
    }
    @PostMapping("/dropBatchBuildingInfo")
    public ResponseView dropBatchBuildingInfo(@RequestBody RequestView requestView){
        //转对象
        BuildingInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), BuildingInfoFilter.class);
        return buildingInfoService.dropBatchBuildingInfo(filter);
    }
}
