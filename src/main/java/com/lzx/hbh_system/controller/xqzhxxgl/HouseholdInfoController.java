package com.lzx.hbh_system.controller.xqzhxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HouseholdInfoFilter;
import com.lzx.hbh_system.service.xqzhxxgl.HouseHoldInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/householdInfo")
public class HouseholdInfoController {
    @Autowired
    private HouseHoldInfoService houseHoldInfoService;
    @PostMapping("/getAllHouseholdInfoPageFilter")
    public ResponseView getAllHouseholdInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        HouseholdInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseholdInfoFilter.class);
        return houseHoldInfoService.queryAllHouseHoldInfoPageFilter(filter);
    }
    @GetMapping("/getAllHouseholdInfoList")
    public ResponseView getAllHouseholdInfoList(){
        return houseHoldInfoService.queryAllHouseHoldInfoList();
    }
    @PostMapping("/addOneHouseholdInfo")
    public ResponseView addOneHouseholdInfo(@RequestBody RequestView requestView){
        //转对象
        HouseholdInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseholdInfoFilter.class);
        return houseHoldInfoService.addOneHouseHoldInfo(filter);
    }
    @PostMapping("/modifyOneHouseholdInfo")
    public ResponseView modifyOneHouseholdInfo(@RequestBody RequestView requestView){
        //转对象
        HouseholdInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseholdInfoFilter.class);
        return houseHoldInfoService.modifyOneHouseHoldInfo(filter);
    }
    @PostMapping("/dropBatchHouseholdInfo")
    public ResponseView dropBatchHouseholdInfo(@RequestBody RequestView requestView){
        //转对象
        HouseholdInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), HouseholdInfoFilter.class);
        return houseHoldInfoService.dropBatchHouseHoldInfo(filter);
    }

}
