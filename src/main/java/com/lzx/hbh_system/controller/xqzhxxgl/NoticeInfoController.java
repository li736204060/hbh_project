package com.lzx.hbh_system.controller.xqzhxxgl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.NoticeInfoFilter;
import com.lzx.hbh_system.service.xqzhxxgl.NoticeInfoService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/noticeinfo")
public class NoticeInfoController {
    @Autowired
    private NoticeInfoService noticeInfoService;
    @PostMapping("/getAllNoticeInfoPageFilter")
    public ResponseView getAllNoticeInfoPageFilter(@RequestBody RequestView requestView){
        //转对象
        NoticeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), NoticeInfoFilter.class);
        return noticeInfoService.queryAllNoticeInfoPageFilter(filter);
    }
    @PostMapping("/addOneNoticeInfo")
    public ResponseView addOneNoticeInfo(@RequestBody RequestView requestView){
        //转对象
        NoticeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), NoticeInfoFilter.class);
        return noticeInfoService.addOneNoticeInfo(filter);
    }
    @PostMapping("/modifyOneNoticeInfo")
    public ResponseView modifyOneNoticeInfo(@RequestBody RequestView requestView){
        //转对象
        NoticeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), NoticeInfoFilter.class);
        return noticeInfoService.modifyOneNoticeInfo(filter);
    }
    @PostMapping("/dropBatchNoticeInfo")
    public ResponseView dropBatchNoticeInfo(@RequestBody RequestView requestView){
        //转对象
        NoticeInfoFilter filter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), NoticeInfoFilter.class);
        return noticeInfoService.dropBatchNoticeInfo(filter);
    }
}
