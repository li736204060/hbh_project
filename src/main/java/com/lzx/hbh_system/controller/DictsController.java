package com.lzx.hbh_system.controller;

import com.lzx.hbh_system.service.DictsService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dicts")
public class DictsController {
    @Autowired
    private DictsService dictsService;
    @GetMapping("/getDictsByType")
    public ResponseView getDictsByType(@RequestParam("dictType") String dictType){
        return dictsService.getDictsByType(dictType);
    }
    @GetMapping("/getTreeDictsByType")
    public ResponseView getTreeDictsByType(@RequestParam("dictType") String dictType){
        return dictsService.getTreeDictsByType(dictType);
    }
    @GetMapping("/getMapTreeDictsByType")
    public ResponseView getMapTreeDictsByType(@RequestParam("dictType") String dictType){
        return dictsService.getTreeMapDictsByType(dictType);
    }
}
