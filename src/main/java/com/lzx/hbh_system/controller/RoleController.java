package com.lzx.hbh_system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.SysRole;
import com.lzx.hbh_system.bo.filter.RoleFilter;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.service.RoleService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/role")
public class RoleController {
    @Autowired
    private RoleService roleService;
    @PostMapping("/getrolebymenucode")
    public ResponseView getrolebymenucode(@RequestBody RequestView requestView){
        //转对象
        RoleFilter roleFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), RoleFilter.class);
        return roleService.getRoleByMenuCode(roleFilter);
    }
    @PostMapping("/getrolebyusername")
    public ResponseView getrolebyusername(@RequestBody RequestView requestView) throws Exception {
        //转对象
        Userfilter userfilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), Userfilter.class);
        return roleService.getRoleByUsernameFilter(userfilter);
    }
    @PostMapping("/getAllRole")
    @RequiresRoles(value = {"超级管理员"},logical = Logical.OR) // 需要 角色为 “超级管理员” 才能访问
    public ResponseView getAllRole(@RequestBody RequestView requestView){
        //转对象
        RoleFilter roleFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), RoleFilter.class);
        return roleService.getAllRoleFilter(roleFilter);
    }
    @PostMapping("/addOne")
    public ResponseView addOne(@RequestBody RequestView requestView){
        //转对象
        RoleFilter roleFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), RoleFilter.class);
        return roleService.addOneRoleFilter(roleFilter);
    }
    @PostMapping("/modifyOne")
    public  ResponseView modifyOne(@RequestBody RequestView requestView){
        //转对象
        RoleFilter roleFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), RoleFilter.class);
        return roleService.modifyOneRoleFilter(roleFilter);
    }
    @PostMapping("/dropBatchs")
    public ResponseView dropBatches(@RequestBody RequestView requestView){
        //转对象
        RoleFilter roleFilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), RoleFilter.class);
        return roleService.deleteOneRoleFilter(roleFilter);
    }
}
