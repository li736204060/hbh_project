package com.lzx.hbh_system.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.lzx.hbh_system.bo.filter.UserRoleFilter;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.service.UserRoleService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/userrole")
public class UserRoleController {
    @Autowired
    private UserRoleService userRoleService;
    @PostMapping("/getAllRoleUser")
    public ResponseView getAllRoleUser(){
        return userRoleService.getRoleUserCountList();
    }
    @RequiresRoles(value = {"超级管理员"},logical = Logical.OR) // 需要 角色为 “超级管理员” 才能访问
    @PostMapping("/modifyUserRole")
    public ResponseView modifyUserRole(@RequestBody RequestView requestView){
        //转对象
        UserRoleFilter userfilter = JSONObject.parseObject(JSON.toJSONString(requestView.getMain()), UserRoleFilter.class);
        return userRoleService.modifyUserRole(userfilter);
    }
}
