package com.lzx.hbh_system.log;


import com.lzx.hbh_system.bo.user.UseroperaInfo;
import com.lzx.hbh_system.service.user.UseroperaInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


/**
 * 注解形式的监听 异步监听日志事件
 */
@Slf4j
@Component
public class SysLogListener {
    @Autowired
    private UseroperaInfoService useroperaInfoService;
    @Async
    @Order // 设置IOC Bean的加载前后执行顺序 默认最低优先级
    @EventListener(SysLogEvent.class)
    public void saveLogData(SysLogEvent event){
        log.info("日志记录操作执行");
        // 获取发布事件中的日志对象数据
        UseroperaInfo useroperaInfo = (UseroperaInfo) event.getSource();
        // 日志保存
        useroperaInfoService.saveLogDataToUserOperainfo(useroperaInfo);
    }
}
