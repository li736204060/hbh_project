package com.lzx.hbh_system.log;

import org.springframework.context.ApplicationEvent;

/**
 * 系统日志事件
 */
public class SysLogEvent extends ApplicationEvent {
    public SysLogEvent(Object source) {
        super(source);
    }
}
