package com.lzx.hbh_system.util.requestEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 请求消息视图
 * @param <T>
 */
@Data
public class RequestView<T> implements Serializable {
    /**
     * 请求消息头
     */
    private ReqInMsgHeader reqInMsgHeader;
    /**
     * 请求消息实体
     */
    private T main;

}
