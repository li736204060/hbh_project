package com.lzx.hbh_system.util.requestEntity;


import lombok.Data;

import java.io.Serializable;
@Data
public class ReqInMsgHeader implements Serializable {
    /**
     * 请求模式-备用
     */
    private String ReqModles;
}
