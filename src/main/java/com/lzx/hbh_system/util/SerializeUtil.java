package com.lzx.hbh_system.util;

import com.lzx.hbh_system.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.*;

/**
 *  ---弃用---
 * 目的:在于将Object 对象放在redis中存储。  --- eq:设置了redis key value 设置策略 自动序列化 和 反序列化 因此 不需要该类了
 * function：序列化 与 反序列化
 */
@Slf4j
public class SerializeUtil {
    public static byte[] serialize(Object object) {
        ObjectOutputStream obi = null;
        ByteArrayOutputStream bai = null;
        try {
            bai = new ByteArrayOutputStream();
            obi = new ObjectOutputStream(bai);
            obi.writeObject(object);
            byte[] byt = bai.toByteArray();
            return byt;
        } catch (IOException e) {
            log.info(e.getMessage());
        }
        return null;
    }

    public static Object unserialize( byte[] bytes) {
        ObjectInputStream oii = null;
        ByteArrayInputStream bis = null;
        bis = new ByteArrayInputStream(bytes);
        try {
            oii = new ObjectInputStream(bis);
            Object obj = oii.readObject();
            return obj;
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
//        String testbyte = "rO0ABXNyAB5jb20ubHp4LmhiaF9zeXN0ZW0uZHRvLlVzZXJEdG9h7EnfewshvQIABEwACWxvZ2ludGltZXQAEkxqYXZhL2xhbmcvU3RyaW5nO0wABHNhbHRxAH4AAUwABnN0YXR1c3EAfgABTAAIdXNlck5hbWVxAH4AAXhwdAATMjAyMi0wMS0yNSAxOTozOTo1NHQABXliQmdIdAABMXQAATE=";
//        System.out.println(testbyte.getBytes());
//        UserDto userDto = new UserDto();
//        userDto.setUserName("1");
//        byte[] ss = serialize(userDto);
//        System.out.println(ss);
//        String ssstr = ss.toString();
//        System.out.println(ss.toString());
//        System.out.println(Byte.valueOf(ssstr));
//        UserDto userDto1 = (UserDto) unserialize(ssstr.getBytes());
//        System.out.println(userDto1.getUserName());
    }
}
