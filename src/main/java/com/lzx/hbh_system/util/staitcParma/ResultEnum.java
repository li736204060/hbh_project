package com.lzx.hbh_system.util.staitcParma;

import lombok.Getter;

/**
 * 响应结果枚举类Enum
 */
@Getter
public enum ResultEnum {
    /**
     * 后台响应成功
     */
    SUCCESS(200,"成功！"),

    /**
     * 后台响应失败
     */
    UN_SUCCESS(400,"失败！"),

    /**
     * 系统错误
     */
    SYSTEM_ERROR(500,"系统错误，请联系管理员！"),
    /**
     * 系统推送服务错误
     */
    SYSTEM_SSESERVICE_ERROR(510,"系统推送服务错误，请联系管理员！"),
    /**
     * 用户权限不足
     */
    NO_PERMISSION(401,"该用户权限不足！"),

    /**
     * 查询无结果
     */
    DATA_NOT_FOUND(201,"数据为空！"),
    /**
     * 参数为空
     */
    PARMAS_NULL(100,"传入参数为空！"),
    /**
     * 参数类型不匹配
     */
    PARMAS_NOT_MATCHE(101,"参数类型不匹配"),
    /**
     * Token非法
     */
    TOKEN_INVALID(600,"token非法！"),
    /**
     * Token不匹配
     */
    TOKEN_NOT_MATCHE(601,"token不匹配！"),
    /**
     * Token已经过期
     */
    TOKEN_OUT_OF_DATE(602,"token已经过期！"),
    /**
     * Token不存在
     */
    TOKEN_NULL(604,"token不存在！"),
    /**
     * 类型不匹配统称 对象类型等
     */
    TYPE_NOT_MATCHE(900,"类型不匹配！"),
    /**
     * 未登录
     */
    NO_LOGIN(1004,"用户未登录！"),
    /**
     * 访问出错，（用户redis状态已经离线，拒绝访问，需要重新登陆为在线状态下，携带有效令牌才能正常访问）
     */
    ACCESS_ERROR(1005,"访问出错,携带令牌存在且可能有效且用户可能已登出，可能未正确登陆该系统，系统拒绝访问，请重新登陆后再次尝试请求操作！"),
    /**
     * 用户存储状态过期，（用户redis状态已经失效，拒绝访问，需要重新登陆）
     */
    ACCESS_LOGIN_TIME_OUT(1006,"用户登陆状态已过期，系统拒绝访问，请重新登陆后再次尝试请求操作！"),
    /**
     * 用户不存在或账号密码不匹配
     */
    USER_NULL_OR_USERACCOUNT_NOT_MACHE(1000,"用户不存在或账号密码不匹配！"),
    /**
     * 用户被锁定-冻结
     */
    USER_LOCKED(1001,"用户被锁定！"),
    /**
     * 用户欠费
     */
    USER_ARREARS(1002,"用户欠费！"),
    /**
     * 用户未登录
     */
    USER_UNLOGIN(1004,"用户未登录，以游客身份访问！"),
    /**
     * 用户注销
     */
    USER_UNREGESTER(1003,"用户已注销！"),
    /**
     * 过期 （含义待定）
     */
    IS_OUT_OF_DATE(4000,"已过期！"),
    /**
     * 权限匹配错误(角色或权限或Token不存在)
     */
    USER_AUTHORIZATION_ERROR(1022,"权限匹配异常,或者未携带登陆认证，联系管理员授权后尝试登陆！"),
    /**
     * 用户不匹配角色，角色不存在或者未匹配 或 用户不匹配权限操作，权限不存在，或不匹配
     */
    USER_NOTMATCH_ROLE_OR_PEMISSION(401,"权限不足[角色或权限不匹配]，请授权后操作！"),
    ;
    private Integer code;
    private String msg;
    ResultEnum(int i, String s) {
        this.code = i;
        this.msg = s;
    }
}
