package com.lzx.hbh_system.util;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
@Slf4j
public class TurnDateToStringUtil {
	/**
	 * 获取当前时间(返回格式：yyyyy-MM-dd HH:mm:ss)
	 *
	 * @return time
	 * @throws Exception
	 */
	public static String getTime(){
		String time = null;
		Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		// 设置日期格式
		SimpleDateFormat df = null;
		try {
			df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} catch (Exception e) {
			log.error("时间格式化出现异常！");
			e.printStackTrace();
		}
		// new Date()为获取当前系统时间
		time = df.format(date);
		return time;

	}
	/**
	 * 获取当前时间(default:返回格式：yyyy-MM-dd HH:mm:ss)
	 *
	 * @return time
	 * @throws Exception
	 */
	public static String getTime(String partten){
		String time = null;
		Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		// 设置日期格式
		SimpleDateFormat df = null;
		try {
			df = new SimpleDateFormat(partten != null ? partten : "yyyy-MM-dd HH:mm:ss");
		} catch (Exception e) {
			log.error("时间格式化出现异常！");
			e.printStackTrace();
		}
		// new Date()为获取当前系统时间
		time = df.format(date);
		return time;

	}
	/**
	 * 获取当前时间 Date(返回格式：yyyyy-MM-dd HH:mm:ss)
	 *
	 * @return Date
	 * @throws Exception
	 */
	public static Date getDateTime(){
		Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		return date;

	}
	/**
	 * 获取当前时间(返回格式：yyyy-MM-dd)
	 *
	 * @return time
	 * @throws Exception
	 */
	public static String getTimeTo_Y_M_D(){
		String time = null;
//		Date date = new Date();
		Calendar c = Calendar.getInstance();
		Date date = c.getTime();
		// 设置日期格式
		SimpleDateFormat df = null;
		try {
			df = new SimpleDateFormat("yyyy-MM-dd");
		} catch (Exception e) {
			log.error("时间格式化出现异常！");
		}
		// new Date()为获取当前系统时间
		time = df.format(date);
		return time;

	}
	/**
	 * 获取指定时间的指定格式(返回格式：yyyy-MM-dd)
	 *
	 * @return time
	 * @throws Exception
	 */
	public static String getTimeTo_Y_M_D(Date date){
		String time = null;
		// 设置日期格式
		SimpleDateFormat df = null;
		try {
			df = new SimpleDateFormat("yyyy-MM-dd");
		} catch (Exception e) {
			log.error("时间格式化出现异常！");
		}
		// new Date()为获取当前系统时间
		time = df.format(date);
		return time;

	}

	/**
	 * 获取预约默认结束时间-开始时间+1天(返回格式：yyyy-MM-dd HH:mm:ss)
	 *
	 * @return timeAddOneday
	 * @throws Exception
	 */
	public static String getDefaltEndTime(){
		String timeAddOneday = null;
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH,1);//返回当月的第几天，这里1 表示返回一号，add加1天
		Date date = c.getTime();
		// 设置日期格式
		SimpleDateFormat df = null;
		try {
			df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} catch (Exception e) {
			log.error("时间格式化出现异常！");
		}
		// new Date()为获取当前系统时间
		timeAddOneday = df.format(date);
		return timeAddOneday;

	}
}