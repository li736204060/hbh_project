package com.lzx.hbh_system.util.responseEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回消息视图
 * @param <T>
 */
@Data
public class ResponseView<T> implements Serializable {
    /**
     * 响应消息头
     */
    private RespOutMsgHeader respOutMsgHeader;
    /**
     * 响应详细实体
     */
    private T Main;
}
