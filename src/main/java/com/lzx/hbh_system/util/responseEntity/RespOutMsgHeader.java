package com.lzx.hbh_system.util.responseEntity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.staitcParma.ResultEnum;

import java.io.Serializable;

public class RespOutMsgHeader implements Serializable {

    /**
     * 响应时间
     */
    @JsonFormat(locale = "zh",timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private String respTime;
    /**
     * 响应消息数量
     */
    private Integer respMsgCount;
    /**
     * 响应状态 true 成功 false 失败
     */
    private Boolean respStatus;
    /**
     * 响应code码
     *
     */
    private Integer respCode;
    /**
     * 消息
     */
    private String msg;
    /**
     * 返回登陆成功后的token
     */
    private String token;


    /**
     * 成功响应，一般成功调用
     */
    public static RespOutMsgHeader success(){
        return new RespOutMsgHeader(TurnDateToStringUtil.getTime(),true,ResultEnum.SUCCESS.getCode());
    }

    /**
     * 成功响应，适用于单一事务响应
     * @param msg
     * @return
     */
    public static RespOutMsgHeader success(String msg){
        return new RespOutMsgHeader(1,true,msg,ResultEnum.SUCCESS.getCode());
    }
    /**
     * 成功响应，可设置MsgCount、Status、msg、
     */
    public static RespOutMsgHeader success(Integer MsgCount,Boolean Status,String msg){
        return new RespOutMsgHeader(MsgCount,Status,msg,ResultEnum.SUCCESS.getCode());
    }
    public static RespOutMsgHeader success(Integer MsgCount,String msg){
        return new RespOutMsgHeader(MsgCount,true,msg,ResultEnum.SUCCESS.getCode());
    }
    /**
     * 登陆成功响应，可设置MsgCount、Status、msg、token
     */
    public static RespOutMsgHeader loginsuccess(Integer MsgCount,Boolean Status,String msg,String token){
        return new RespOutMsgHeader(MsgCount,Status,msg,token,ResultEnum.SUCCESS.getCode());
    }
    /**
     * 失败响应，响应时间
     */
    public static RespOutMsgHeader error(){
        return new RespOutMsgHeader(false, ResultEnum.UN_SUCCESS.getCode());
    }
    /**
     * 失败响应，响应数目、失败响应状态、错误消息
     */
    public static RespOutMsgHeader error(Integer MsgCount,Boolean Status,String msg,Integer code){
        return new RespOutMsgHeader(MsgCount,Status,msg,code);
    }

    public static RespOutMsgHeader error(Boolean Status,String msg,Integer code){
        return new RespOutMsgHeader(0,Status,msg,code);
    }
    public static RespOutMsgHeader error(String msg){
        return new RespOutMsgHeader(0,false,msg,500);
    }
    public static RespOutMsgHeader error(String msg,Integer code){
        return new RespOutMsgHeader(0,false,msg,code);
    }
    public RespOutMsgHeader(Integer respMsgCount, Boolean respStatus, Integer respCode, String msg, String token){
        this.respTime = TurnDateToStringUtil.getTime();
        this.respMsgCount = respMsgCount;
        this.respStatus = respStatus;
        this.respCode = respCode;
        this.msg = msg;
        this.token = token;
    }

    public RespOutMsgHeader() {
    }

    public RespOutMsgHeader(String respTime, Boolean respStatus, Integer respCode) {
        this.respTime = respTime;
        this.respStatus = respStatus;
        this.respCode = respCode;
    }

    public RespOutMsgHeader(boolean status, Integer code) {
        this.respStatus = status;
        this.respCode = code;
    }
    public RespOutMsgHeader(Integer msgCount, Boolean status, String msg,Integer code){
        this.respTime = TurnDateToStringUtil.getTime();
        this.msg = msg;
        this.respStatus = status;
        this.respMsgCount = msgCount;
        this.respCode = code;
    }
    public RespOutMsgHeader(Integer msgCount, Boolean status, String msg, String token,Integer code){
        this.respTime = TurnDateToStringUtil.getTime();
        this.respMsgCount = msgCount;
        this.respStatus = status;
        this.msg = msg;
        this.token = token;
        respCode = code;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public Integer getRespMsgCount() {
        return respMsgCount;
    }

    public void setRespMsgCount(Integer respMsgCount) {
        this.respMsgCount = respMsgCount;
    }

    public Boolean getRespStatus() {
        return respStatus;
    }

    public void setRespStatus(Boolean respStatus) {
        this.respStatus = respStatus;
    }

    public Integer getRespCode() {
        return respCode;
    }

    public void setRespCode(Integer respCode) {
        this.respCode = respCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
