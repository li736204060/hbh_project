package com.lzx.hbh_system.util;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

@Slf4j
public class TimetUtil {
    static final String DAYS = "days"; // 计算天数差
    static final String HOURS = "hours"; // 计算小时差
    static final String MINUTES= "minutes"; // 计算分钟差
    static final String MIlLS= "mills"; // 计算毫秒差



    /**
     *
     * @param start 起始时间
     * @param end 结束时间
     * @param pattern 时间格式
     * @param getTimetype 需要获取的时间类型 （天数差-小时差-分钟差-毫秒差）
     * @return
     */
    public static Integer getDaysBetweenDate(Date start, Date end,String pattern,String getTimetype){
        SimpleDateFormat simpleFormat = new SimpleDateFormat(pattern == null ? "yyyy-MM-dd hh:mm" : pattern);//如2016-08-10 20:40
        String startDate = simpleFormat.format(start);
        String endDate = simpleFormat.format(end);
        int timeBetween = 0;
        try {
            long from = simpleFormat.parse(startDate).getTime();
            long to = simpleFormat.parse(endDate).getTime();
            if (DAYS.equals(getTimetype)){
                timeBetween = (int) ((to - from)/(1000 * 60 * 60 * 24));
            }else if(HOURS.equals(getTimetype)){
                timeBetween = (int) ((to - from)/(1000 * 60 * 60));
            }else if(MIlLS.equals(getTimetype)){
                timeBetween = (int)to - (int)from;
            }else{
                timeBetween = (int) ((to - from)/(1000 * 60));
            }
            return timeBetween;
        } catch (ParseException e) {
            e.printStackTrace();
            return timeBetween;
        }
    }

    /**
     * 获取对应时间格式的Date类型
     * @param date
     * @param pattern default yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date getDatePattern(Date date,String pattern){
        // 设置日期格式
        SimpleDateFormat df = null;
        try {
            String time = null;
            df = new SimpleDateFormat(pattern == null ? "yyyy-MM-dd HH:mm:ss" : pattern);
            time = df.format(date);
            date = df.parse(time);
        } catch (Exception e) {
            log.error("时间格式化出现异常！");
        }
        return date;
    }

    /**
     * 转换时间字符格式反回对应的格式的时间字符串
     * tips:暂时支持 转化 “yyyy-MM-dd'T'HH:mm:ss.SSSZ”的时间字符串类型格式
     * @param sourceDate 源时间字符串
     * @param pattern 需要转换的格式 默认 yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String transferTimeFormat(String sourceDate,String pattern){
        if (sourceDate == null || "".equals(sourceDate)){
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date date = new Date();
//            String time = dateFormat.format(date);
//            log.info("当前 sourceDate 为空，默认返回当前时间。");
            return null;
        }
        if (Objects.equals(pattern, "default") || pattern.equals("")){
            pattern = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat dfs = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);// 输入的被转化的时间格式
        SimpleDateFormat df1 = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = dfs.parse(sourceDate);
        } catch (ParseException e) {
            e.printStackTrace();
            log.error("错误："+e.getMessage());
        }
        System.out.println(df1.format(date));
        return df1.format(date);
    }

    public static void main(String[] args) {
        System.out.println(TimetUtil.transferTimeFormat("2022-03-31T16:00:00.000Z","default"));
    }
}
