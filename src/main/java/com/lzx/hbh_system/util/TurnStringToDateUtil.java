package com.lzx.hbh_system.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TurnStringToDateUtil {
	/**
	 * 获取时间字符串转换返回对应date类型时间 （返回类型为 yyyy-MM-dd HH:mm:ss）
	 *
	 * @return Date
	 * @param strTime String
	 * @throws Exception
	 */
	public static Date setStrToTime(String strTime){
		// 设置日期格式
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 转换为Date类型
		Date date = null;
		try {
			date = format.parse(strTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
	/**
	 * 获取时间字符串转换返回对应date类型时间 （返回类型为 yyyy-MM-dd）
	 *
	 * @return Date
	 * @param strTime String
	 * @Default yyyy-MM-dd
	 * @throws Exception
	 */
	public static Date setStrToTimePattern(String strTime,String pattern){
		// 设置日期格式
		DateFormat format = new SimpleDateFormat(pattern == null ? "yyyy-MM-dd" : pattern);
		// 转换为Date类型
		Date date = null;
		try {
			date = format.parse(strTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
}