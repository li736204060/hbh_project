package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.dict.Dicts;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface DictsService extends IService<Dicts> {
    ResponseView getDictsByType(String dictsType);
    ResponseView getTreeDictsByType(String dictsType);
    ResponseView getTreeMapDictsByType(String dictsType);
}
