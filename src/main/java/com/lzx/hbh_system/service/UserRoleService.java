package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysUserRole;
import com.lzx.hbh_system.bo.filter.UserRoleFilter;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface UserRoleService extends IService<SysUserRole> {
    public ResponseView insertUserRole(UserRoleFilter userRoleFilter);
    public ResponseView getRoleUserCountList();
    ResponseView modifyUserRole(UserRoleFilter filter);
}
