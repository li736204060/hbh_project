package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysRoleMenu;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface RoleMenuService extends IService<SysRoleMenu> {
    public ResponseView dropRoleMenuByRoleCode(RequestView requestView);
    public ResponseView addRoleMenuFilter(ResponseView responseView);
}
