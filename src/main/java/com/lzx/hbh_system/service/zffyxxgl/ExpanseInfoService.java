package com.lzx.hbh_system.service.zffyxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.zffyxxgl.ExpanseInfoFilter;
import com.lzx.hbh_system.bo.zffyxxgl.ExpanseInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface ExpanseInfoService extends IService<ExpanseInfo> {
    ResponseView queryAllExpanseInfoPageFilter(ExpanseInfoFilter filter);
    ResponseView queryAllExpanseInfo();
    ResponseView queryAllExpanseInfoByHousehold(ExpanseInfoFilter filter);
    ResponseView addOneExpanseInfo(ExpanseInfoFilter filter);
    ResponseView modifyOneExpanseInfo(ExpanseInfoFilter filter);
    ResponseView dropBatchExpanseInfo(ExpanseInfoFilter filter);
}
