package com.lzx.hbh_system.service.zffyxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.zffyxxgl.PayInfoFilter;
import com.lzx.hbh_system.bo.zffyxxgl.PayInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface PayInfoService extends IService<PayInfo> {
    ResponseView queryAllPayInfoPageFilter(PayInfoFilter filter);
    ResponseView queryAllCurrentQuarterPayInfoByHouseHoldCode(PayInfoFilter filter);
    ResponseView queryAllHasPaidPayInfoByHouseHoldCode(PayInfoFilter filter);
    ResponseView processingPay(PayInfoFilter filter);
    ResponseView addBatchPayInfo(PayInfoFilter filter);
    ResponseView modifyBatchPayInfo(PayInfoFilter filter);
    ResponseView dropBatchPayInfo(PayInfoFilter filter);
}
