package com.lzx.hbh_system.service.zffyxxgl;

import com.lzx.hbh_system.bo.filter.zffyxxgl.HeatingAndPayInfoFilter;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface HeatingAndPayInfoSerivce {
    ResponseView getAllHeatingAndPayInfo(HeatingAndPayInfoFilter filter);
}
