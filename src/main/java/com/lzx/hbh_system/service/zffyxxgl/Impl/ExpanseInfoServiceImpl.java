package com.lzx.hbh_system.service.zffyxxgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.zffyxxgl.ExpanseInfoFilter;
import com.lzx.hbh_system.bo.zffyxxgl.ExpanseInfo;
import com.lzx.hbh_system.dto.zffyxxgl.PayInfoDto;
import com.lzx.hbh_system.mapper.zffyxxgl.ExpanseInfoMapper;
import com.lzx.hbh_system.service.zffyxxgl.ExpanseInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class ExpanseInfoServiceImpl extends ServiceImpl<ExpanseInfoMapper, ExpanseInfo> implements ExpanseInfoService {
    @Autowired
    private ExpanseInfoMapper expanseInfoMapper;
    @Override
    public ResponseView queryAllExpanseInfoPageFilter(ExpanseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<ExpanseInfo> expanseInfoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<ExpanseInfo> expanseInfoIPage = expanseInfoMapper.selectAllExpanseInfoPageFilter(expanseInfoPage,filter);
        // 处理结果集
        List<ExpanseInfo> expanseInfos = expanseInfoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(expanseInfos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", expanseInfos.toArray());
        map.put("currentPage",expanseInfoIPage.getCurrent());
        map.put("totalsData",expanseInfoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllExpanseInfo() {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<ExpanseInfo> expanseInfos = expanseInfoMapper.selectList(null);
        List<Map<String,Object>> mapList = new ArrayList<>();
        expanseInfos.forEach(item ->{
            Map<String,Object> map = new HashMap<>();
            map.put("expanseInfoId",item.getId());
            map.put("expenseName",item.getExpenseName());
            map.put("expenseType",item.getExpenseType());
            map.put("expenseModle",item.getExpenseModle());
            map.put("amountPrice",item.getAmountPrice());
            map.put("level",item.getLevel());
            mapList.add(map);
        });
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(mapList.toArray());
        return responseView;
    }

    @Override
    public ResponseView queryAllExpanseInfoByHousehold(ExpanseInfoFilter filter) {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<PayInfoDto> expansePayInfos = expanseInfoMapper.selectAllExpanseInfoByHouseholdCode(filter);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(expansePayInfos.toArray());
        return responseView;
    }

    @Override
    public ResponseView addOneExpanseInfo(ExpanseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        ExpanseInfo expanseInfo = filter.getExpanseInfo();
        String villageInfoKey = SnowFlakeUtil.getId(); // 生成雪花ID
        expanseInfo.setId(villageInfoKey); // 主键
        expanseInfo.setCreatTime(TurnDateToStringUtil.getTime());
        expanseInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        expanseInfoMapper.insert(expanseInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneExpanseInfo(ExpanseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        ExpanseInfo expanseInfo = filter.getExpanseInfo();
        expanseInfo.setUpdateTime(TurnDateToStringUtil.getTime()); // 设置更新时间
        expanseInfoMapper.updateById(expanseInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchExpanseInfo(ExpanseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> expanseInfoIdList = filter.getExpanseInfoIdList();
        // 逻辑实现
        expanseInfoMapper.deleteBatchIds(expanseInfoIdList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }

    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, ExpanseInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getExpanseInfoIdList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("费用项目信息编号不能为空！"));
            return responseView;
        }
//        else if (Objects.equals(operaType, "00") && filter.g() == null ){
//            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("小区信息编号不能为空！"));
//            return responseView;
//        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
