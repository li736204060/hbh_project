package com.lzx.hbh_system.service.zffyxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.zffyxxgl.PayOrderInfoFilter;
import com.lzx.hbh_system.bo.zffyxxgl.PayOrderInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface PayOrderInfoService extends IService<PayOrderInfo> {
    ResponseView queryAllPayOrderInfoPageFilter(PayOrderInfoFilter filter);
    ResponseView addOnePayOrderInfo(PayOrderInfoFilter filter);
    ResponseView modifyBatchPayOrderInfo(PayOrderInfoFilter filter);
}
