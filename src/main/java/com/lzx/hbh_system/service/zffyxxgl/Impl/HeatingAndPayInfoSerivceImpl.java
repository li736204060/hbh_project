package com.lzx.hbh_system.service.zffyxxgl.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzx.hbh_system.bo.filter.zffyxxgl.HeatingAndPayInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.VillageInfo;
import com.lzx.hbh_system.bo.zffyxxgl.PayOrderInfo;
import com.lzx.hbh_system.dto.zffyxxgl.HeatingAndPayInfoDto;
import com.lzx.hbh_system.mapper.xqzhxxgl.HeatingInfoMapper;
import com.lzx.hbh_system.mapper.xqzhxxgl.HouseInfoMapper;
import com.lzx.hbh_system.mapper.xqzhxxgl.HouseholdInfoMapper;
import com.lzx.hbh_system.mapper.xqzhxxgl.VillageInfoMapper;
import com.lzx.hbh_system.mapper.zffyxxgl.PayInfoMapper;
import com.lzx.hbh_system.mapper.zffyxxgl.PayOrderInfoMapper;
import com.lzx.hbh_system.service.zffyxxgl.HeatingAndPayInfoSerivce;
import com.lzx.hbh_system.util.ListUtils;
import com.lzx.hbh_system.util.TimetUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.TurnStringToDateUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.math.BigDecimal;
import java.util.*;

@Service
@Slf4j
public class HeatingAndPayInfoSerivceImpl implements HeatingAndPayInfoSerivce {
    @Autowired
    private HouseholdInfoMapper householdInfoMapper;
    @Autowired
    private HeatingInfoMapper heatingInfoMapper;
    @Autowired
    private VillageInfoMapper villageInfoMapper;
    @Autowired
    private PayOrderInfoMapper payOrderInfoMapper;
    @Override
    public ResponseView getAllHeatingAndPayInfo(HeatingAndPayInfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 逻辑实现
        // 获取所有小区
        List<VillageInfo> villageInfoList = villageInfoMapper.selectList(null);
        List<HeatingAndPayInfoDto> list = new ArrayList<>();
        villageInfoList.forEach(item ->{
            // 初始化一个出参实体
            HeatingAndPayInfoDto heatingAndPayInfoDto = new HeatingAndPayInfoDto();
            Integer villageHouseholdCount = getVillageHouseholdCount(item); // 该小区的住户个数
            BigDecimal villageHouseholdHeatingAreaCount = getVillageHouseholdAllHeatingArea(item); // 该小区的总供热面积
            Map<String,Object> HeatingAmountCountAndToDayMap = getVillageHouseholdHeatingAmountCountAndToDayMap(item); // 获取总金额以及今日统计户数、面积、费用Map
            BigDecimal VillageHouseholdHeatingAmountCount = (BigDecimal) HeatingAmountCountAndToDayMap.get("heatingAmountCount"); // 获取总缴费金额
            Integer todayPaidHouseholdCount = (Integer) HeatingAmountCountAndToDayMap.get("todayHouseholdCount");// 该小区中今日缴费的用户个数
            BigDecimal todayPaidHeatingAreaCount = (BigDecimal) HeatingAmountCountAndToDayMap.get("todayPaidHeatingArea"); // 该小区中今日缴费的面积
            BigDecimal todayPaidAmountCount = (BigDecimal) HeatingAmountCountAndToDayMap.get("todayPaidAmountCount"); // 该小区中今日缴费的金额
            Map<String,Object> CumulativeDataMap = getCumulativeDataMap(item);
            // 累计缴费户数
            Integer CumulativePaidHousehold = (Integer) CumulativeDataMap.get("hasPaidHousehold");
            // 累计缴费面积
            BigDecimal CumulativePaidHeatingAreaCount = (BigDecimal) CumulativeDataMap.get("hasPaidHeatingAreaCount");
            // 实例化出参实体
            heatingAndPayInfoDto.setAllHouseholdCount(villageHouseholdCount);
            heatingAndPayInfoDto.setAllHouseholdHeatingAreaCount(villageHouseholdHeatingAreaCount);
            heatingAndPayInfoDto.setVillageName(item.getVillageName());
            heatingAndPayInfoDto.setAllHouseholdPaidAmountCount(VillageHouseholdHeatingAmountCount);
            heatingAndPayInfoDto.setTodayPaidHouseHoldCount(todayPaidHouseholdCount);
            heatingAndPayInfoDto.setTodayPaidHeatingAreaCount(todayPaidHeatingAreaCount);
            heatingAndPayInfoDto.setTodayPaidAmountCount(todayPaidAmountCount);
            heatingAndPayInfoDto.setCumulativePaidHouseholdCount(CumulativePaidHousehold);
            heatingAndPayInfoDto.setCumulativePaidHeatingAreaCount(CumulativePaidHeatingAreaCount);
            list.add(heatingAndPayInfoDto);
        });
        // 总数据集 - 手动分页
        List<HeatingAndPayInfoDto> heatingAndPayInfoDtos = ListUtils.pageList(list,filter.getPageIndex(),filter.getPageSize());
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(heatingAndPayInfoDtos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", heatingAndPayInfoDtos.toArray());
        map.put("currentPage",filter.getPageIndex());
        map.put("totalsData",list.size());
        responseView.setMain(map);
        return responseView;
    }

    /**
     * 获取该小区的累计数据
     * @return
     */
    public Map<String,Object> getCumulativeDataMap(VillageInfo villageInfo){
        // 查询该小区的住户
        QueryWrapper<HouseholdInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("village_code",villageInfo.getId());
        List<HouseholdInfo> householdInfoList = householdInfoMapper.selectList(queryWrapper);
        Integer hasPaidHousehold = 0; // 已缴费住户数量
        BigDecimal hasPaidHeatingAreaCount = new BigDecimal("0.00"); // 已缴费住户的供热面积总和
        Map<String,Object> map = new HashMap<>();
        for (HouseholdInfo householdInfo : householdInfoList){
            if (Objects.equals(householdInfo.getHouseholdStatus(), "2")){
                // 住户状态为 已缴费
                hasPaidHousehold += 1;
                QueryWrapper<HeatingInfo> HeatingInfoqueryWrapper = new QueryWrapper<>();
                HeatingInfoqueryWrapper.eq("household_code",householdInfo.getId());
                HeatingInfo heatingInfo = heatingInfoMapper.selectOne(HeatingInfoqueryWrapper);
                hasPaidHeatingAreaCount = hasPaidHeatingAreaCount.add(heatingInfo.getHeatArea());
            }
        }
        map.put("hasPaidHousehold",hasPaidHousehold);
        map.put("hasPaidHeatingAreaCount",hasPaidHeatingAreaCount);
        return map;
    }
    /**
     * 获取总金额以及今日统计户数以及今日统计户数、面积、费用Map
     * @param villageInfo
     * @return
     */
    public Map<String,Object> getVillageHouseholdHeatingAmountCountAndToDayMap(VillageInfo villageInfo){
        // 查询该小区的住户
        QueryWrapper<HouseholdInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("village_code",villageInfo.getId());
        List<HouseholdInfo> householdInfoList = householdInfoMapper.selectList(queryWrapper);
        List<PayOrderInfo> payOrderInfoList = new ArrayList<>(); // 统计今日缴费用户订单
        BigDecimal todayPaidHeatingArea = new BigDecimal("0.00"); // 统计今日缴费面积
        BigDecimal todayPaidAmountCount = new BigDecimal("0.00"); // 统计今日缴费金额
        Map<String,Object> map = new HashMap<>();
        // 获取住户的账单信息
        BigDecimal heatingAmountCount = new BigDecimal("0.00");
        for (HouseholdInfo householdInfo : householdInfoList){
            QueryWrapper<PayOrderInfo> queryWrapperPayOrderInfo = new QueryWrapper<>();
            queryWrapperPayOrderInfo.eq("household_code",householdInfo.getId());
            PayOrderInfo payOrderInfo = payOrderInfoMapper.selectOne(queryWrapperPayOrderInfo);
            if (payOrderInfo == null){
                continue;
            }
            if (TurnDateToStringUtil.getTimeTo_Y_M_D(TurnStringToDateUtil.setStrToTimePattern(payOrderInfo.getOrderTime(),"yyyy-MM-dd")).equals(TurnDateToStringUtil.getTimeTo_Y_M_D())){
                // 相同则是今日缴费订单
                payOrderInfoList.add(payOrderInfo);
                // 获取今日缴费的总面积
                QueryWrapper<HeatingInfo> heatingInfoQueryWrapper = new QueryWrapper<>();
                heatingInfoQueryWrapper.eq("household_code",payOrderInfo.getHouseholdCode());
                HeatingInfo heatingInfo = heatingInfoMapper.selectOne(heatingInfoQueryWrapper);
                todayPaidHeatingArea = todayPaidHeatingArea.add(heatingInfo.getHeatArea());
            }
            // 获取今日缴费的金额
            for(PayOrderInfo payOrderInfoobj : payOrderInfoList){
                todayPaidAmountCount = todayPaidAmountCount.add(payOrderInfoobj.getOrderPayAmount());
            }
            heatingAmountCount = heatingAmountCount.add(payOrderInfo.getOrderPayAmount()); // 总金额
        }
        map.put("heatingAmountCount",heatingAmountCount);
        map.put("todayHouseholdCount",payOrderInfoList.size());
        map.put("todayPaidHeatingArea",todayPaidHeatingArea);
        map.put("todayPaidAmountCount",todayPaidAmountCount);
        return map;
    }
    /**
     * 获取对应小区的住户总户数
     * @param villageInfo
     * @return
     */
    public Integer getVillageHouseholdCount(VillageInfo villageInfo){
        // 查询该小区的住户
        QueryWrapper<HouseholdInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("village_code",villageInfo.getId());
        List<HouseholdInfo> householdInfoList = householdInfoMapper.selectList(queryWrapper);
        return householdInfoList.size();
    }

    /**
     * 获取对应小区的住户总供热面积
     * @param villageInfo
     * @return
     */
    public BigDecimal getVillageHouseholdAllHeatingArea(VillageInfo villageInfo){
        // 查询该小区的住户
        QueryWrapper<HouseholdInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("village_code",villageInfo.getId());
        List<HouseholdInfo> householdInfoList = householdInfoMapper.selectList(queryWrapper);
        BigDecimal heatingAreaCount = new BigDecimal("0.00");
        for (HouseholdInfo householdInfo : householdInfoList){
            QueryWrapper<HeatingInfo> queryWrapperHeating = new QueryWrapper<>();
            queryWrapperHeating.eq("household_code",householdInfo.getId());
            HeatingInfo heatingInfo = heatingInfoMapper.selectOne(queryWrapperHeating);
            BigDecimal oneHeatingArea = heatingInfo.getHeatArea(); // 得到一个住户的供热面积
            heatingAreaCount = heatingAreaCount.add(oneHeatingArea);
        }
        return heatingAreaCount;
    }
}
