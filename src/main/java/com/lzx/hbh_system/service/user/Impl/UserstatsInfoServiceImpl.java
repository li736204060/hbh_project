package com.lzx.hbh_system.service.user.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.UserstatsInfoFilter;
import com.lzx.hbh_system.bo.user.UserStatsInfo;
import com.lzx.hbh_system.dto.UserStatsDto;
import com.lzx.hbh_system.mapper.user.UserstatsInfoMapper;
import com.lzx.hbh_system.service.user.UserstatsInfoService;
import com.lzx.hbh_system.util.TimetUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
@Slf4j
public class UserstatsInfoServiceImpl extends ServiceImpl<UserstatsInfoMapper, UserStatsInfo> implements UserstatsInfoService {
    // 设置默认请求参数类型
    private static String DEFAULT_QUERT_TYPE = "line";
    @Autowired
    private UserstatsInfoMapper userstatsInfoMapper;

    /**
     * 统计数据表的数据
     * @param filter userCode
     * @return
     */
    @Override
    public ResponseView getStatsinfo(String queryType,UserstatsInfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 逻辑处理
        UserStatsDto userStatsDto = statsHandle(queryType,filter);
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"信息查询成功！"));
        // 设置返回结果
        responseView.setMain(userStatsDto); // 返回结果
        return responseView;
    }

    /**
     * 逻辑处理函数
     * @param filter
     * @return
     */
    public UserStatsDto statsHandle(String queryType,UserstatsInfoFilter filter){
        // 初始化查询Wrapper对象
        QueryWrapper<UserStatsInfo> wrapper = new QueryWrapper<>();
        if ("01".equals(filter.getStatisSearchType())){ // 01 - 获取 （当天在线总时长 - 时间 ） 关系
            wrapper.eq("user_code",filter.getUserCode()); // 设置查询条件
            wrapper.orderByAsc("create_time"); // 升序 - 记录日期
            List<UserStatsInfo> userStatsInfoList = userstatsInfoMapper.selectList(wrapper); // 执行查询
            UserStatsDto userStatsDto = new UserStatsDto(); // 初始化Dto映射对象
            List<String> createTimeList = new ArrayList<>(); // 初始化时间列表 x-轴
            List<Integer> onlineTimeDataList = new ArrayList<>(); // 初始化数据列表 y-轴
            userStatsInfoList.forEach(userStatsInfo -> { // 遍历数据库统计表数据 获取对应字段信息
                String createTime = TurnDateToStringUtil.getTimeTo_Y_M_D(userStatsInfo.getCreateTime()); // 获取数据统计当天
                Integer onlineTimeData =  userStatsInfo.getInlineTime();
                createTimeList.add(createTime);
                onlineTimeDataList.add(onlineTimeData);
            });
            // 设置响应实体属性
            // 设置出参 - 数据名称
            userStatsDto.setName(filter.getUserCode());
            // 设置出参 - 数据内容列表
            userStatsDto.setData(onlineTimeDataList);
            // 设置出参 - 数据时间列表
            userStatsDto.setTime(createTimeList);
            // 设置出参 - 默认类型 可根据请求参数动态改变数据展示类型 默认是 line 折线刑
            userStatsDto.setType(queryType != null ? queryType : DEFAULT_QUERT_TYPE);
            // tips ：添加时 需要确保添加的数据是一天一条 如果存在会出现重复数据，影响前端图表或数据处理显示问题
            // 这里暂时直接返回查询的该用户的统计数据
            return userStatsDto;
        }else{
            return null;
        }
    }
}
