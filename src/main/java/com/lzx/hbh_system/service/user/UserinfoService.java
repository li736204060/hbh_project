package com.lzx.hbh_system.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.UserinfoFilter;
import com.lzx.hbh_system.bo.user.Userinfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface UserinfoService extends IService<Userinfo> {
    ResponseView getAllUserinfoFilter(UserinfoFilter filter);
    ResponseView getOneUserinfoFilter(UserinfoFilter filter);
    ResponseView addOneUserinfoFilter(UserinfoFilter filter);
    ResponseView modifyOneUserinfoFilter(UserinfoFilter filter);
    ResponseView modifyOneUserAvartarInfoFilter(UserinfoFilter filter);
}
