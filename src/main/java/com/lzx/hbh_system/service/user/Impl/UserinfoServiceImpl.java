package com.lzx.hbh_system.service.user.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.UserinfoFilter;
import com.lzx.hbh_system.bo.user.Userinfo;
import com.lzx.hbh_system.bo.user.UseroperaInfo;
import com.lzx.hbh_system.mapper.user.UserinfoMapper;
import com.lzx.hbh_system.mapper.user.UseroperaInfoMapper;
import com.lzx.hbh_system.service.user.UserinfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class UserinfoServiceImpl extends ServiceImpl<UserinfoMapper,Userinfo> implements UserinfoService {
    private static final String EVCT_OBJECTNAME = "userinfo";
    @Autowired
    private UserinfoMapper userinfoMapper; // 用户信息表
    @Autowired
    private ModelMapper modelMapper; // 对象信息拷贝工具
    @Autowired
    private UseroperaInfoMapper useroperaInfoMapper; // 用户操作信息表
    /**
     * 查询所有的用户信息表数据 -- 未测试
     * @param filter
     * @return
     */
    @Override
    public ResponseView getAllUserinfoFilter(UserinfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 逻辑实现
        // 分页 - index 从 1 开始 ，size 每页数据条数
        Page<Userinfo> page = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // queryWrapper 过滤结果 具体用法详情：https://blog.csdn.net/qq_18671415/article/details/109071446
        // 添加过滤结果 -- 这里过滤所以vali_flag == '1'
        QueryWrapper<Userinfo> wrapper = new QueryWrapper<>();
        wrapper.eq("vali_flag","1");
        Page<Userinfo> userinfoPage = userinfoMapper.selectPage(page,wrapper);
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success((int)userinfoPage.getTotal(),true,"查询成功！"));
        // 设置返回结果
        responseView.setMain(userinfoPage);
        return responseView;
    }

    /**
     * 获取一个用户信息表数据 -- 未测试
     * @param filter
     * @return
     */
    @Override
    public ResponseView getOneUserinfoFilter(UserinfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 逻辑实现
        System.out.println(filter.getUserCode());
        Userinfo userinfo = userinfoMapper.queryOneUserinfoFilter(filter.getUserCode());
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"查询成功！"));
        // 设置返回结果
        responseView.setMain(userinfo);
        return responseView;
    }

    /**
     * 添加 一个用户信息
     * @param filter
     * @return
     */
    @Override
    public ResponseView addOneUserinfoFilter(UserinfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        String id = SnowFlakeUtil.getId(); // 生成不唯一编号
        String infoKey = "Userinfo_"+id;
        Userinfo userinfo = new Userinfo();
        modelMapper.map(filter.getUserinfo(),userinfo);// 数据拷贝
        // 逻辑实现
        // 校验
        if(checkParma("01",filter,userinfo).equals("1111")){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"存在重复数据",500));
            return responseView;
        }
        userinfo.setInfoCode(infoKey); // 设置主键
        userinfo.setCreatTime(new Date()); // 设置创建时间
        userinfo.setId(id);// 设置唯一标识
        userinfo.setValiFlag("1"); // 默认有效
        userinfoMapper.insert(userinfo);
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"查询成功！"));
        // 设置返回结果
        responseView.setMain(userinfo);
        return responseView;
    }

    /**
     * 修改用户信息表 -- 加入用户操作信息 （修改）
     * @param filter
     * @return
     */
    @Override
    public ResponseView modifyOneUserinfoFilter(UserinfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if(checkParma("03",filter,filter.getUserinfo()).equals("1000")){
            String errormsg = "用户编号为空，或者请求参数的对象为空";
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"修改失败！",500));
//            insertEvtOpera_info("03",filter,errormsg); // 事件加入
            return responseView;
        }
        Userinfo destUserinfo = userinfoMapper.queryOneUserinfoFilter(filter.getUserinfo().getUserCode() == null ? filter.getUserCode() : filter.getUserinfo().getUserCode());
        String destCode = destUserinfo.getInfoCode(); // 因为会覆盖了 ，出现infocode为空的情况 因此先保存一下
        modelMapper.map(filter.getUserinfo(),destUserinfo); // 信息拷贝
        destUserinfo.setInfoCode(destCode); // 重新赋值回去
        destUserinfo.setUpdateTime(new Date()); // 设置新增修改时间
        userinfoMapper.updateById(destUserinfo);
//        insertEvtOpera_info("03",filter,null); // 事件加入
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"修改信息成功！"));
        // 设置返回结果
        responseView.setMain(destUserinfo); // 返回修改结果
        return responseView;
    }

    /**
     * 用户头像信息上传 - 修改用户信息表
     * @param filter
     * @return
     */
    @Override
    public ResponseView modifyOneUserAvartarInfoFilter(UserinfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 校验函数
        if(filter.getUserinfo() == null ){
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"请求参数userinfo不能为空，请检查！",500));
            return responseView;
        }
        if(filter.getUserinfo().getUserCode() == null ){
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"请求参数userCode不能为空，请检查！",500));
            return responseView;
        }
        if(filter.getUserinfo().getAvatar() == null ){
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"请求参数avatar不能为空，请检查！",500));
            return responseView;
        }
        Userinfo userinfo = userinfoMapper.queryOneUserinfoFilter(filter.getUserinfo().getUserCode());
        userinfo.setAvatar(filter.getUserinfo().getAvatar()); // 更新用户头像地址
        userinfo.setAvatarGiteeSha(filter.getUserinfo().getAvatarGiteeSha()); // 更新用户头像sha值
        userinfoMapper.updateById(userinfo);
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"用户头像信息上传成功！"));
        return responseView;
    }


    /**
     * 校验函数
     * @param OpType
     * @param filter
     * @return 1111 数据库存在重复数据 ，1000 请求参数为空，0000 参数正常
     */
    public String checkParma(String OpType,UserinfoFilter filter,Userinfo userinfo){
        if(filter.getUserinfo() == null){
            log.error("请求参数不能为空：checkParma: filter.getUserinfo()");
            return "1000";
        }
        if (OpType.equals("01") || OpType.equals("02")){ // 增加或者删除校验 （01-增 02-删除 03-修改）
            //是否重复
            if (userinfoMapper.queryOneUserinfoFilter(userinfo.getUserCode()) != null){
                log.error("数据库数据重复：checkParma: userinfoMapper.queryOneUserinfoFilter(userinfo.getUserCode())");
                return "1111";
            }
        }else{ // 修改校验
            if (userinfo.getUserCode() == null){
                log.error("请求用户的用户编号为空：checkParma: userinfo.getUserCode()");
                return "1000";
            }
        }
        // 返回响应消息
        return "0000";
    }
    /**
     * 加入操作事件
     * OperaResult（成功-1 失败（出错）-0 未知 - 11）
     */
//    public void insertEvtOpera_info(String OpType,UserinfoFilter filter,String erromsg){
//        UseroperaInfo useroperaInfo = new UseroperaInfo();
//        String id = SnowFlakeUtil.getId();
//        useroperaInfo.setId(id); // 设置操作信息唯一编号
//        useroperaInfo.setOperaCode("Operainfo_"+id);// 设置操作信息主键
//        useroperaInfo.setCreatTime(new Date());// 设置操作信息创建时间
//        useroperaInfo.setOperaType(OpType);// 设置操作信息操作类型编号
//        useroperaInfo.setErrorMsg(erromsg);// 设置操作信息错误信息
//        useroperaInfo.setOperaResult(erromsg != null ? "0" : "1");// 设置操作结果信息
//        useroperaInfo.setValiFlag("1");// 设置操作信息有效标识
//        useroperaInfo.setUserCode(filter.getUserinfo().getUserCode() == null ? filter.getUserCode() : filter.getUserinfo().getUserCode());// 设置操作信息关联用户编号
//        if("01".equals(OpType)){ // 增事件
//            useroperaInfo.setOperaTitle("增加信息操作"); // 设置操作信息名称
//            useroperaInfo.setOperaContent("用户["+filter.getUserinfo().getUserCode()+"]，昵称："+filter.getUserinfo().getName()+"，向表["+EVCT_OBJECTNAME+"]进行了信息insert操作。"); // 设置操作信息操作内容
//            useroperaInfoMapper.insert(useroperaInfo); // 加入事件
//        }else if("02".equals(OpType)){ // 删事件
//            useroperaInfo.setOperaTitle("删除信息操作"); // 设置操作信息名称
//            useroperaInfo.setOperaContent("用户["+filter.getUserinfo().getUserCode()+"]，昵称："+filter.getUserinfo().getName()+"，向表["+EVCT_OBJECTNAME+"]进行了信息delete操作。"); // 设置操作信息操作内容
//            useroperaInfoMapper.insert(useroperaInfo); // 加入事件
//        }else{ // 改事件
//            useroperaInfo.setOperaTitle("修改信息操作"); // 设置操作信息名称
//            useroperaInfo.setOperaContent("用户["+filter.getUserinfo().getUserCode()+"]，昵称："+filter.getUserinfo().getName()+"，向表["+EVCT_OBJECTNAME+"]进行了信息update操作。"); // 设置操作信息操作内容
//            useroperaInfoMapper.insert(useroperaInfo); // 加入事件
//        }
//    }
}
