package com.lzx.hbh_system.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.UseracivityInfoFilter;
import com.lzx.hbh_system.bo.user.UseractivityInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface UseracivityInfoService extends IService<UseractivityInfo> {
    ResponseView queryAllByFilter(UseracivityInfoFilter filter);
    ResponseView addOneByFilter(UseracivityInfoFilter filter);
    ResponseView modifyByFilter(UseracivityInfoFilter filter);
    ResponseView deleteByFilter(UseracivityInfoFilter filter);
    ResponseView handleByFilter(UseracivityInfoFilter filter);
}
