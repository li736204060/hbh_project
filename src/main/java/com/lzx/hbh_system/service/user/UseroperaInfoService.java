package com.lzx.hbh_system.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.UseroperaInfoFilter;
import com.lzx.hbh_system.bo.user.UseroperaInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface UseroperaInfoService extends IService<UseroperaInfo> {
    ResponseView getSomeListByFilter(UseroperaInfoFilter filter);
    Boolean saveLogDataToUserOperainfo(UseroperaInfo info);
}
