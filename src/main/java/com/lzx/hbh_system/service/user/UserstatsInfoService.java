package com.lzx.hbh_system.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.UserstatsInfoFilter;
import com.lzx.hbh_system.bo.user.UserStatsInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface UserstatsInfoService extends IService<UserStatsInfo> {
    ResponseView getStatsinfo(String queryType,UserstatsInfoFilter filter);
}
