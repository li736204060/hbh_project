package com.lzx.hbh_system.service.user.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.SysRole;
import com.lzx.hbh_system.bo.filter.UseracivityInfoFilter;
import com.lzx.hbh_system.bo.user.UseractivityInfo;
import com.lzx.hbh_system.bo.user.UserpictureInfo;
import com.lzx.hbh_system.dto.UseractivityInfoDto;
import com.lzx.hbh_system.mapper.user.UseractivityInfoMapper;
import com.lzx.hbh_system.mapper.user.UserpictureInfoMapper;
import com.lzx.hbh_system.service.user.UseracivityInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class UseracivityInfoServiceImpl extends ServiceImpl<UseractivityInfoMapper, UseractivityInfo> implements UseracivityInfoService {
    // 静态判断参数
    static final String LIKE = "like";
    static final String DISLIKE = "dislike";
    static final String TOP = "top";
    static final String UNTOP = "untop";
    @Autowired
    private UseractivityInfoMapper useractivityInfoMapper;
    @Autowired
    private UserpictureInfoMapper userpictureInfoMapper;
    @Autowired
    private ModelMapper modelMapper;

    /**
     * 查询所以留言板信息 分页
     * @param filter
     * @return
     */
    @Override
    public ResponseView queryAllByFilter(UseracivityInfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 逻辑实现
        // 初始化分页对象
        Page<UseractivityInfoDto> InfoDtoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<UseractivityInfoDto> useractivityInfoDtoIPage = useractivityInfoMapper.selectAllByFilter(InfoDtoPage,filter);
        // 处理结果集
        List<UseractivityInfoDto> useractivityInfoDtoss = useractivityInfoDtoIPage.getRecords();
        List<UseractivityInfoDto> useractivityInfoDtos = new ArrayList<>();
        useractivityInfoDtoss.forEach(activityDtoObj -> {
            UseractivityInfoDto dto = new UseractivityInfoDto();
            List<String> PictureUrlList = getUserPictureUrlList(activityDtoObj.getActivityUserCode());
            // 对象信息拷贝
            modelMapper.map(activityDtoObj,dto);
            dto.setPictureUrlList(PictureUrlList);
            useractivityInfoDtos.add(dto);
        });
//        System.out.println(useractivityInfoDtos);
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(useractivityInfoDtos.size(),true,"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", useractivityInfoDtos.toArray());
        map.put("currentPage",useractivityInfoDtoIPage.getCurrent());
        map.put("totalsData",useractivityInfoDtoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    /**
     * 增加一个留言板信息
     * @param filter
     * @return
     */
    @Override
    public ResponseView addOneByFilter(UseracivityInfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 校验参数
        if (filter.getUseractivityInfo() == null){
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"请求的留言板对象信息不能为空",500));
            return responseView;
        }
        // 逻辑处理
        UseractivityInfo newuserActivityInfo = modelMapper.map(filter.getUseractivityInfo(),UseractivityInfo.class) ;
        // 获取主键
        String idkey = SnowFlakeUtil.getId();
        // 设置主键
        newuserActivityInfo.setId(idkey);
        newuserActivityInfo.setActivityCode("ActivityCode_"+idkey);
        newuserActivityInfo.setCreateTime(TurnDateToStringUtil.getTime()); // 设置留言时间
        newuserActivityInfo.setFirstFlag("0"); // 默认置顶标志 为 0 - 不置顶
        newuserActivityInfo.setDislikeCount(0); // 默认记录数 0
        newuserActivityInfo.setLikeCount(0);// 默认记录数 0
        newuserActivityInfo.setValiFlag("1"); // 默认有效
        System.out.println(newuserActivityInfo);
        // 保存数据
        useractivityInfoMapper.insert(newuserActivityInfo);
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"留言成功！"));
        return responseView;
    }

    /**
     * 修改留言板
     * @param filter
     * @return
     */
    @Override
    public ResponseView modifyByFilter(UseracivityInfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 校验参数
        if (filter.getUseractivityInfo() == null){
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"请求的留言板对象信息不能为空",500));
            return responseView;
        }
        // 逻辑处理
        useractivityInfoMapper.updateById(filter.getUseractivityInfo());
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"修改成功！"));
        return responseView;
    }

    /**
     * 删除
     * @param filter
     * @return
     */
    @Override
    public ResponseView deleteByFilter(UseracivityInfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 校验参数
        if (filter.getUseractivityInfoCode() == null){
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"请求的留言板对象编号不能为空",500));
            return responseView;
        }
        // 逻辑处理 -- 逻辑删除
        UseractivityInfo useractivityInfo = useractivityInfoMapper.selectById(filter.getUseractivityInfoCode());
        useractivityInfo.setValiFlag("0"); // 设置失效
        // 执行
        useractivityInfoMapper.updateById(useractivityInfo);
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"删除成功！"));
        return responseView;
    }

    /**
     * 点赞 和 置顶 是否 函数
     * @param filter
     * @return
     */
    @Override
    public ResponseView handleByFilter(UseracivityInfoFilter filter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 校验参数
        if (filter.getHandleType() == null && filter.getUseractivityInfoCode() == null){
            // 返回响应消息
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"请求的执行操作类型信息和对象编号不能为空",500));
            return responseView;
        }
        // 逻辑处理
        if (LIKE.equals(filter.getHandleType())){
            // 点击了喜欢
            UseractivityInfo useractivityInfo = useractivityInfoMapper.selectById(filter.getUseractivityInfoCode());
            Integer like = useractivityInfo.getLikeCount();
            like = like + 1; // 喜欢记录加一
            useractivityInfo.setLikeCount(like);
            useractivityInfoMapper.updateById(useractivityInfo);
        }else if (DISLIKE.equals(filter.getHandleType())){
            // 点击了不喜欢
            UseractivityInfo useractivityInfo = useractivityInfoMapper.selectById(filter.getUseractivityInfoCode());
            Integer dislike = useractivityInfo.getDislikeCount();
            dislike = dislike + 1; // 不喜欢记录加一
            useractivityInfo.setDislikeCount(dislike);
            useractivityInfoMapper.updateById(useractivityInfo);
        }else if (TOP.equals(filter.getHandleType())){
            // 点击了置顶
            UseractivityInfo useractivityInfo = useractivityInfoMapper.selectById(filter.getUseractivityInfoCode());
            useractivityInfo.setFirstFlag("1");
            useractivityInfoMapper.updateById(useractivityInfo);
        }else if (UNTOP.equals(filter.getHandleType())){
            // 点击了取消置顶
            UseractivityInfo useractivityInfo = useractivityInfoMapper.selectById(filter.getUseractivityInfoCode());
            useractivityInfo.setFirstFlag("0");
            useractivityInfoMapper.updateById(useractivityInfo);
        }
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"操作成功！"));
        return responseView;
    }

    /**
     * 根据用户编号获取用户上传图片信息对象列表
     * @param userCode
     * @return
     */
    public List<String> getUserPictureUrlList(String userCode){
        List<String> UserPictureUrlList = new ArrayList<>();
        QueryWrapper<UserpictureInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("user_code",userCode);
        List<UserpictureInfo> userpictureInfos = userpictureInfoMapper.selectList(wrapper);
        userpictureInfos.forEach(obj -> {
            UserPictureUrlList.add(obj.getPictureUrl());
        });
        return UserPictureUrlList;
    }

}
