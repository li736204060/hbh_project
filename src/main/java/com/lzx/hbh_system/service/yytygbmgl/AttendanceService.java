package com.lzx.hbh_system.service.yytygbmgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.yytygbmgl.AttendenceInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.cqgl.AttendanceInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface AttendanceService extends IService<AttendanceInfo> {
    ResponseView queryAllAttendanceInfoPageFilter(AttendenceInfoFilter filter);
    ResponseView queryAllAttendanceInfo();
    ResponseView queryOneAttendanceInfo(AttendenceInfoFilter filter);
    ResponseView addOneAttendanceInfoFilter(AttendenceInfoFilter filter);
    ResponseView modifyOneAttendanceInfoFilter(AttendenceInfoFilter filter);
    ResponseView dropBatchAttendanceInfoFilter(AttendenceInfoFilter filter);
}
