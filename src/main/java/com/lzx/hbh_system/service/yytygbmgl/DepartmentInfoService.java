package com.lzx.hbh_system.service.yytygbmgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysMenu;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface DepartmentInfoService extends IService<DepartmentInfo> {
    ResponseView queryAllDepPageFilter(DepartmentInfoFilter filter);
    ResponseView queryAllDep();
    ResponseView addOneDepFilter(DepartmentInfoFilter filter);
    ResponseView dropBatchDepFilter(DepartmentInfoFilter filter);
    ResponseView modifyOneDepFilter(DepartmentInfoFilter filter);
}
