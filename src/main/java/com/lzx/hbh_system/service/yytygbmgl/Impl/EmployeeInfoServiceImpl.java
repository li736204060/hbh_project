package com.lzx.hbh_system.service.yytygbmgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.qjgl.VacationInfo;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import com.lzx.hbh_system.dto.yytygbmgl.EmployeeInfoDto;
import com.lzx.hbh_system.mapper.yytygbmgl.EmployeeInfoMapper;
import com.lzx.hbh_system.service.yytygbmgl.EmployeeInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class EmployeeInfoServiceImpl extends ServiceImpl<EmployeeInfoMapper, EmployeeInfo> implements EmployeeInfoService {
    @Autowired
    private EmployeeInfoMapper employeeInfoMapper;
    @Autowired
    private ModelMapper modelMapper;
    @Override
    public ResponseView queryAllEmpInfoPageFilter(EmployeeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<EmployeeInfo> employeeInfoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<EmployeeInfo> employeeInfoIPage = employeeInfoMapper.selectAllEmpInfoPageFilter(employeeInfoPage,filter);
        // 处理结果集
        List<EmployeeInfo> employeeInfos = employeeInfoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(employeeInfos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", employeeInfos.toArray());
        map.put("currentPage",employeeInfoIPage.getCurrent());
        map.put("totalsData",employeeInfoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllEmpInfo() {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<EmployeeInfoDto> employeeInfoDtos = employeeInfoMapper.selectAllEmpSimpleList();
        List<Map<String,Object>> mapList = new ArrayList<>();
        employeeInfoDtos.forEach(item ->{
            Map<String,Object> map = new HashMap<>();
            map.put("empCode",item.getEmployCode());
            map.put("userInfoName",item.getUserName());
            mapList.add(map);
        });
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(mapList.toArray());
        return responseView;
    }

    @Override
    public ResponseView addOneEmpInfoFilter(EmployeeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        EmployeeInfo employeeInfo = filter.getEmployeeInfo();
        String EmpKey = SnowFlakeUtil.getId(); // 生成雪花ID
        employeeInfo.setId(EmpKey);
        employeeInfo.setEmployCode("Emp_"+EmpKey); // 主键
        employeeInfo.setCreateTime(TurnDateToStringUtil.getTime()); // 创建时间
        employeeInfo.setUpdateTime(null); // 创建时间
        employeeInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        employeeInfoMapper.insert(employeeInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneEmpInfoFilter(EmployeeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        EmployeeInfo employeeInfo = filter.getEmployeeInfo();
        employeeInfo.setUpdateTime(TurnDateToStringUtil.getTime());
        employeeInfoMapper.updateById(employeeInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchEmpInfoFilter(EmployeeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> empCodeList = filter.getEmpCodeList();
        // 逻辑实现
        employeeInfoMapper.deleteBatchIds(empCodeList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }

    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, EmployeeInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getEmpCodeList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("员工编号不能为空！"));
            return responseView;
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
