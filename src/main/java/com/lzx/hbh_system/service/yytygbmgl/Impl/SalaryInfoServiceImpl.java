package com.lzx.hbh_system.service.yytygbmgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.yytygbmgl.SalaryInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.gzgl.SalaryInfo;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import com.lzx.hbh_system.mapper.yytygbmgl.EmployeeInfoMapper;
import com.lzx.hbh_system.mapper.yytygbmgl.SalaryInfoMapper;
import com.lzx.hbh_system.service.yytygbmgl.SalaryInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class SalaryInfoServiceImpl extends ServiceImpl<SalaryInfoMapper, SalaryInfo> implements SalaryInfoService {
    @Autowired
    private SalaryInfoMapper salaryInfoMapper;
    @Autowired
    private EmployeeInfoMapper employeeInfoMapper;
    @Override
    public ResponseView queryAllSalaryInfoPageFilter(SalaryInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<SalaryInfo> salaryInfoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<SalaryInfo> salaryInfoIPage = salaryInfoMapper.selectAllSalaryInfoPageFilter(salaryInfoPage,filter);
        // 处理结果集
        List<SalaryInfo> salaryInfos = salaryInfoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(salaryInfos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", salaryInfos.toArray());
        map.put("currentPage",salaryInfoIPage.getCurrent());
        map.put("totalsData",salaryInfoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllSalaryInfo() {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<SalaryInfo> salaryInfos = salaryInfoMapper.selectList(null);
        List<Map<String,Object>> mapList = new ArrayList<>();
        salaryInfos.forEach(item ->{
            Map<String,Object> map = new HashMap<>();
            map.put("salaryCode",item.getSalaryCode());
            map.put("empCode",item.getEmpCode());
            map.put("tips","接口功能待完善！");
            mapList.add(map);
        });
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(mapList.toArray());
        return responseView;
    }

    @Override
    public ResponseView addOneSalaryInfoFilter(SalaryInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        SalaryInfo salaryInfo = filter.getSalaryInfo();
        String SalaryInfoKey = SnowFlakeUtil.getId(); // 生成雪花ID
        String codeKey = "SalaryInfo_"+SalaryInfoKey;
        salaryInfo.setId(SalaryInfoKey);
        salaryInfo.setSalaryCode(codeKey); // 主键
        salaryInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        salaryInfoMapper.insert(salaryInfo);
        // 同步数据到员工表中
        EmployeeInfo employeeInfo = employeeInfoMapper.selectById(filter.getSalaryInfo().getEmpCode());
        employeeInfo.setSalaryCode(codeKey);
        employeeInfoMapper.updateById(employeeInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneSalaryInfoFilter(SalaryInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        SalaryInfo salaryInfo = filter.getSalaryInfo();
        salaryInfoMapper.updateById(salaryInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchSalaryInfoFilter(SalaryInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> salaryInfoCodeList = filter.getSalaryInfoCodeList();
        // 逻辑实现
        salaryInfoMapper.deleteBatchIds(salaryInfoCodeList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, SalaryInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getSalaryInfoCodeList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("工资信息编号不能为空！"));
            return responseView;
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
