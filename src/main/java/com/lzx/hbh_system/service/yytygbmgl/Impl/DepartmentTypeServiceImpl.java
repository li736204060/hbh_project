package com.lzx.hbh_system.service.yytygbmgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentTypeFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentInfo;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentType;
import com.lzx.hbh_system.mapper.yytygbmgl.DepartmentTypeMapper;
import com.lzx.hbh_system.service.yytygbmgl.DepartmentTypeService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TimetUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class DepartmentTypeServiceImpl extends ServiceImpl<DepartmentTypeMapper,DepartmentType> implements DepartmentTypeService {
    @Autowired
    private DepartmentTypeMapper departmentTypeMapper;
    @Override
    public ResponseView queryAllDepTypePageFilter(DepartmentTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<DepartmentType> departmentTypePage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<DepartmentType> departmentTypeIPage = departmentTypeMapper.selectAllDepTypePageFilter(departmentTypePage,filter);
        // 处理结果集
        List<DepartmentType> departmentTypes = departmentTypeIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(departmentTypes.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", departmentTypes.toArray());
        map.put("currentPage",departmentTypeIPage.getCurrent());
        map.put("totalsData",departmentTypeIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView addOneDepTypeFilter(DepartmentTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        DepartmentType departmentType = filter.getDepartmentType();
        String DepKey = SnowFlakeUtil.getId(); // 生成雪花ID
        departmentType.setId(DepKey);
        departmentType.setDepTypeCode("DepType_"+DepKey); // 主键
        departmentType.setCreatTime(TurnDateToStringUtil.getTime()); // 创建时间
        departmentType.setValiFlag("1"); // 有效标识
        // 保存数据库
        departmentTypeMapper.insert(departmentType);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchDepTypeFilter(DepartmentTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> depTypeList = filter.getDepTypeList();
        // 逻辑实现
        departmentTypeMapper.deleteBatchIds(depTypeList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneDepTypeFilter(DepartmentTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        DepartmentType departmentType = filter.getDepartmentType();
        departmentType.setUpdateTime(TurnDateToStringUtil.getTime());
        departmentTypeMapper.updateById(departmentType);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView queryAllDepType() {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        List<DepartmentType> departmentTypes = departmentTypeMapper.selectList(null);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功！"));
        responseView.setMain(departmentTypes.toArray());
        return responseView;
    }

    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, DepartmentTypeFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getDepTypeList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("部门编号不能为空！"));
            return responseView;
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
