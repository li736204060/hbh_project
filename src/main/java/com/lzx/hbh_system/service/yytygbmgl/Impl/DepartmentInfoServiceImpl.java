package com.lzx.hbh_system.service.yytygbmgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentInfo;
import com.lzx.hbh_system.dto.yytygbmgl.DepartmentInfoDto;
import com.lzx.hbh_system.mapper.yytygbmgl.DepartmentInfoMapper;
import com.lzx.hbh_system.service.yytygbmgl.DepartmentInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class DepartmentInfoServiceImpl extends ServiceImpl<DepartmentInfoMapper, DepartmentInfo> implements DepartmentInfoService {
    @Autowired
    private DepartmentInfoMapper departmentInfoMapper;
    @Override
    public ResponseView queryAllDepPageFilter(DepartmentInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<DepartmentInfoDto> departmentInfoDtoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<DepartmentInfoDto> departmentInfoDtoIPage = departmentInfoMapper.selectAllDepartmentInfoPageFilter(departmentInfoDtoPage,filter);
        // 处理结果集
        List<DepartmentInfoDto> departmentInfoDtos = departmentInfoDtoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(departmentInfoDtos.size(),true,"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", departmentInfoDtos.toArray());
        map.put("currentPage",departmentInfoDtoIPage.getCurrent());
        map.put("totalsData",departmentInfoDtoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllDep() {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<DepartmentInfo> departmentInfos = departmentInfoMapper.selectList(null);
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(departmentInfos.size(),"查询成功"));
        responseView.setMain(departmentInfos.toArray());
        return responseView;
    }

    @Override
    public ResponseView addOneDepFilter(DepartmentInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        DepartmentInfo departmentInfo = filter.getDepartmentInfo();
        String DepKey = SnowFlakeUtil.getId(); // 生成雪花ID
        departmentInfo.setId(DepKey); // 唯一标识
        departmentInfo.setDepCode("dep_DepKey"+DepKey); // 设置主键
        departmentInfo.setValiFlag("1"); // 新增 - 默认 有效
        // 保存数据库
        departmentInfoMapper.insert(departmentInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchDepFilter(DepartmentInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> depCodeList = filter.getBmbhList();
        // 逻辑实现
        departmentInfoMapper.deleteBatchIds(depCodeList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneDepFilter(DepartmentInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        DepartmentInfo departmentInfo = filter.getDepartmentInfo();
        departmentInfoMapper.updateById(departmentInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType,DepartmentInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getBmbhList().size() == 0){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("部门编号不能为空！"));
            return responseView;
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
