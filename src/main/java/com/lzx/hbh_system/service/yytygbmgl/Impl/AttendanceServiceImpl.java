package com.lzx.hbh_system.service.yytygbmgl.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.yytygbmgl.AttendenceInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.cqgl.AttendanceInfo;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import com.lzx.hbh_system.mapper.yytygbmgl.AttendanceInfoMapper;
import com.lzx.hbh_system.mapper.yytygbmgl.EmployeeInfoMapper;
import com.lzx.hbh_system.service.yytygbmgl.AttendanceService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TimetUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.TurnStringToDateUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class AttendanceServiceImpl extends ServiceImpl<AttendanceInfoMapper, AttendanceInfo> implements AttendanceService {
    @Autowired
    private AttendanceInfoMapper attendanceInfoMapper;
    @Autowired
    private EmployeeInfoMapper employeeInfoMapper;
    @Override
    public ResponseView queryAllAttendanceInfoPageFilter(AttendenceInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<AttendanceInfo> attendanceInfoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<AttendanceInfo> attendanceInfoIPage = attendanceInfoMapper.selectAllAttendanceInfoPageFilter(attendanceInfoPage,filter);
        // 处理结果集
        List<AttendanceInfo> attendanceInfos = attendanceInfoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(attendanceInfos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", attendanceInfos.toArray());
        map.put("currentPage",attendanceInfoIPage.getCurrent());
        map.put("totalsData",attendanceInfoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllAttendanceInfo() {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<AttendanceInfo> attendanceInfos = attendanceInfoMapper.selectList(null);
        List<Map<String,Object>> mapList = new ArrayList<>();
        attendanceInfos.forEach(item ->{
            Map<String,Object> map = new HashMap<>();
            map.put("attendCode",item.getAttendCode());
            map.put("empCode",item.getEmpCode());
            map.put("tips","接口功能待完善！");
            mapList.add(map);
        });
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(mapList.toArray());
        return responseView;
    }

    @Override
    public ResponseView queryOneAttendanceInfo(AttendenceInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("04",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        QueryWrapper<AttendanceInfo> queryWrapper = new QueryWrapper();
        queryWrapper.eq("attend_code",filter.getCqbh());
        AttendanceInfo attendanceInfo = attendanceInfoMapper.selectOne(queryWrapper);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,"查询成功"));
        responseView.setMain(attendanceInfo);
        return responseView;

    }

    @Override
    public ResponseView addOneAttendanceInfoFilter(AttendenceInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        AttendanceInfo attendanceInfo = filter.getAttendanceInfo();
        String AttendanceInfoKey = SnowFlakeUtil.getId(); // 生成雪花ID
        String codeKey = "AttendanceInfo_"+AttendanceInfoKey;
        attendanceInfo.setId(AttendanceInfoKey);
        attendanceInfo.setAttendCode(codeKey); // 主键
        attendanceInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        attendanceInfoMapper.insert(attendanceInfo);
          // 同步数据到员工表中
        EmployeeInfo employeeInfo = employeeInfoMapper.selectById(filter.getAttendanceInfo().getEmpCode());
        employeeInfo.setAttendCode(codeKey);
        employeeInfoMapper.updateById(employeeInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneAttendanceInfoFilter(AttendenceInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        AttendanceInfo attendanceInfo = filter.getAttendanceInfo();
        attendanceInfoMapper.updateById(attendanceInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchAttendanceInfoFilter(AttendenceInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> attendanceCodeList = filter.getAttendanceCodeList();
        // 逻辑实现
        attendanceInfoMapper.deleteBatchIds(attendanceCodeList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  04 查询单个
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, AttendenceInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getAttendanceCodeList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("出勤信息编号不能为空！"));
            return responseView;
        }else if (Objects.equals(operaType, "04") && filter.getCqbh() == null ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("出勤信息编号不能为空！"));
            return responseView;
        }else if (Objects.equals(operaType, "03")){
            // 查询是否已经存在出勤信息且未过期、 若是 则不能添加相同员工出勤信息
            QueryWrapper<AttendanceInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("emp_code",filter.getAttendanceInfo().getEmpCode());
            AttendanceInfo attendanceInfo = attendanceInfoMapper.selectOne(queryWrapper);
            if (attendanceInfo != null ){
                // 获取现在时间 判断是否大于出勤结束时间
                Integer betweenDays = TimetUtil.getDaysBetweenDate(attendanceInfo.getAttendEndTime(),new Date(),null,"days");
                if (betweenDays <= 0){
                    responseView.setRespOutMsgHeader(RespOutMsgHeader.error("当前员工出勤尚未过期！"));
                    return responseView;
                }
            }
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
