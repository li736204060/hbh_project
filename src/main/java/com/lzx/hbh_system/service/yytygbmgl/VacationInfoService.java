package com.lzx.hbh_system.service.yytygbmgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeTypeFilter;
import com.lzx.hbh_system.bo.filter.yytygbmgl.VacationInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.qjgl.VacationInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface VacationInfoService extends IService<VacationInfo>{
    ResponseView queryAllVacationInfoPageFilter(VacationInfoFilter filter);
    ResponseView queryAllVacationInfo();
    ResponseView queryOneVavationInfo(VacationInfoFilter filter);
    ResponseView addOneVacationInfoFilter(VacationInfoFilter filter);
    ResponseView modifyOneVacationInfoFilter(VacationInfoFilter filter);
    ResponseView dropBatchVacationInfoFilter(VacationInfoFilter filter);
}
