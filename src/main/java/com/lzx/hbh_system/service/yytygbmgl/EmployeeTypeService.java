package com.lzx.hbh_system.service.yytygbmgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeTypeFilter;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeType;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface EmployeeTypeService extends IService<EmployeeType> {
    ResponseView queryAllEmpTypePageFilter(EmployeeTypeFilter filter);
    ResponseView queryAllEmpType();
    ResponseView addOneEmpTypeFilter(EmployeeTypeFilter filter);
    ResponseView modifyOneEmpTypeFilter(EmployeeTypeFilter filter);
    ResponseView dropBatchEmpTypeFilter(EmployeeTypeFilter filter);
}
