package com.lzx.hbh_system.service.yytygbmgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentTypeFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentType;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface DepartmentTypeService extends IService<DepartmentType> {
    ResponseView queryAllDepTypePageFilter(DepartmentTypeFilter filter);
    ResponseView queryAllDepType();
    ResponseView addOneDepTypeFilter(DepartmentTypeFilter filter);
    ResponseView dropBatchDepTypeFilter(DepartmentTypeFilter filter);
    ResponseView modifyOneDepTypeFilter(DepartmentTypeFilter filter);
}
