package com.lzx.hbh_system.service.yytygbmgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.yytygbmgl.SalaryInfoFilter;
import com.lzx.hbh_system.bo.filter.yytygbmgl.VacationInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.gzgl.SalaryInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface SalaryInfoService extends IService<SalaryInfo> {
    ResponseView queryAllSalaryInfoPageFilter(SalaryInfoFilter filter);
    ResponseView queryAllSalaryInfo();
    ResponseView addOneSalaryInfoFilter(SalaryInfoFilter filter);
    ResponseView modifyOneSalaryInfoFilter(SalaryInfoFilter filter);
    ResponseView dropBatchSalaryInfoFilter(SalaryInfoFilter filter);
}
