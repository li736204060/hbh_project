package com.lzx.hbh_system.service.yytygbmgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface EmployeeInfoService extends IService<EmployeeInfo> {
    ResponseView queryAllEmpInfoPageFilter(EmployeeInfoFilter filter);
    ResponseView queryAllEmpInfo();
    ResponseView addOneEmpInfoFilter(EmployeeInfoFilter filter);
    ResponseView modifyOneEmpInfoFilter(EmployeeInfoFilter filter);
    ResponseView dropBatchEmpInfoFilter(EmployeeInfoFilter filter);
}
