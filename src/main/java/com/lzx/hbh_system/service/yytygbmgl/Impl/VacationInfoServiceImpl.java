package com.lzx.hbh_system.service.yytygbmgl.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.yytygbmgl.VacationInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.cqgl.AttendanceInfo;
import com.lzx.hbh_system.bo.yytygbmgl.qjgl.VacationInfo;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import com.lzx.hbh_system.mapper.yytygbmgl.EmployeeInfoMapper;
import com.lzx.hbh_system.mapper.yytygbmgl.VacationInfoMapper;
import com.lzx.hbh_system.service.yytygbmgl.VacationInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TimetUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class VacationInfoServiceImpl extends ServiceImpl<VacationInfoMapper, VacationInfo> implements VacationInfoService {
    @Autowired
    private VacationInfoMapper vacationInfoMapper;
    @Autowired
    private EmployeeInfoMapper employeeInfoMapper;
    @Override
    public ResponseView queryAllVacationInfoPageFilter(VacationInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<VacationInfo> vacationInfoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<VacationInfo> vacationInfoIPage = vacationInfoMapper.selectAllVacationInfoPageFilter(vacationInfoPage,filter);
        // 处理结果集
        List<VacationInfo> vacationInfos = vacationInfoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(vacationInfos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", vacationInfos.toArray());
        map.put("currentPage",vacationInfoIPage.getCurrent());
        map.put("totalsData",vacationInfoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllVacationInfo() {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<VacationInfo> vacationInfos = vacationInfoMapper.selectList(null);
        List<Map<String,Object>> mapList = new ArrayList<>();
        vacationInfos.forEach(item ->{
            Map<String,Object> map = new HashMap<>();
            map.put("vacationCode",item.getVacateCode());
            map.put("empCode",item.getEmpCode());
            map.put("tips","接口功能待完善！");
            mapList.add(map);
        });
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(mapList.toArray());
        return responseView;
    }

    @Override
    public ResponseView queryOneVavationInfo(VacationInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("04",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        QueryWrapper<VacationInfo> queryWrapper = new QueryWrapper ();
        queryWrapper.eq("vacate_code",filter.getQjbh());
        VacationInfo vacationInfo = vacationInfoMapper.selectOne(queryWrapper);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,"查询成功"));
        responseView.setMain(vacationInfo);
        return responseView;
    }

    @Override
    public ResponseView addOneVacationInfoFilter(VacationInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        VacationInfo vacationInfo = filter.getVacationInfo();
        String VacateInfoKey = SnowFlakeUtil.getId(); // 生成雪花ID
        String codeKey = "VacateInfo_"+VacateInfoKey;
        vacationInfo.setId(VacateInfoKey);
        vacationInfo.setVacateCode(codeKey); // 主键
        vacationInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        vacationInfoMapper.insert(vacationInfo);
        // 同步数据到员工表中
        EmployeeInfo employeeInfo = employeeInfoMapper.selectById(filter.getVacationInfo().getEmpCode());
        employeeInfo.setEmployVacateCode(codeKey);
        employeeInfoMapper.updateById(employeeInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneVacationInfoFilter(VacationInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        VacationInfo vacationInfo = filter.getVacationInfo();
        vacationInfoMapper.updateById(vacationInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchVacationInfoFilter(VacationInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> vacationInfoCodeList = filter.getVacationInfoCodeList();
        // 逻辑实现
        vacationInfoMapper.deleteBatchIds(vacationInfoCodeList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  04 查询单个
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, VacationInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getVacationInfoCodeList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("请假信息编号不能为空！"));
            return responseView;
        }else if (Objects.equals(operaType, "04") && filter.getQjbh() == null ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("请假信息编号不能为空！"));
            return responseView;
        }else if (Objects.equals(operaType, "03")){
            // 查询是否已经存在出勤信息且未过期、 若是 则不能添加相同员工出勤信息
            QueryWrapper<VacationInfo> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("emp_code",filter.getVacationInfo().getEmpCode());
            VacationInfo vacationInfo = vacationInfoMapper.selectOne(queryWrapper);
            if (vacationInfo != null ){
                // 获取现在时间 判断是否大于出勤结束时间
                Integer betweenDays = TimetUtil.getDaysBetweenDate(vacationInfo.getVacateEndTime(),new Date(),null,"days");
                if (betweenDays <= 0){
                    responseView.setRespOutMsgHeader(RespOutMsgHeader.error("当前员工以存在请假信息且尚未过期！"));
                    return responseView;
                }
            }
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
