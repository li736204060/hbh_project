package com.lzx.hbh_system.service.yytygbmgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeTypeFilter;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeType;
import com.lzx.hbh_system.mapper.yytygbmgl.EmployeeTypeMapper;
import com.lzx.hbh_system.service.yytygbmgl.EmployeeTypeService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class EmployeeTypeServiceImpl extends ServiceImpl<EmployeeTypeMapper, EmployeeType> implements EmployeeTypeService {
    @Autowired
    private EmployeeTypeMapper employeeTypeMapper;
    @Override
    public ResponseView queryAllEmpTypePageFilter(EmployeeTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<EmployeeType> employeeTypePage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<EmployeeType> employeeTypeIPage = employeeTypeMapper.selectAllEmpTypePageFilter(employeeTypePage,filter);
        // 处理结果集
        List<EmployeeType> employeeTypes = employeeTypeIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(employeeTypes.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", employeeTypes.toArray());
        map.put("currentPage",employeeTypeIPage.getCurrent());
        map.put("totalsData",employeeTypeIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllEmpType() {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        List<EmployeeType> employeeTypeList = employeeTypeMapper.selectList(null);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功！"));
        responseView.setMain(employeeTypeList.toArray());
        return responseView;
    }

    @Override
    public ResponseView addOneEmpTypeFilter(EmployeeTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        EmployeeType employeeType = filter.getEmployeeType();
        String EmpTypeKey = SnowFlakeUtil.getId(); // 生成雪花ID
        employeeType.setId(EmpTypeKey);
        employeeType.setEmployTypeCode("EmpType_"+EmpTypeKey); // 主键
        employeeType.setCreateTime(TurnDateToStringUtil.getTime()); // 创建时间
        employeeType.setUpdateTime(null); // 创建时间
        employeeType.setValiFlag("1"); // 有效标识
        // 保存数据库
        employeeTypeMapper.insert(employeeType);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneEmpTypeFilter(EmployeeTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        EmployeeType employeeType = filter.getEmployeeType();
        employeeType.setUpdateTime(TurnDateToStringUtil.getTime());
        employeeTypeMapper.updateById(employeeType);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchEmpTypeFilter(EmployeeTypeFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> empTypeList = filter.getEmpTypeList();
        // 逻辑实现
        employeeTypeMapper.deleteBatchIds(empTypeList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }

    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, EmployeeTypeFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getEmpTypeList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("员工类型编号不能为空！"));
            return responseView;
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
