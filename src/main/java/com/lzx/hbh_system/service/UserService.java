package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysUser;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

import javax.servlet.http.HttpServletRequest;

public interface UserService extends IService<SysUser> {
    public SysUser getOneUser(SysUser sysUser);
    public ResponseView getAllUserList();
    public ResponseView getOneUserByUsername(Userfilter userfilter);
    public ResponseView CheckLoingUserByNameAndPwd(Userfilter userfilter, HttpServletRequest request);
    public ResponseView loginout(Userfilter userfilter);
    public ResponseView registerUser(Userfilter userfilter);
    public ResponseView modifyUser(Userfilter userfilter);
    ResponseView queryAllUserInfoPageFilter(Userfilter userfilter);
}
