package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysMenu;
import com.lzx.hbh_system.bo.filter.MenuFilter;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface MenuService extends IService<SysMenu> {
   ResponseView getAllMenu(MenuFilter filter);
   ResponseView getAllMenuDto();
   ResponseView getMenuByFilter(MenuFilter filter);
   ResponseView addOneMenuInfo(MenuFilter filter);
   ResponseView getOneMenuInfo(MenuFilter filter);
   ResponseView modifyOneMenuInfo(MenuFilter filter);
   ResponseView dropBatchMenuInfo(MenuFilter filter);
}
