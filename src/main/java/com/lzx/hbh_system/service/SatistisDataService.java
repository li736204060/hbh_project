package com.lzx.hbh_system.service;

import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface SatistisDataService {
    ResponseView getDashBoardData();
}
