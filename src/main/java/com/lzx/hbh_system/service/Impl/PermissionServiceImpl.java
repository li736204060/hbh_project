package com.lzx.hbh_system.service.Impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.SysPermission;
import com.lzx.hbh_system.bo.SysRole;
import com.lzx.hbh_system.bo.filter.PermissionFilter;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.mapper.SysPermissionMapper;
import com.lzx.hbh_system.service.PermissionService;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements PermissionService {
    @Autowired
    private SysPermissionMapper permissionMapper;

    @Override
    public ResponseView getPermissionByUsernameFilter(Userfilter userfilter){
        //初始化响应消息实体
        ResponseView responseView = new ResponseView();
        System.out.println(userfilter.toString());
        //执行查询--应该加入校验函数为前提，这里先进行直接查询，后期加入校验。
        List<SysPermission> permissionList = permissionMapper.selectPermission_byusername(userfilter);
        //返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(permissionList.size(),true,"查询成功！"));
        //设置返回结果
        responseView.setMain(permissionList);
        return responseView;
    }

    /**
     * JWTtoken验证 内部使用
     * @param username
     * @return
     */
    @Override
    public List<SysPermission> getPermissionByUsername(String username) {
        return permissionMapper.searchAllByUsername(username);
    }

    @Override
    public ResponseView getAllPermissionFilter(PermissionFilter permissionFilter) {
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        // 判断是否开启分页 若为true 则分页 否则查询所有
        if(!permissionFilter.isPage()){
            List<SysPermission> permissionList = permissionMapper.selectList(null);
            responseView.setMain(permissionList.toArray());
            responseView.setRespOutMsgHeader(RespOutMsgHeader.success(permissionList.size(),true,"查询成功！"));
            return responseView;
        }
        // 分页 - index 从 1 开始 ，size 每页数据条数
        Page<SysPermission> page = new Page<>(permissionFilter.getPageIndex(),permissionFilter.getPageSize());
        // queryWrapper 过滤结果 具体用法详情：https://blog.csdn.net/qq_18671415/article/details/109071446
        Page<SysPermission> permissionPage = permissionMapper.selectPage(page,null);
        // 设置查询结果，并返回消息
        responseView.setMain(permissionPage.getRecords().toArray());
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(permissionPage.getRecords().size(),true,"查询成功！"));
        return responseView;
    }
}
