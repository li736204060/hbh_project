package com.lzx.hbh_system.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.SysRole;
import com.lzx.hbh_system.bo.SysUser;
import com.lzx.hbh_system.bo.SysUserRole;
import com.lzx.hbh_system.bo.filter.UserRoleFilter;
import com.lzx.hbh_system.dto.RoleUserCountDto;
import com.lzx.hbh_system.dto.UserDto;
import com.lzx.hbh_system.mapper.SysRoleMapper;
import com.lzx.hbh_system.mapper.SysUserMapper;
import com.lzx.hbh_system.mapper.SysUserRoleMapper;
import com.lzx.hbh_system.service.UserRoleService;
import com.lzx.hbh_system.util.RedisUtils;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class UserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements UserRoleService {
    @Autowired
    private SysUserRoleMapper userRoleMapper;
    @Autowired
    private SysUserMapper UserMapper;
    @Autowired
    private SysRoleMapper sysRoleMapper;
    @Autowired
    private RedisUtils redisUtils;
    @Override
    public ResponseView insertUserRole(UserRoleFilter userRoleFilter) {
        // 初始化
        ResponseView responseView = new ResponseView();
        // 请求体非空校验
        if(ObjectUtils.isEmpty(userRoleFilter)){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"空的请求载体，服务器未接收到请求数据，请重试！",500));
            return responseView;
        }
        // 请求角色主键和用户主键不能为空校验
        if(userRoleFilter.getRoleCode().length() == 0 && userRoleFilter.getUserCode().length() == 0){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"不能设置空角色和空用户，请重试！",500));
            return responseView;
        }
        // 是否重复一个用户同一个角色的校验
        if(!isNotRoleNameRepeat(userRoleFilter)){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false,"同一个用户已经拥有了该角色，请重试！",500));
            return responseView;
        }
        // 初始化角色用户实体
        SysUserRole sysUserRole = new SysUserRole();
        sysUserRole.setId(SnowFlakeUtil.getId()); // 数据唯一标识
        sysUserRole.setUserToRoleCode(userRoleFilter.getUserCode().trim()+":"+userRoleFilter.getRoleCode().trim()); // 设置主键
        sysUserRole.setRoleCode(userRoleFilter.getRoleCode().trim());
        sysUserRole.setUserCode(userRoleFilter.getUserCode().trim());
        userRoleMapper.insert(sysUserRole);
        //返回成功消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(1,true,"用户角色信息添加成功！"));
        return responseView;
    }

    /**
     * 获取角色用户统计数量列表 - 未做redis缓存处理 数据量较小
     * @return
     */
    @Override
    public ResponseView getRoleUserCountList() {
        // 初始化
        ResponseView responseView = new ResponseView();
        // 逻辑处理
        List<RoleUserCountDto> roleUserCountDtos = userRoleMapper.queryALLUserforCount();
        // 消息响应
        // 返回成功消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(roleUserCountDtos.size(),true,"查询成功！"));
        responseView.setMain(roleUserCountDtos.toArray());
        return responseView;
    }

    @Override
    public ResponseView modifyUserRole(UserRoleFilter filter) {
        // 初始化
        ResponseView responseView = new ResponseView();
        // 逻辑处理
        QueryWrapper<SysUserRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_code",filter.getUserCode());
        SysUserRole userRole = userRoleMapper.selectOne(queryWrapper);
        // 修改用户信息角色关联字段
        SysUser sysUser = UserMapper.selectById(filter.getUserCode());
        sysUser.setRoleCode(filter.getRoleCode()); // 更新 新的关联的角色roleCode
        userRole.setRoleCode(filter.getRoleCode()); // 设置新的关联的角色roleCode
        // 保存
        UserMapper.updateById(sysUser);// 保存
        userRoleMapper.updateById(userRole);
        // 消息响应
        // 返回成功消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    //用户是否用户重复角色--用于用户角色添加或者修改时经行校验的函数-已登录的状态或者注册状态也可以进行校验
    public boolean isNotRoleNameRepeat(UserRoleFilter userRoleFilter){
        if (userRoleFilter.isResisting()){
            return true; // 这里的true是让直接不检索redis，因为是注册用户，redis没有数据，避免空指针出现。
        }
        UserDto userDto = (UserDto) redisUtils.getObject(userRoleFilter.getUserName());
        List<String> roleNamelist = userDto.getRoleName();
        if(roleNamelist.contains(userRoleFilter.getRoleName())){
            return false;
        }
        return true;
    }
}
