package com.lzx.hbh_system.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.VillageInfo;
import com.lzx.hbh_system.bo.zffyxxgl.PayInfo;
import com.lzx.hbh_system.bo.zffyxxgl.PayOrderInfo;
import com.lzx.hbh_system.dto.satistiDto.*;
import com.lzx.hbh_system.mapper.xqzhxxgl.HeatingInfoMapper;
import com.lzx.hbh_system.mapper.xqzhxxgl.HouseholdInfoMapper;
import com.lzx.hbh_system.mapper.xqzhxxgl.VillageInfoMapper;
import com.lzx.hbh_system.mapper.zffyxxgl.PayInfoMapper;
import com.lzx.hbh_system.mapper.zffyxxgl.PayOrderInfoMapper;
import com.lzx.hbh_system.service.SatistisDataService;
import com.lzx.hbh_system.util.TimetUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.TurnStringToDateUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SatistisDataServiceImpl implements SatistisDataService {
    @Autowired
    private HouseholdInfoMapper householdInfoMapper; // 住户信息Dao
    @Autowired
    private HeatingInfoMapper heatingInfoMapper; // 供暖信息Dao
    @Autowired
    private VillageInfoMapper villageInfoMapper; // 小区信息Dao
    @Autowired
    private PayOrderInfoMapper payOrderInfoMapper; // 账单Dao
    @Autowired
    private PayInfoMapper payInfoMapper; // 支付Dao
    @Autowired
    private ModelMapper modelMapper;
    @Override
    public ResponseView getDashBoardData() {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = new ResponseView();
        // 逻辑实现
        // 获取首页顶部展示统计条数据
        DashBoardTopDto dashBoardTopDto = new DashBoardTopDto();
        Integer allHouseHoldCount = householdInfoMapper.selectCount(null); // 住户总数量
        BigDecimal allHeatingCount = getallHeatingCount(); // 供暖总量
        BigDecimal allHeatingAreaCount = getallHeatingAreaCount(); // 供暖总面积
        BigDecimal allOrderPaidAmount = getallOrderPaidAmount(); // 收费总额度
        dashBoardTopDto.setAllHeatingCount(allHeatingCount);
        dashBoardTopDto.setAllHeatingAreaCount(allHeatingAreaCount);
        dashBoardTopDto.setAllHouseholdCount(allHouseHoldCount);
        dashBoardTopDto.setAllOrderPaidAmount(allOrderPaidAmount);
        // 获取内容
        // 获取住户注册数量时间折线图数据
        HouseholdTimeAndCountDto householdTimeAndCountDto = getHouseholdTimeAndCount();
        // 获取小区与小区总供暖量的关系折线图数据
        VillageAndHeatingCapacityDto villageAndHeatingCapacityDto = getVillageAndHeatingCapacity();
        // 获取小区与小区总供暖面积的关系折现图数据
        VillageAndHeatingAreaDto villageAndHeatingAreaDto = getVillageAndHeatingArea();
        // 获取账单总额与日期的关系折线图数据
        AmountAndDateDto amountAndDateDto = getAmountAndDateDto();
        // 获取各季度 各费用项目 额度统计分布数据
        QuarterlyStatisticsParentDto quarterlyStatisticsParentDto = getQuarterlyStatisticsAllData();
        // 获取各费用项目总累计营收额度数据
        ExpanseStatisicsParentDto expanseStatisicsParentDto = getExpanseStatisicsAllData();
        // 获取年度各月份营收额度统计数据
        MonthOrderInfoStatisicsDto monthOrderInfoStatisicsDto = getMonthOrderInfoStatisicsData();
        // 首页数据父实体
        DashBoardParentDto dashBoardParentDto = new DashBoardParentDto();
        dashBoardParentDto.setDashBoardTopDto(dashBoardTopDto);
        dashBoardParentDto.setHouseholdTimeAndCountDto(householdTimeAndCountDto);
        dashBoardParentDto.setVillageAndHeatingCapacityDto(villageAndHeatingCapacityDto);
        dashBoardParentDto.setVillageAndHeatingAreaDto(villageAndHeatingAreaDto);
        dashBoardParentDto.setAmountAndDateDto(amountAndDateDto);
        dashBoardParentDto.setQuarterlyStatisticsParentDto(quarterlyStatisticsParentDto);
        dashBoardParentDto.setExpanseStatisicsParentDto(expanseStatisicsParentDto);
        dashBoardParentDto.setMonthOrderInfoStatisicsDto(monthOrderInfoStatisicsDto);
        // 返回视图
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(dashBoardParentDto);
        return responseView;
    }

    /**
     * 获取年度的月份营收统计额度实体
     * @return
     */
    public MonthOrderInfoStatisicsDto getMonthOrderInfoStatisicsData(){
        // 获得所有账单信息
        List<PayOrderInfo> payOrderInfos = payOrderInfoMapper.selectList(null);
        // 过滤是否作废账单
        List<PayOrderInfo> payOrderInfosFilterList =  payOrderInfos.stream().filter(item -> "1".equals(item.getValiFlag())).collect(Collectors.toList());
        // 转换时间格式
        List<PayOrderInfo> payOrderInfosnewList = new ArrayList<>();
        payOrderInfosFilterList.forEach(item ->{
            PayOrderInfo payOrderInfo = modelMapper.map(item,PayOrderInfo.class);
            payOrderInfo.setOrderTime(Integer.toString(getOrderObjMounth(item)));
            payOrderInfosnewList.add(payOrderInfo);
        });
        // 分组取值
        Map<String, List<PayOrderInfo>> PayOrderInfoGroupByMonth = payOrderInfosnewList.stream().collect(Collectors.groupingBy(PayOrderInfo::getOrderTime));
        // 获取统计得月份列表
        List<String> MonthList = PayOrderInfoGroupByMonth.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getKey).collect((Collectors.toList()));
        // 获取统计得月份营收额列表
        List<BigDecimal> OrderAmountValueList = PayOrderInfoGroupByMonth.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().stream().map(PayOrderInfo::getOrderPayAmount).reduce(BigDecimal.ZERO, BigDecimal::add)).collect((Collectors.toList()));
        MonthOrderInfoStatisicsDto monthOrderInfoStatisicsDto = new MonthOrderInfoStatisicsDto();
        monthOrderInfoStatisicsDto.setName(MonthList);
        monthOrderInfoStatisicsDto.setValue(OrderAmountValueList);
        return monthOrderInfoStatisicsDto;
    }
    /**
     * 获取费用项目统计实体
     * @return
     */
    public ExpanseStatisicsParentDto getExpanseStatisicsAllData(){
        // 获取所有支付信息列表
        List<PayInfo> payInfos = payInfoMapper.selectList(null);
        // 过滤已经支付的支付信息
        List<PayInfo> payInfosFitlerList = payInfos.stream().filter(item -> "1".equals(item.getPayStatus())).collect(Collectors.toList());
        // 按费用项目分组统计每个费用类型的总额度
        Map<String, List<PayInfo>> PayInfoGroupByExpanseType = payInfosFitlerList.stream().collect(Collectors.groupingBy(PayInfo::getExpanseCode));
        List<String> ExpanseTypeList = PayInfoGroupByExpanseType.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getKey).collect((Collectors.toList()));
        List<BigDecimal> ExpanseValueList = PayInfoGroupByExpanseType.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().stream().map(PayInfo::getPaidInAmount).reduce(BigDecimal.ZERO, BigDecimal::add)).collect((Collectors.toList()));
        ExpanseStatisicsParentDto expanseStatisicsParentDto = new ExpanseStatisicsParentDto();
        List<ExpanseStatisicsDto> expanseStatisicsDtoList = new ArrayList<>();
        for (int i = 0; i < ExpanseTypeList.size(); i++) {
            ExpanseStatisicsDto expanseStatisicsDto = new ExpanseStatisicsDto();
            if (Objects.equals(ExpanseTypeList.get(i), "1")){
                expanseStatisicsDto.setName("取暖费");
                expanseStatisicsDto.setValue(ExpanseValueList.get(i));
            }else if (Objects.equals(ExpanseTypeList.get(i), "2")){
                expanseStatisicsDto.setName("配套费");
                expanseStatisicsDto.setValue(ExpanseValueList.get(i));
            }else if(Objects.equals(ExpanseTypeList.get(i), "3")){
                expanseStatisicsDto.setName("历史欠费");
                expanseStatisicsDto.setValue(ExpanseValueList.get(i));
            }else if(Objects.equals(ExpanseTypeList.get(i), "4")){
                expanseStatisicsDto.setName("停暖费");
                expanseStatisicsDto.setValue(ExpanseValueList.get(i));
            }else if(Objects.equals(ExpanseTypeList.get(i), "5")){
                expanseStatisicsDto.setName("往年退费");
                expanseStatisicsDto.setValue(ExpanseValueList.get(i));
            }
            expanseStatisicsDtoList.add(expanseStatisicsDto);
        }
        expanseStatisicsParentDto.setExpanseStatisicsDtoList(expanseStatisicsDtoList);
        return expanseStatisicsParentDto;
    }
    /**
     * 获取各季度 各费用项目 额度统计分布数据实体
     * @return
     */
    public QuarterlyStatisticsParentDto getQuarterlyStatisticsAllData(){
        // 获取所有支付信息列表
        List<PayInfo> payInfos = payInfoMapper.selectList(null);
        // 获取当年的每季度数据
        Integer currentYear = Integer.valueOf(TurnDateToStringUtil.getTime("yyyy"));
        // 春季
        List<PayInfo> springPayinfoList = new ArrayList<>();
        // 夏季
        List<PayInfo> summerPayinfoList = new ArrayList<>();
        // 秋季
        List<PayInfo> autumnPayinfoList = new ArrayList<>();
        // 冬季
        List<PayInfo> winterPayinfoList = new ArrayList<>();
        for (PayInfo payInfo : payInfos) {
            if (!Objects.equals(payInfo.getYearsBegin(), currentYear) || payInfo.getPayTime() == null || "0".equals(payInfo.getPayStatus())){
                // 不是现在的年份 或者 没有支付时间的 又或者 支付状态为0的 （未支付） ---》跳过
                continue;
            }
            if ("springQuarter".equals(verifyQuarter(getObjMounth(payInfo)))){
                // 该支付的信息符合条件
                payInfo.setPayTime(Integer.toString(getObjMounth(payInfo)));
                springPayinfoList.add(payInfo);
            }else if("summerQuarter".equals(verifyQuarter(getObjMounth(payInfo)))){
                payInfo.setPayTime(Integer.toString(getObjMounth(payInfo)));
                summerPayinfoList.add(payInfo);
            }else if("autumnQuarter".equals(verifyQuarter(getObjMounth(payInfo)))){
                payInfo.setPayTime(Integer.toString(getObjMounth(payInfo)));
                autumnPayinfoList.add(payInfo);
            }else{
                payInfo.setPayTime(Integer.toString(getObjMounth(payInfo)));
                winterPayinfoList.add(payInfo);
            }
        }
        // 处理每个季度的数据 转换成 Dto类型集合
        String springDataName = "春季";
        String summerDataName = "夏季";
        String autumnDataName = "秋季";
        String winterDataName = "冬季";
        List<BigDecimal> springValue = new ArrayList<>();
        List<BigDecimal> summerValue = new ArrayList<>();
        List<BigDecimal> autumnValue = new ArrayList<>();
        List<BigDecimal> winterValue = new ArrayList<>();
        // 分组统计 各季度 各项目的Map
        Map<String, List<PayInfo>> SpringPayInfoGroupByExpanseType = springPayinfoList.stream().collect(Collectors.groupingBy(PayInfo::getExpanseCode));
        Map<String, List<PayInfo>> SummerPayInfoGroupByExpanseType = summerPayinfoList.stream().collect(Collectors.groupingBy(PayInfo::getExpanseCode));
        Map<String, List<PayInfo>> AutumnPayInfoGroupByExpanseType = autumnPayinfoList.stream().collect(Collectors.groupingBy(PayInfo::getExpanseCode));
        Map<String, List<PayInfo>> WinterPayInfoGroupByExpanseType = winterPayinfoList.stream().collect(Collectors.groupingBy(PayInfo::getExpanseCode));
        // 统计不同的费用项目组的总收费额度
        springValue = SpringPayInfoGroupByExpanseType.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().stream().map(PayInfo::getPaidInAmount).reduce(BigDecimal.ZERO, BigDecimal::add)).collect((Collectors.toList()));
        summerValue = SummerPayInfoGroupByExpanseType.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().stream().map(PayInfo::getPaidInAmount).reduce(BigDecimal.ZERO, BigDecimal::add)).collect((Collectors.toList()));
        autumnValue = AutumnPayInfoGroupByExpanseType.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().stream().map(PayInfo::getPaidInAmount).reduce(BigDecimal.ZERO, BigDecimal::add)).collect((Collectors.toList()));
        winterValue = WinterPayInfoGroupByExpanseType.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().stream().map(PayInfo::getPaidInAmount).reduce(BigDecimal.ZERO, BigDecimal::add)).collect((Collectors.toList()));
        // 实例化出参统计数据类
        QuarterlyStatisticsDto SpringDto = new QuarterlyStatisticsDto();
        SpringDto.setName(springDataName);
        SpringDto.setValue(springValue);
        QuarterlyStatisticsDto SummerDto = new QuarterlyStatisticsDto();
        SummerDto.setName(summerDataName);
        SummerDto.setValue(summerValue);
        QuarterlyStatisticsDto AutumnDto = new QuarterlyStatisticsDto();
        AutumnDto.setName(autumnDataName);
        AutumnDto.setValue(autumnValue);
        QuarterlyStatisticsDto WinterDto = new QuarterlyStatisticsDto();
        WinterDto.setName(winterDataName);
        WinterDto.setValue(winterValue);
        // 返回处理完成的数据
        QuarterlyStatisticsParentDto quarterlyStatisticsParentDto = new QuarterlyStatisticsParentDto();
        quarterlyStatisticsParentDto.setSpringQuarterlyStatistics(SpringDto);
        quarterlyStatisticsParentDto.setSummerQuarterlyStatistics(SummerDto);
        quarterlyStatisticsParentDto.setAutumnQuarterlyStatistics(AutumnDto);
        quarterlyStatisticsParentDto.setWinterQuarterlyStatistics(WinterDto);
        return quarterlyStatisticsParentDto;
    }
    /**
     * 获取PayOrderInfo对象的 ‘支付时间’ 转化获得其中的月份数
     * @param payOrderInfo
     * @return
     */
    public Integer getOrderObjMounth(PayOrderInfo payOrderInfo){
        // 获取当前系统时间对象
        Calendar date = Calendar.getInstance();
        // 获取当前对象的时间对象
        Calendar payInfoDate = Calendar.getInstance();
        payInfoDate.setTime(TurnStringToDateUtil.setStrToTime(payOrderInfo.getOrderTime()));
        return payInfoDate.get(Calendar.MONTH) + 1 ;
    }
    /**
     * 获取PayInfo对象的 ‘支付时间’ 转化获得其中的月份数
     * @param payInfo
     * @return
     */
    public Integer getObjMounth(PayInfo payInfo){
        // 获取当前系统时间对象
        Calendar date = Calendar.getInstance();
        // 获取当前对象的时间对象
        Calendar payInfoDate = Calendar.getInstance();
        payInfoDate.setTime(TurnStringToDateUtil.setStrToTime(payInfo.getPayTime()));
        return payInfoDate.get(Calendar.MONTH) + 1 ;
    }
    /**
     * 季度判断函数
     * @param verifyMounth 校验的月份
     * @return 返回季度说明字符串
     */
    public String verifyQuarter(int verifyMounth){
        String springQuarter = "3,4,5"; // 春
        String summerQuarter = "6,7,8"; // 夏
        String autumnQuarter = "9,10,11"; // 秋
        String winterQuarter = "12,1,2"; // 冬
        if(springQuarter.contains(String.valueOf(verifyMounth))){
            return "springQuarter";
        }else if(summerQuarter.contains(String.valueOf(verifyMounth))){
            return "summerQuarter";
        }else if(autumnQuarter.contains(String.valueOf(verifyMounth))){
            return "autumnQuarter";
        }else{
            return "winterQuarter";
        }
    }
    /**
     * 获取小区-小区供暖面积总量 折线图数据对象
     * @return
     */
    public VillageAndHeatingAreaDto getVillageAndHeatingArea(){
        // 获取所有的小区
        List<VillageInfo> villageInfos = villageInfoMapper.selectList(null);
        VillageAndHeatingAreaDto villageAndHeatingAreaDto = new VillageAndHeatingAreaDto();
        List<String> villageNameList = new ArrayList<>();
        List<BigDecimal> villageAllAreaList = new ArrayList<>();
        villageInfos.forEach(item->{
            // 查询每个小区对应的住户的总供暖面积量
            String villageName = item.getVillageName();
            BigDecimal villageHeatingAreaCount = getVillageHouseholdAllHeatingAreaCount(item);
            villageNameList.add(villageName);
            villageAllAreaList.add(villageHeatingAreaCount);
        });
        villageAndHeatingAreaDto.setVillageHeatingAreaList(villageAllAreaList);
        villageAndHeatingAreaDto.setVillageNameList(villageNameList);
        return villageAndHeatingAreaDto;
    }
    /**
     * 返回关于该小区的总供暖面积量
     * @param villageInfo
     * @return
     */
    public BigDecimal getVillageHouseholdAllHeatingAreaCount(VillageInfo villageInfo){
        // 查询所有住户信息
        QueryWrapper<HouseholdInfo> householdInfoQueryWrapper = new QueryWrapper<>();
        householdInfoQueryWrapper.eq("village_code",villageInfo.getId());
        List<HouseholdInfo> householdInfos = householdInfoMapper.selectList(householdInfoQueryWrapper);
        BigDecimal allHeatingAreaCount = new BigDecimal("0.00"); // 该小区的所有供热量
        for (HouseholdInfo householdInfo : householdInfos) {
            QueryWrapper<HeatingInfo> heatingInfoQueryWrapper = new QueryWrapper<>();
            heatingInfoQueryWrapper.eq("household_code",householdInfo.getId());
            HeatingInfo heatingInfo = heatingInfoMapper.selectOne(heatingInfoQueryWrapper);
            allHeatingAreaCount = allHeatingAreaCount.add(heatingInfo.getHeatArea());
        }
        return allHeatingAreaCount;
    }
    /**
     * 获取小区-小区供暖量 折线图数据对象
     * @return
     */
    public VillageAndHeatingCapacityDto getVillageAndHeatingCapacity(){
        // 获取所有的小区
        List<VillageInfo> villageInfos = villageInfoMapper.selectList(null);
        VillageAndHeatingCapacityDto villageAndHeatingCapacityDto = new VillageAndHeatingCapacityDto();
        List<String> villageNameList = new ArrayList<>();
        List<BigDecimal> villageAllCapacityList = new ArrayList<>();
        villageInfos.forEach(item->{
            // 查询每个小区对应的住户的供暖总量
            String villageName = item.getVillageName();
            BigDecimal villageHeatingCount = getVillageHouseholdAllHeatingCount(item);
            villageNameList.add(villageName);
            villageAllCapacityList.add(villageHeatingCount);
        });
        villageAndHeatingCapacityDto.setVillageAllCapacityList(villageAllCapacityList);
        villageAndHeatingCapacityDto.setVillageNameList(villageNameList);
        return villageAndHeatingCapacityDto;
    }
    /**
     * 返回关于该小区的供暖总量
     * @param villageInfo
     * @return
     */
    public BigDecimal getVillageHouseholdAllHeatingCount(VillageInfo villageInfo){
        // 查询所有住户信息
        QueryWrapper<HouseholdInfo> householdInfoQueryWrapper = new QueryWrapper<>();
        householdInfoQueryWrapper.eq("village_code",villageInfo.getId());
        List<HouseholdInfo> householdInfos = householdInfoMapper.selectList(householdInfoQueryWrapper);
        BigDecimal allHeatingCount = new BigDecimal("0.00"); // 该小区的所有供热量
        for (HouseholdInfo householdInfo : householdInfos) {
            QueryWrapper<HeatingInfo> heatingInfoQueryWrapper = new QueryWrapper<>();
            heatingInfoQueryWrapper.eq("household_code",householdInfo.getId());
            HeatingInfo heatingInfo = heatingInfoMapper.selectOne(heatingInfoQueryWrapper);
            if (heatingInfo.getHeatEquipmentHeatingCapacity() == null){continue;}
            allHeatingCount = allHeatingCount.add(heatingInfo.getHeatEquipmentHeatingCapacity());
        }
        return allHeatingCount;
    }
    /**
     * 获取住户数量-注册时间 折线图数据对象
     * @return
     */
    public HouseholdTimeAndCountDto getHouseholdTimeAndCount(){
        // 获取所有住户
        List<HouseholdInfo> householdInfos = householdInfoMapper.selectList(null);
        return getData(householdInfos);
    }

    /**
     * 获取订单总额-统计天数 折线图数据对象
     * @return
     */
    public AmountAndDateDto getAmountAndDateDto(){
        // 获取所有订单
        List<PayOrderInfo> payOrderInfoList = payOrderInfoMapper.selectList(null);
        return getAmountAndDateData(payOrderInfoList);
    }
//    public static void test(){
//        List<HouseholdInfo> householdInfos = new ArrayList<>();//存放str对象集合
//        HouseholdInfo householdInfo1 = new HouseholdInfo();
//        householdInfo1.setId("weaweaw1");
//        householdInfo1.setCreateTime("2022-04-17 11:22:46");
//        HouseholdInfo householdInfo2 = new HouseholdInfo();
//        householdInfo2.setId("weaweaw2");
//        householdInfo2.setCreateTime("2022-04-17 11:22:26");
//        HouseholdInfo householdInfo3 = new HouseholdInfo();
//        householdInfo3.setId("weaweaw3");
//        householdInfo3.setCreateTime("2022-04-17 11:22:36");
//        HouseholdInfo householdInfo4 = new HouseholdInfo();
//        householdInfo4.setId("weaweaw4");
//        householdInfo4.setCreateTime("2022-04-17 11:22:16");
//        HouseholdInfo householdInfo5 = new HouseholdInfo();
//        householdInfo5.setId("weaweaw5");
//        householdInfo5.setCreateTime("2022-04-15 11:22:56");
//        householdInfos.add(householdInfo1);
//        householdInfos.add(householdInfo2);
//        householdInfos.add(householdInfo3);
//        householdInfos.add(householdInfo4);
//        householdInfos.add(householdInfo5);
//        isSameDay(householdInfos);
//    }

    /**
     * 获取住户数量与日期的关系数据
     * @param householdInfoList
     * @return
     */
    public HouseholdTimeAndCountDto getData(List<HouseholdInfo> householdInfoList){
        HouseholdTimeAndCountDto householdTimeAndCountDto = new HouseholdTimeAndCountDto();
        List<HouseholdInfo> householdInfos = new ArrayList<>();
        // 处理数据、将原有的数据列表的每个数据的创建时间转化成YYYY-MM-DD格式
        for (HouseholdInfo householdInfo : householdInfoList) {
            HouseholdInfo newhousehold = modelMapper.map(householdInfo,HouseholdInfo.class);
            newhousehold.setCreateTime(TurnDateToStringUtil.getTimeTo_Y_M_D(TurnStringToDateUtil.setStrToTime(householdInfo.getCreateTime())));
            householdInfos.add(newhousehold);
        }
        Map<String, List<HouseholdInfo>> HouseholdInfoGroupByTime = householdInfos.stream().collect(Collectors.groupingBy(HouseholdInfo::getCreateTime));
        // 将分组的Map集合分别获取map的Key和value转化为列表
        List<String> Timelist = HouseholdInfoGroupByTime.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getKey).collect((Collectors.toList()));
        List<Integer> Countlist = HouseholdInfoGroupByTime.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().size()).collect((Collectors.toList()));
        householdTimeAndCountDto.setHouseholdCountList(Countlist);
        householdTimeAndCountDto.setTimeList(Timelist);
        return householdTimeAndCountDto;
    }

    /**
     * 获取账单总金额与日期的关系数据
     * @param payOrderInfoList
     * @return
     */
    public AmountAndDateDto getAmountAndDateData(List<PayOrderInfo> payOrderInfoList){
        AmountAndDateDto amountAndDateDto = new AmountAndDateDto();
        List<PayOrderInfo> newpayOrderInfoList =  new ArrayList<>();
        // 处理数据、将原有的数据列表的每个数据的创建时间转化成YYYY-MM-DD格式
        for (PayOrderInfo payOrderInfo : payOrderInfoList) {
            PayOrderInfo payOrderInfoObj = modelMapper.map(payOrderInfo,PayOrderInfo.class);
            payOrderInfoObj.setOrderTime(TurnDateToStringUtil.getTimeTo_Y_M_D(TurnStringToDateUtil.setStrToTime(payOrderInfo.getOrderTime())));
            newpayOrderInfoList.add(payOrderInfoObj);
        }
        Map<String, List<PayOrderInfo>> PayOrderInfoGroupByTime = newpayOrderInfoList.stream().collect(Collectors.groupingBy(PayOrderInfo::getOrderTime));
        // 将分组的Map集合分别获取map的Key和value转化并计算同一天的总额最后合计为列表
        List<String> Timelist = PayOrderInfoGroupByTime.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(Map.Entry::getKey).collect((Collectors.toList()));
        List<BigDecimal> Valuelist = PayOrderInfoGroupByTime.entrySet().stream().sorted(Map.Entry.comparingByKey())
                .map(item->item.getValue().stream().map(PayOrderInfo::getOrderPayAmount).reduce(BigDecimal.ZERO, BigDecimal::add)).collect((Collectors.toList()));
        amountAndDateDto.setAmountList(Valuelist);
        amountAndDateDto.setTimeList(Timelist);
        return amountAndDateDto;
    }
    /**
     * 获取所有收款金额
     * @return
     */
    public BigDecimal getallOrderPaidAmount(){
        BigDecimal allOrderPaidAmount = new BigDecimal("0.00"); // 账单总额度
        List<PayOrderInfo> payOrderInfoList = payOrderInfoMapper.selectList(null);
        for(PayOrderInfo payOrderInfo : payOrderInfoList){
            allOrderPaidAmount = allOrderPaidAmount.add(payOrderInfo.getOrderPayAmount()); // 累加账单额度
        }
        return allOrderPaidAmount;
    }
    /**
     * 获取所有设备供暖量
     * @return
     */
    public BigDecimal getallHeatingCount(){
        BigDecimal allHeatingCount = new BigDecimal("0.00"); // 供暖总量
        List<HeatingInfo> heatingInfoList = heatingInfoMapper.selectList(null);
        for(HeatingInfo heatingInfo : heatingInfoList){
            if (heatingInfo.getHeatEquipmentHeatingCapacity() == null){
                continue;
            }
            allHeatingCount = allHeatingCount.add(heatingInfo.getHeatEquipmentHeatingCapacity()); // 累加注册的设备供热量
        }
        return allHeatingCount;
    }
    /**
     * 获取所有供暖面积
     * @return
     */
    public BigDecimal getallHeatingAreaCount(){
        BigDecimal allHeatingArea = new BigDecimal("0.00"); // 供暖总面积
        List<HeatingInfo> heatingInfoList = heatingInfoMapper.selectList(null);
        for(HeatingInfo heatingInfo : heatingInfoList){
            allHeatingArea = allHeatingArea.add(heatingInfo.getHeatArea()); // 累加注册的设备供热面积
        }
        return allHeatingArea;
    }
}
