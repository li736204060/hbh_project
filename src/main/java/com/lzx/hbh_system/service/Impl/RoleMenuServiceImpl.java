package com.lzx.hbh_system.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.SysRoleMenu;
import com.lzx.hbh_system.mapper.SysRoleMenuMapper;
import com.lzx.hbh_system.service.RoleMenuService;
import com.lzx.hbh_system.util.requestEntity.RequestView;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.stereotype.Service;

@Service
public class RoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements RoleMenuService {

    @Override
    public ResponseView dropRoleMenuByRoleCode(RequestView requestView) {
        return null;
    }

    @Override
    public ResponseView addRoleMenuFilter(ResponseView responseView) {
        return null;
    }
}
