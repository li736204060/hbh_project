package com.lzx.hbh_system.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.SysRolePermission;
import com.lzx.hbh_system.mapper.SysRolePermissionMapper;
import com.lzx.hbh_system.service.RolePermissionService;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements RolePermissionService {

}
