package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysRolePermission;

public interface RolePermissionService  extends IService<SysRolePermission> {
}
