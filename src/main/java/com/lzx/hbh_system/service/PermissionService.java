package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysPermission;
import com.lzx.hbh_system.bo.filter.PermissionFilter;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

import java.util.List;

public interface PermissionService  extends IService<SysPermission> {
    ResponseView getPermissionByUsernameFilter(Userfilter userfilter);
    List<SysPermission> getPermissionByUsername(String Username);
    ResponseView getAllPermissionFilter(PermissionFilter permissionFilter);
}
