package com.lzx.hbh_system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.SysRole;
import com.lzx.hbh_system.bo.filter.RoleFilter;
import com.lzx.hbh_system.bo.filter.UserRoleFilter;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

import java.util.List;

public interface RoleService extends IService<SysRole> {
    public ResponseView getRoleByUsernameFilter(Userfilter userfilter) throws Exception;
    public SysRole getRoleByUsername(String userName);
    public ResponseView getAllRoleFilter(RoleFilter roleFilter);
    public ResponseView addOneRoleFilter(RoleFilter roleFilter);
    public ResponseView modifyOneRoleFilter(RoleFilter roleFilter);
    public ResponseView deleteOneRoleFilter(RoleFilter roleFilter);
    public ResponseView getRoleByMenuCode(RoleFilter roleFilter);
}
