package com.lzx.hbh_system.service.xqzhxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.NoticeInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.NoticeInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface NoticeInfoService extends IService<NoticeInfo> {
    ResponseView queryAllNoticeInfoPageFilter(NoticeInfoFilter filter);
    ResponseView addOneNoticeInfo(NoticeInfoFilter filter);
    ResponseView modifyOneNoticeInfo(NoticeInfoFilter filter);
    ResponseView dropBatchNoticeInfo(NoticeInfoFilter filter);
}
