package com.lzx.hbh_system.service.xqzhxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HeatingInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface HeatingInfoService extends IService<HeatingInfo> {
    ResponseView queryAllHeatingInfoPageFilter(HeatingInfoFilter filter);
    ResponseView addOneHeatingInfo(HeatingInfoFilter filter);
    ResponseView modifyOneHeatingInfo(HeatingInfoFilter filter);
    ResponseView dropBatchHeatingInfo(HeatingInfoFilter filter);
}
