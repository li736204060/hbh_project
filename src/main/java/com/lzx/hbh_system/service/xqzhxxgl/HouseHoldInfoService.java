package com.lzx.hbh_system.service.xqzhxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HouseholdInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface HouseHoldInfoService extends IService<HouseholdInfo> {
    ResponseView queryAllHouseHoldInfoPageFilter(HouseholdInfoFilter filter);
    ResponseView queryAllHouseHoldInfoList();
    ResponseView addOneHouseHoldInfo(HouseholdInfoFilter filter);
    ResponseView modifyOneHouseHoldInfo(HouseholdInfoFilter filter);
    ResponseView dropBatchHouseHoldInfo(HouseholdInfoFilter filter);
}
