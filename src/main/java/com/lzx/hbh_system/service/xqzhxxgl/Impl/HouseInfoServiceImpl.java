package com.lzx.hbh_system.service.xqzhxxgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HouseInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.HeatingInfoDto;
import com.lzx.hbh_system.dto.xqzhxxgl.HouseInfoDto;
import com.lzx.hbh_system.mapper.xqzhxxgl.HouseInfoMapper;
import com.lzx.hbh_system.service.xqzhxxgl.HouseInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class HouseInfoServiceImpl extends ServiceImpl<HouseInfoMapper, HouseInfo> implements HouseInfoService {
    @Autowired
    private HouseInfoMapper houseInfoMapper;

    @Override
    public ResponseView queryAllHouseInfoPageFilter(HouseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<HouseInfoDto> houseInfoDtoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<HouseInfoDto> houseInfoDtoIPage = houseInfoMapper.selectAllHouseInfoPageFilter(houseInfoDtoPage,filter);
        // 处理结果集
        List<HouseInfoDto> houseInfoDtos = houseInfoDtoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(houseInfoDtos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", houseInfoDtos.toArray());
        map.put("currentPage",houseInfoDtoIPage.getCurrent());
        map.put("totalsData",houseInfoDtoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView addOneHouseInfo(HouseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        HouseInfo houseInfo = filter.getHouseInfo();
        String Key = SnowFlakeUtil.getId(); // 生成雪花ID
        houseInfo.setId(Key); // 主键
        houseInfo.setCreateTime(TurnDateToStringUtil.getTime()); // 设置生成时间
        houseInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        houseInfoMapper.insert(houseInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneHouseInfo(HouseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        HouseInfo houseInfo = filter.getHouseInfo();
        houseInfo.setUpdateTime(TurnDateToStringUtil.getTime()); // 设置修改时间
        houseInfoMapper.updateById(houseInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchHouseInfo(HouseInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> houseInfoIdList = filter.getHouseInfoIdList();
        // 逻辑实现
        houseInfoMapper.deleteBatchIds(houseInfoIdList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, HouseInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getHouseInfoIdList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("房屋信息编号不能为空！"));
            return responseView;
        }
        else if (Objects.equals(operaType, "03") || Objects.equals(operaType, "02") ){
            if (filter.getHouseInfo().getHouseArea() == null){
                responseView.setRespOutMsgHeader(RespOutMsgHeader.error("房屋的建筑面积不能为空！"));
                return responseView;
            }
            if (filter.getHouseInfo().getHouseNumber() == null){
                responseView.setRespOutMsgHeader(RespOutMsgHeader.error("房屋的房号不能为空！"));
                return responseView;
            }
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
