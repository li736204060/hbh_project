package com.lzx.hbh_system.service.xqzhxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.BuildingInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.BuildingInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface BuildingInfoService extends IService<BuildingInfo> {
    ResponseView queryAllBuilldingInfoPageFilter(BuildingInfoFilter filter);
    ResponseView queryAllBuilldingInfoByVillageCode(BuildingInfoFilter filter);
    ResponseView addOneBuilldingInfo(BuildingInfoFilter filter);
    ResponseView modifyOneBuildingInfo(BuildingInfoFilter filter);
    ResponseView dropBatchBuildingInfo(BuildingInfoFilter filter);
}
