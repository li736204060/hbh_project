package com.lzx.hbh_system.service.xqzhxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.ReportInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.ReportInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface ReportInfoService extends IService<ReportInfo> {
    ResponseView quertAllReportInfoPageFilter(ReportInfoFilter filter);
    ResponseView addOneReportInfo(ReportInfoFilter filter);
    ResponseView modifyOneReportInfo(ReportInfoFilter filter);
    ResponseView dropBatchReportInfo(ReportInfoFilter filter);
}
