package com.lzx.hbh_system.service.xqzhxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HouseInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface HouseInfoService extends IService<HouseInfo> {
    ResponseView queryAllHouseInfoPageFilter(HouseInfoFilter filter);
    ResponseView addOneHouseInfo(HouseInfoFilter filter);
    ResponseView modifyOneHouseInfo(HouseInfoFilter filter);
    ResponseView dropBatchHouseInfo(HouseInfoFilter filter);
}
