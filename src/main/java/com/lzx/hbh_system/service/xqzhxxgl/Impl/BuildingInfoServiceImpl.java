package com.lzx.hbh_system.service.xqzhxxgl.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.BuildingInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.BuildingInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.BuildingInfoDto;
import com.lzx.hbh_system.mapper.xqzhxxgl.BuildingInfoMapper;
import com.lzx.hbh_system.service.xqzhxxgl.BuildingInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class BuildingInfoServiceImpl extends ServiceImpl<BuildingInfoMapper, BuildingInfo> implements BuildingInfoService {
    @Autowired
    private BuildingInfoMapper buildingInfoMapper;
    @Override
    public ResponseView queryAllBuilldingInfoPageFilter(BuildingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<BuildingInfoDto> buildingInfoDtoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<BuildingInfoDto> buildingInfoDtoIPage = buildingInfoMapper.selectAllBuildingInfoPageFilter(buildingInfoDtoPage,filter);
        // 处理结果集
        List<BuildingInfoDto> buildingInfoDtos = buildingInfoDtoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(buildingInfoDtos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", buildingInfoDtos.toArray());
        map.put("currentPage",buildingInfoDtoIPage.getCurrent());
        map.put("totalsData",buildingInfoDtoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllBuilldingInfoByVillageCode(BuildingInfoFilter filter) {
        // 初始化
        ResponseView responseView = new ResponseView();
        QueryWrapper<BuildingInfo> queryWrapper = new QueryWrapper();
        queryWrapper.eq("village_code",filter.getXqbh());
        List<BuildingInfo> buildingInfos = buildingInfoMapper.selectList(queryWrapper);
        List<Map<String,Object>> mapList = new ArrayList<>();
        buildingInfos.forEach(item ->{
            Map<String,Object> map = new HashMap<>();
            map.put("buildingInfoId",item.getId());
            map.put("buildingInfoNumber",item.getBuildingNumber());
            mapList.add(map);
        });
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(mapList.toArray());
        return responseView;
    }

    @Override
    public ResponseView addOneBuilldingInfo(BuildingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        BuildingInfo buildingInfo = filter.getBuildingInfo();
        String Key = SnowFlakeUtil.getId(); // 生成雪花ID
        buildingInfo.setId(Key); // 主键
        buildingInfo.setCreateTime(TurnDateToStringUtil.getTime());
        buildingInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        buildingInfoMapper.insert(buildingInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneBuildingInfo(BuildingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        BuildingInfo buildingInfo = filter.getBuildingInfo();
        buildingInfo.setUpdateTime(TurnDateToStringUtil.getTime()); // 设置更新时间
        buildingInfoMapper.updateById(buildingInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchBuildingInfo(BuildingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> buildingInfoIdList = filter.getBuildingInfoIdList();
        // 逻辑实现
        buildingInfoMapper.deleteBatchIds(buildingInfoIdList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, BuildingInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getBuildingInfoIdList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("楼号信息编号不能为空！"));
            return responseView;
        }
        else if (Objects.equals(operaType, "03") && filter.getBuildingInfo().getBuildingNumber().length() > 3){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("楼号区间过大，请注意是否西小于等于3字符！"));
            return responseView;
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
