package com.lzx.hbh_system.service.xqzhxxgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.NoticeInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.NoticeInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.NoticeInfoDto;
import com.lzx.hbh_system.mapper.xqzhxxgl.HouseholdInfoMapper;
import com.lzx.hbh_system.mapper.xqzhxxgl.NoticeInfoMapper;
import com.lzx.hbh_system.service.xqzhxxgl.NoticeInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class NoticeInfoServiceImpl extends ServiceImpl<NoticeInfoMapper, NoticeInfo> implements NoticeInfoService {
    @Autowired
    private NoticeInfoMapper noticeInfoMapper;
    @Autowired
    private HouseholdInfoMapper householdInfoMapper;

    @Override
    public ResponseView queryAllNoticeInfoPageFilter(NoticeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<NoticeInfoDto> noticeInfoDtoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<NoticeInfoDto> noticeInfoDtoIPage = noticeInfoMapper.selectAllNoticeInfoPageFilter(noticeInfoDtoPage,filter);
        // 处理结果集
        List<NoticeInfoDto> noticeInfoDtos = noticeInfoDtoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(noticeInfoDtos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", noticeInfoDtos.toArray());
        map.put("currentPage",noticeInfoDtoIPage.getCurrent());
        map.put("totalsData",noticeInfoDtoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView addOneNoticeInfo(NoticeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        NoticeInfo noticeInfo = filter.getNoticeInfo();
        String Key = SnowFlakeUtil.getId(); // 生成雪花ID
        noticeInfo.setId(Key); // 主键
        noticeInfo.setNoticeStatus("0"); // 默认通知新增为未读状态
        noticeInfo.setNoticeTime(TurnDateToStringUtil.getTime()); // 设置通知时间
        // 查询住户是否有效 若无效则通知失败
        HouseholdInfo householdInfo = householdInfoMapper.selectById(noticeInfo.getNoticeHouseholdCode());
        if (Objects.equals(householdInfo.getValiFlag(), "0")){
            // 住户无效状态
            // 设置通知失败状态
            noticeInfo.setNoticeStatus("2");
            // 设置失败原因
            noticeInfo.setNoticeFailResion("无效住户信息");
        }
        noticeInfo.setCreateTime(TurnDateToStringUtil.getTime()); // 设置生成时间
        noticeInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        noticeInfoMapper.insert(noticeInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneNoticeInfo(NoticeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        NoticeInfo noticeInfo = filter.getNoticeInfo();
        if (!Objects.equals(noticeInfo.getNoticeStatus(), "2")){
            // 只要不是失败状态的情况下
            // 清空失败原因
            noticeInfo.setNoticeFailResion("暂无失败原因");
        }
        noticeInfo.setUpdateTime(TurnDateToStringUtil.getTime()); // 设置修改时间
        noticeInfoMapper.updateById(noticeInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchNoticeInfo(NoticeInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> noticeInfoIdList = filter.getNoticeInfoIdList();
        // 逻辑实现
        noticeInfoMapper.deleteBatchIds(noticeInfoIdList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, NoticeInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getNoticeInfoIdList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("通知信息编号不能为空！"));
            return responseView;
        }
        else if (Objects.equals(operaType, "03") || Objects.equals(operaType, "02") ){
            if (filter.getNoticeInfo().getNoticeHouseholdCode() == null){
                responseView.setRespOutMsgHeader(RespOutMsgHeader.error("通知的住户编号不能为空！"));
                return responseView;
            }
            if (filter.getNoticeInfo().getNoticeContent() == null){
                responseView.setRespOutMsgHeader(RespOutMsgHeader.error("通知的住户内容不能为空！"));
                return responseView;
            }
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
