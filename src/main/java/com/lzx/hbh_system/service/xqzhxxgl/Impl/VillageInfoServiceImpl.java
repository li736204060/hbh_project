package com.lzx.hbh_system.service.xqzhxxgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.VillageInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.VillageInfo;
import com.lzx.hbh_system.mapper.xqzhxxgl.VillageInfoMapper;
import com.lzx.hbh_system.service.xqzhxxgl.VillageInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class VillageInfoServiceImpl extends ServiceImpl<VillageInfoMapper, VillageInfo> implements VillageInfoService {
    @Autowired
    private VillageInfoMapper villageInfoMapper;
    @Override
    public ResponseView queryAllVillageInfoPageFilter(VillageInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<VillageInfo> villageInfoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<VillageInfo> villageInfoIPage = villageInfoMapper.selectAllVillageInfoPageFilter(villageInfoPage,filter);
        // 处理结果集
        List<VillageInfo> villageInfos = villageInfoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(villageInfos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", villageInfos.toArray());
        map.put("currentPage",villageInfoIPage.getCurrent());
        map.put("totalsData",villageInfoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView queryAllVillageInfoList() {
        // 初始化
        ResponseView responseView = new ResponseView();
        List<VillageInfo> villageInfos = villageInfoMapper.selectList(null);
        List<Map<String,Object>> mapList = new ArrayList<>();
        villageInfos.forEach(item ->{
            Map<String,Object> map = new HashMap<>();
            map.put("villageId",item.getId());
            map.put("villageName",item.getVillageName());
            mapList.add(map);
        });
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("查询成功"));
        responseView.setMain(mapList.toArray());
        return responseView;
    }

    @Override
    public ResponseView addOneVillageInfo(VillageInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        VillageInfo villageInfo = filter.getVillageInfo();
        String villageInfoKey = SnowFlakeUtil.getId(); // 生成雪花ID
        villageInfo.setId(villageInfoKey); // 主键
        villageInfo.setCreateTime(TurnDateToStringUtil.getTime());
        villageInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        villageInfoMapper.insert(villageInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneVillageInfo(VillageInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        VillageInfo villageInfo = filter.getVillageInfo();
        villageInfo.setUpdateTime(TurnDateToStringUtil.getTime()); // 设置更新时间
        villageInfoMapper.updateById(villageInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchVillageInfo(VillageInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> villageInfoIdList = filter.getVillageInfoIdList();
        // 逻辑实现
        villageInfoMapper.deleteBatchIds(villageInfoIdList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, VillageInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getVillageInfoIdList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("小区信息编号不能为空！"));
            return responseView;
        }
//        else if (Objects.equals(operaType, "00") && filter.g() == null ){
//            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("小区信息编号不能为空！"));
//            return responseView;
//        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
