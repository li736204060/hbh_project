package com.lzx.hbh_system.service.xqzhxxgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.NoticeInfoFilter;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.ReportInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.NoticeInfo;
import com.lzx.hbh_system.bo.xqzhxxgl.ReportInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.NoticeInfoDto;
import com.lzx.hbh_system.dto.xqzhxxgl.ReportInfoDto;
import com.lzx.hbh_system.mapper.xqzhxxgl.ReportInfoMapper;
import com.lzx.hbh_system.service.xqzhxxgl.ReportInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class ReportInfoServiceImpl extends ServiceImpl<ReportInfoMapper, ReportInfo> implements ReportInfoService {
    @Autowired
    private ReportInfoMapper reportInfoMapper;

    @Override
    public ResponseView quertAllReportInfoPageFilter(ReportInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<ReportInfoDto> reportInfoDtoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<ReportInfoDto> reportInfoDtoIPage = reportInfoMapper.selectAllReportInfoPageFilter(reportInfoDtoPage,filter);
        // 处理结果集
        List<ReportInfoDto> reportInfoDtos = reportInfoDtoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(reportInfoDtos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", reportInfoDtos.toArray());
        map.put("currentPage",reportInfoDtoIPage.getCurrent());
        map.put("totalsData",reportInfoDtoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView addOneReportInfo(ReportInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        ReportInfo reportInfo = filter.getReportInfo();
        String Key = SnowFlakeUtil.getId(); // 生成雪花ID
        reportInfo.setId(Key); // 主键
        reportInfo.setCreateTime(TurnDateToStringUtil.getTime()); // 设置生成时间
        reportInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        reportInfoMapper.insert(reportInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneReportInfo(ReportInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        ReportInfo reportInfo = filter.getReportInfo();
        reportInfo.setUpdateTime(TurnDateToStringUtil.getTime()); // 设置修改时间
        reportInfoMapper.updateById(reportInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchReportInfo(ReportInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> reportInfoIdList = filter.getReportInfoIdList();
        // 逻辑实现
        reportInfoMapper.deleteBatchIds(reportInfoIdList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, ReportInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getReportInfoIdList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("供热报告信息编号不能为空！"));
            return responseView;
        }
        else if (Objects.equals(operaType, "03") || Objects.equals(operaType, "02") ){
            if (filter.getReportInfo().getHouseholdCode() == null){
                responseView.setRespOutMsgHeader(RespOutMsgHeader.error("供热报告信息住户编号不能为空！"));
                return responseView;
            }
            if (filter.getReportInfo().getReportResion() == null){
                responseView.setRespOutMsgHeader(RespOutMsgHeader.error("供热报告信息的原因不能为空！"));
                return responseView;
            }
            if (filter.getReportInfo().getReportType() == null){
                responseView.setRespOutMsgHeader(RespOutMsgHeader.error("供热报告信息的类型不能为空！"));
                return responseView;
            }
        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
