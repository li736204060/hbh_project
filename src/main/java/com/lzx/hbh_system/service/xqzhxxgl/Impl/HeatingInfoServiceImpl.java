package com.lzx.hbh_system.service.xqzhxxgl.Impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HeatingInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.HeatingInfoDto;
import com.lzx.hbh_system.mapper.xqzhxxgl.HeatingInfoMapper;
import com.lzx.hbh_system.service.xqzhxxgl.HeatingInfoService;
import com.lzx.hbh_system.util.SnowFlakeUtil;
import com.lzx.hbh_system.util.TurnDateToStringUtil;
import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@Slf4j
public class HeatingInfoServiceImpl extends ServiceImpl<HeatingInfoMapper, HeatingInfo> implements HeatingInfoService {
    @Autowired
    private HeatingInfoMapper heatingInfoMapper;

    @Override
    public ResponseView queryAllHeatingInfoPageFilter(HeatingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("00",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        // 初始化分页对象
        Page<HeatingInfoDto> heatingInfoDtoPage = new Page<>(filter.getPageIndex(),filter.getPageSize());
        // 查询
        IPage<HeatingInfoDto> heatingInfoDtoIPage = heatingInfoMapper.selectAllHeatingInfoPageFilter(heatingInfoDtoPage,filter);
        // 处理结果集
        List<HeatingInfoDto> heatingInfoDtos = heatingInfoDtoIPage.getRecords();
        // 返回响应消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success(heatingInfoDtos.size(),"查询成功！"));
        // 设置返回结果
        Map<String,Object> map = new HashMap<>();
        map.put("dataArray", heatingInfoDtos.toArray());
        map.put("currentPage",heatingInfoDtoIPage.getCurrent());
        map.put("totalsData",heatingInfoDtoIPage.getTotal());
        responseView.setMain(map);
        return responseView;
    }

    @Override
    public ResponseView addOneHeatingInfo(HeatingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("03",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        HeatingInfo heatingInfo = filter.getHeatingInfo();
        String Key = SnowFlakeUtil.getId(); // 生成雪花ID
        heatingInfo.setId(Key); // 主键
        heatingInfo.setHeatTemperature(new BigDecimal("39")); // 设置默认供热温度
        heatingInfo.setValiFlag("1"); // 有效标识
        // 保存数据库
        heatingInfoMapper.insert(heatingInfo);
        // 响应结果
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("添加成功！"));
        return responseView;
    }

    @Override
    public ResponseView modifyOneHeatingInfo(HeatingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("02",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        // 逻辑实现
        HeatingInfo heatingInfo = filter.getHeatingInfo();
        // 查询原状态信息
        HeatingInfo OriginheatingInfo = heatingInfoMapper.selectById(heatingInfo.getId());
        if(heatingInfo.getHeatValveFlag() != null){
            // 仅当供热信息的阀门状态不为空的情况下才进行
            if (!Objects.equals(heatingInfo.getHeatValveFlag(), OriginheatingInfo.getHeatValveFlag())){
                heatingInfo.setHeatValveFlagChangeTime(TurnDateToStringUtil.getTime()); // 阀门变更时间
            }
            if (heatingInfo.getHeatValveFlag().equals("1") || heatingInfo.getHeatValveFlag().equals("2")){
                // 模拟硬件获取温度 这里使用随机生成温度数据进行显示
                // 整数随机生成 30 - 60 的数字作为温度
                int min = 30;
                int max = 60;
                int tempRandom = (int) (min+Math.random()*(max-min+1));
                // 小数位 范围  1 — 9
                int minpoint = 1;
                int maxpoint = 9;
                int tempRandomPoint = (int) (minpoint+Math.random()*(maxpoint-minpoint+1));
                String resultTemp = tempRandom +"."+ tempRandomPoint;
                heatingInfo.setHeatTemperature(new BigDecimal(resultTemp));
            }else{
                heatingInfo.setHeatTemperature(new BigDecimal("0.00"));
            }
        }

        heatingInfoMapper.updateById(heatingInfo);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("修改成功！"));
        return responseView;
    }

    @Override
    public ResponseView dropBatchHeatingInfo(HeatingInfoFilter filter) {
        // 初始化返回视图
        // 校验参数
        ResponseView responseView = checkParams("01",filter);
        if(responseView.getRespOutMsgHeader().getRespCode().equals(500)){
            return responseView;
        }
        List<String> heatingInfoIdList = filter.getHeatingInfoIdList();
        // 逻辑实现
        heatingInfoMapper.deleteBatchIds(heatingInfoIdList);
        // 返回消息
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success("删除成功！"));
        return responseView;
    }
    /**
     * 参数校验函数
     *  01 删除
     *  02 修改
     *  03 新增
     *  00 查询
     * @param filter
     * @return
     */
    public ResponseView checkParams(String operaType, HeatingInfoFilter filter){
        // 初始化返回视图
        ResponseView responseView = new ResponseView();
        if (Objects.equals(operaType, "01") && filter.getHeatingInfoIdList().size() == 0 ){
            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("供热设备信息编号不能为空！"));
            return responseView;
        }
//        else if (Objects.equals(operaType, "02") && filter.g== null ){
//            responseView.setRespOutMsgHeader(RespOutMsgHeader.error("小区信息编号不能为空！"));
//            return responseView;
//        }
        responseView.setRespOutMsgHeader(RespOutMsgHeader.success());
        return responseView;
    }
}
