package com.lzx.hbh_system.service.xqzhxxgl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.VillageInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.VillageInfo;
import com.lzx.hbh_system.util.responseEntity.ResponseView;

public interface VillageInfoService extends IService<VillageInfo> {
    ResponseView queryAllVillageInfoPageFilter(VillageInfoFilter filter);
    ResponseView queryAllVillageInfoList();
    ResponseView addOneVillageInfo(VillageInfoFilter filter);
    ResponseView modifyOneVillageInfo(VillageInfoFilter filter);
    ResponseView dropBatchVillageInfo(VillageInfoFilter filter);
}
