package com.lzx.hbh_system.dto;

import lombok.Data;

import java.util.List;

@Data
public class DictsDtoStronger {
    private String value;
    private String code;
    private List<DictsDtoStronger> children;
}
