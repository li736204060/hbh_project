package com.lzx.hbh_system.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色用户数量统计Dto
 */
@Data
public class RoleUserCountDto implements Serializable {
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 对应用户注册数量
     */
    private Integer roleUserCount;
}
