package com.lzx.hbh_system.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 个人用户统计表 - 数据统计 出参DTO
 * 统一Echarts的series数据展示字段
 */
@Data
public class UserStatsDto implements Serializable {
    /**
     * 展示的数据名称
      */
    private String name;
    /**
     * 展示的数据 - y
     */
    private List<Integer> data;
    /**
     * 对应的数据采集时间 - x
     */
    private List<String> time;
    /**
     * 数据展示类型（line - 折线图 bar - 饼状图 。。）
     */
    private String type;
}
