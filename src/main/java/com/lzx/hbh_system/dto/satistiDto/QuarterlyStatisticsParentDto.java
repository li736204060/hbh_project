package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 季度-费用-分布图数据集
 */
@Data
public class QuarterlyStatisticsParentDto implements Serializable {
    private QuarterlyStatisticsDto springQuarterlyStatistics; // 春季度数据集合
    private QuarterlyStatisticsDto summerQuarterlyStatistics; // 夏季度数据集合
    private QuarterlyStatisticsDto autumnQuarterlyStatistics; // 秋季度数据集合
    private QuarterlyStatisticsDto winterQuarterlyStatistics; // 冬季度数据集合
}
