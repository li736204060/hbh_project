package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class DashBoardTopDto implements Serializable {
    private Integer allHouseholdCount; // 总户数
    private BigDecimal allHeatingCount; // 供暖量
    private BigDecimal allHeatingAreaCount; // 供暖面积
    private BigDecimal allOrderPaidAmount; // 供暖总收费额度
}
