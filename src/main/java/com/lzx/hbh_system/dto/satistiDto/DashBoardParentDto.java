package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DashBoardParentDto implements Serializable {
    private DashBoardTopDto dashBoardTopDto; // 首页头部统计数据实体
    private HouseholdTimeAndCountDto householdTimeAndCountDto; // 关于住户数量与日期的关系数据
    private VillageAndHeatingCapacityDto villageAndHeatingCapacityDto; // 关于小区与小区总供暖的关系数据
    private VillageAndHeatingAreaDto villageAndHeatingAreaDto; // 关于小区与小区总供暖面积的关系数据
    private AmountAndDateDto amountAndDateDto; // 关于账单总额与日期的关系数据
    private QuarterlyStatisticsParentDto quarterlyStatisticsParentDto; // 各季度 各费用项目 额度统计分布数据
    private ExpanseStatisicsParentDto expanseStatisicsParentDto; // 各费用项目总累计营收额度数据
    private MonthOrderInfoStatisicsDto monthOrderInfoStatisicsDto; // 年度各月份统计营收额度数据
}
