package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class AmountAndDateDto implements Serializable {
    private List<String> timeList;
    private List<BigDecimal> amountList;
}
