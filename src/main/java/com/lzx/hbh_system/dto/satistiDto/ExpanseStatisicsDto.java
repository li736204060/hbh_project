package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 费用项目饼状图数据实体 - 出参
 */
@Data
public class ExpanseStatisicsDto implements Serializable {
    private String name;
    private BigDecimal value;
}
