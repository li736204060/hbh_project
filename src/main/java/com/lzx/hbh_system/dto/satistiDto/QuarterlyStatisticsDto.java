package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 各季度 各费用项目 额度统计分布数据
 */
@Data
public class QuarterlyStatisticsDto implements Serializable {
    private String name;
    private List<BigDecimal> value;
}
