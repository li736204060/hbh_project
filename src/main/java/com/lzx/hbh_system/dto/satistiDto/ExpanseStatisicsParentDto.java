package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ExpanseStatisicsParentDto implements Serializable {
    private List<ExpanseStatisicsDto> expanseStatisicsDtoList;
}
