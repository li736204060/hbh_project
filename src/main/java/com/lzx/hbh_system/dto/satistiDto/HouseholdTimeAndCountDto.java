package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * echarts 数据 出参类
 * 住户注册数量与时间的关系
 */
@Data
public class HouseholdTimeAndCountDto implements Serializable {
    private List<String> timeList; // 注册时间
    private List<Integer> householdCountList; // 住户数量
}
