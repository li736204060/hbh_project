package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class VillageAndHeatingAreaDto implements Serializable {
    private List<String> VillageNameList;
    private List<BigDecimal> villageHeatingAreaList;

}
