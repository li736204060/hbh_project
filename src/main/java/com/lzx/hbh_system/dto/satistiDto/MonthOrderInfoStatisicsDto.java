package com.lzx.hbh_system.dto.satistiDto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 当前年度的月份营收统计额度条形图 - 出参
 */
@Data
public class MonthOrderInfoStatisicsDto implements Serializable {
    private List<String> name;
    private List<BigDecimal> value;
}
