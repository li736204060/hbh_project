package com.lzx.hbh_system.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户实体bo映射类
 * */
@Data
public class UserDto implements Serializable {
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户编号
     */
    private String userCode;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 用户状态标识
     * 1 正常
     * 0 禁用-冻结
     * 01 欠费（账单未结）
     * 11 审核中
     * 00 注销-不再使用
     */
    private String status;
    /**
     * 最近登陆时间 YYYY-MM-DD HH:MM:SS
     */
    private String logintime;
    /**
     * 用户登陆时，（首次）设置redis存储登陆初始信息，设置对应登陆状态、登陆时间、登陆用户名、salt值、IP地址等，
     * eq：设置redis有效期为，1、3、7天（初始登陆有效期--换算小时--）。
     * 请求访问时在（或者拿到secret值-未知）有效期内使用原token与redis中的salt值匹配成功后，【1】刷新token重新发放给前端，重置数据库salt值，
     *--------------------------------
     * 【1】刷新token实现，用户token正常token有效期为5分钟，每次请求访问时【0】，先判断redis中该用户信息是否有效期内（或者是否登陆状态？），
     * 【0】问题：如果我登陆后不访问，那么token将不会进行刷新，则5分钟后token失效，token失效后携带失效token访问报token失效异常，
     * 问题处理：此时redis还为登陆状态（如果存在），抛出异常时，设置redis的当前用户信息的登陆状态为离线，并设置离线有效期【4】
     *
         * （1）若存在（初始登陆有效期）且为登陆状态，从redis中拿到信息生成secret与原token的secret匹配，若成功则说明同一用户，刷新token，重新获取token，
         * 设置新的salt值给redis，和发放回前端，前端保存token信息（暂不需要同步至数据库，用户离线前还可能修改密码的操作也涉及到salt的变化，还可以再次刷新token）
         * （2）若为存在（初始登陆有效期）且离线状态【2】，这时候前端已经退出登陆了-（可能正在登陆页面执行登陆），原token前端已经删除了，不能刷新token，因此直接再次重新执行初始登陆，并设置对应状态。。。
         * （3）若直接不存在【3】，直接进行登陆并存放salt值等部分用户信息到redis中存放并设置初始登陆有效期。
         *  （以此循环）
         * 使得用户已登录信息在设定redis有效时间内不失效
     *--------------------------------
     * eq：用户退出时【4】，设置redis登陆用户信息，登陆状态设置为离线，（此时redis中登陆用户信息依然有效）系统获取redis当前用户key，getandset命令，
     * 重新设置key有效期：（离线有效期--换算成小时--） = （初始登陆有效期--换算成小时--)*20 % ，
     * 【2】若离线【有效期内】再次登陆时，则重新设置为初始登陆有效期（1、3、7天--换算小时--），并设置对应登陆状态、登陆时间。
     *
     * eq：若该用户离线超过redis中的离线有效期时，系统同步最后用户redis存放信息，同步至数据库中，以备下次登陆使用。
     * 【3】若离线【有效期外】再次登陆时，登陆时，相当于redis中没有该信息，因此按照首次登陆情况，查询数据库信息，登陆成功后同步登陆状态到redis中（上次离线前redis同步的信息或首次登陆信息）。
     * （以此循环）
     * 问题记录：时间：2022-1-26 17：23
     * 问题：token刷新后如何向前端回传新的token，不影响原有的controller服务方法，实现无感更新token？？？---待解决
     * 暂时想到方案：使用类似Websokect协议 后端主动向前端发送数据，前端接收到后将数据处理。。。 --实现，但不稳定容易丢失数据
     *
     * 登陆状态：在线，离线
     */
    private Boolean islogin;
    /**
     * 登陆信息存放有效期 - 小时
     */
    private Integer lifetime;
    /**
     * 最近登陆IP地址：
     */
    private String ipAddress;
    /**
     * 用户角色名称
     * @return
     */
    private List<String> roleName;
    @Override
    public String toString(){
        String dlzt = null;
        String yhzt = null;
        if (this.islogin != null && this.islogin.equals(true)){
            dlzt = "登陆";
        }else{
            dlzt = "离线";
        }
        if(this.status != null && this.status.equals("1")){//‘1’ 正常 ‘0’ 冻结 ‘01’ 用户欠费-status设为 0  ‘00’ 用户注销-vali设为 0 ‘11’ 审核中
            yhzt = "正常";
        }else if(this.status != null && this.status.equals("0")){
            yhzt = "禁用";
        }else if(this.status != null && this.status.equals("01")){
            yhzt = "用户欠费";
        }else if(this.status != null && this.status.equals("00")){
            yhzt = "用户注销";
        }else{
            yhzt = "审核中";
        }
        return "当前登陆用户信息：[用户名："+this.userName+",用户编号："+this.userCode+",用户角色："+this.roleName+",登陆状态："+dlzt+",最近登陆时间："+this.logintime+",用户状态："+yhzt+",状态有效期："+this.lifetime+"小时,登陆IP："+this.ipAddress+"]";
    }
}
