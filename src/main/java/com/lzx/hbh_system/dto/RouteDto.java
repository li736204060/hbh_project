package com.lzx.hbh_system.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RouteDto implements Serializable {
    private String title; // 显示标题
    private List<RouteDto> children; // 携带的次级子节点列表
    private String path; // 访问地址
    private String url; // 路由地址
    private String icon; // 菜单图标名称
    private String hidden; // 是否隐藏
    private String menukey; // 编号
    private String mainkey; // 父级编号
}
