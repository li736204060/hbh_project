package com.lzx.hbh_system.dto;

import com.lzx.hbh_system.bo.SysUser;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户查询 - 出参实体
 */
@Data
public class UserQueryDto extends SysUser implements Serializable {
    private String roleName; // 用户关联的角色名称  暂时支持单一角色
    private String userInfoName; // 用户关联用户详情 用户昵称
}
