package com.lzx.hbh_system.dto.xqzhxxgl;

import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class HeatingInfoDto extends HeatingInfo implements Serializable {
    private String householdName;
    private String cotyCode;
}
