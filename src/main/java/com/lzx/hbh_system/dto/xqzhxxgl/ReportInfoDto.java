package com.lzx.hbh_system.dto.xqzhxxgl;

import com.lzx.hbh_system.bo.xqzhxxgl.ReportInfo;
import lombok.Data;

import java.io.Serializable;

@Data
public class ReportInfoDto extends ReportInfo implements Serializable {
    private String householdName; // 当前住户名称
    private String householdPreviousStatus; // 当前住户往期缴费状态
    private String householdPayStatus; // 当前住户支付状态
    private String buildingNumber; // 当前住户的楼号
    private String houseNumber; // 当前住户的房号
    private String villageName; // 当前住户的小区名称
    private String cotyCode; // 当前住户的隶属权限编号
}
