package com.lzx.hbh_system.dto.xqzhxxgl;

import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class HouseholdInfoDto extends HouseholdInfo implements Serializable {
    private String cotyCode;
    private String villageName;
    private String buildingNumber;
    private String heatArea;
    private String houseArea;
    private String houseNumber;
}
