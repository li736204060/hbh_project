package com.lzx.hbh_system.dto.xqzhxxgl;

import com.lzx.hbh_system.bo.xqzhxxgl.BuildingInfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class BuildingInfoDto extends BuildingInfo implements Serializable {
    private String villageName; // 小区名称
    private String cotyCode; // 隶属管辖区域
}
