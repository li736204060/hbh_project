package com.lzx.hbh_system.dto.xqzhxxgl;

import com.lzx.hbh_system.bo.xqzhxxgl.HouseInfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class HouseInfoDto extends HouseInfo implements Serializable {
    private String buildingNumber; // 当前房屋的楼号
    private String villageName; // 当前房屋的小区名称
    private String cotyCode; // 当前房屋的隶属权限编号
    private String householdName; // 住户名称
    private String heatArea; // 住户房屋可供热面积
}
