package com.lzx.hbh_system.dto;

import com.lzx.hbh_system.bo.user.UseractivityInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 用户留言板 出参DTO
 */
@Data
public class UseractivityInfoDto extends UseractivityInfo implements Serializable {
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 上传的图片地址列表
     */
    private List<String> pictureUrlList;
    /**
     * 来自省会名称
     */
    private String regionName;
    /**
     * 用户拥有角色 ... 若角色拥有多个角色则需要修改成获取角色列表 否则会报错
     */
    private String roleName;
}
