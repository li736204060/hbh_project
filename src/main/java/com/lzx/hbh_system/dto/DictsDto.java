package com.lzx.hbh_system.dto;

import com.lzx.hbh_system.bo.dict.Dicts;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 字典信息 - 出参
 */
@Data
public class DictsDto extends Dicts implements Serializable {
    List<Dicts> children; // 子节点
}
