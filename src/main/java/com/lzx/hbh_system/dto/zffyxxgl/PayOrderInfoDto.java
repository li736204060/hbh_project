package com.lzx.hbh_system.dto.zffyxxgl;

import com.lzx.hbh_system.bo.zffyxxgl.PayOrderInfo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PayOrderInfoDto extends PayOrderInfo implements Serializable {
    private String hasAllReconciliationFlag; // 是否全对账 （ 1 已对账 2 未对账（或者存在未对账项目））
    private String householdName; // 住户名称
    private String householdType; // 住户类型
    private String householdStatus; // 住户状态
    private String previousStatus; // 住户往期缴费主管状态
    private String cotyCode; // 区县编号
    private String buildingNumber; // 楼号
    private String houseNumber; // 房号
    private String villageName; // 小区名称
    private BigDecimal heatArea; // 供热面积
}
