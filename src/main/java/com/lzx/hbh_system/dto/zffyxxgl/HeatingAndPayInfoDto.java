package com.lzx.hbh_system.dto.zffyxxgl;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 用户采暖费收费日报 - 出参实体类
 */
@Data
public class HeatingAndPayInfoDto implements Serializable {
    private String villageName; // 小区名称
    private Integer allHouseholdCount; // 总户数
    private BigDecimal allHouseholdHeatingAreaCount; // 总缴费面积
    private BigDecimal allHouseholdPaidAmountCount; // 总缴费金额
    private Integer todayPaidHouseHoldCount; // 今日缴费户数
    private BigDecimal todayPaidHeatingAreaCount; // 今日缴费供热面积
    private BigDecimal todayPaidAmountCount; // 今日缴费金额
    private Integer cumulativePaidHouseholdCount; // 累计缴费户数
    private BigDecimal cumulativePaidHeatingAreaCount; // 累计缴费面积
}
