package com.lzx.hbh_system.dto.zffyxxgl;

import com.lzx.hbh_system.bo.zffyxxgl.PayInfo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PayInfoDto extends PayInfo implements Serializable {
    // 用于费用项目支付信息展示
    private String expenseName; // 费用项目名称
    private String expenseType; // 费用项目收费按类型
    private String expenseModle; // 费用项目收费模式
    private String expenseLevel; // 优先级
    private String amountPrice; // 单价
    private BigDecimal heatArea; // 住户房屋可供热面积
    // 用于主页住户的基本信息展示
    private String householdName; // 住户名称
    private String householdType; // 住户类型
    private String idCard; // 住户身份证号码
    private String contactPhone; // 住户联系电话
    private String householdStatus; // 住户状态
    private String householdPayStatus; // 住户支付状态 -- 需修改  不应该是住户的属性
    private String payType; // 支付类型 -- 需修改  不应该是住户的属性
    private String previousStatus; // 往年支付状态
    private String buildingNumber; // 住户所在的楼号号码
    private String houseNumber; // 住户所在的房号号码
    private String cotyCode; // 住户所在管理区县区域编号
    // 支付信息详情
    private String reconciliationFlag; // 是否对账标识
}
