package com.lzx.hbh_system.dto.yytygbmgl;

import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentInfo;
import lombok.Data;

import java.io.Serializable;

/**
 * 部门信息 - 出参
 */
@Data
public class DepartmentInfoDto extends DepartmentInfo implements Serializable {
    /**
     * 部门类型名称
     */
    private String depTypeName;
    /**
     * 部门类型备注
     */
    private String depTypeMemo; // memo
}
