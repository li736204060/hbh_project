package com.lzx.hbh_system.dto.yytygbmgl;

import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 员工信息 dto - 出参
 */
@Data
public class EmployeeInfoDto extends EmployeeInfo implements Serializable {
    private Integer employWorkTimes; // 员工工作天数
    // 员工类型
    private String employTypeName; // 员工类型名称
    // 出勤
    private String attendRecodeTime; // 员工出勤签到时间
    private String attendFlag; // 是否出勤 （1 - 是 2 - 否）
    private String attendBeginTime; // 出勤开始时间
    private String attendEndTime; // 出勤结束时间
    private String attendMemo; // 出勤备注
    // 工资
    private BigDecimal basicSalary; // 基础工资
    private BigDecimal allInsurance; // 综合保险
    private BigDecimal awardSalary; // 奖金
    private BigDecimal finedSalary; // 罚金
    private BigDecimal finalSalary; // 实际获得工资
    // 请假
    private String vacateFlag; // 是否请假 （判断是否存在关联编号）
    private String vacateType; // 请假类型 （1 - 其他特殊情况请假 2 - 病假 3 - 事假 4 - 丧假 5 - 产假 6 - 探亲假）
    private String vacateBeginTime; // 请假开始时间
    private String vacateEndTime; // 请假结束时间
    private String vacateResion; // 请假缘由
    private String isApproveFlag; // 是否批准
    private String approverName; // 批准人
    private String approveTime; // 批准时间
    // 部门信息
    private String depName; // 部门名称
    // 用户
    private String userName; // 用户名
}
