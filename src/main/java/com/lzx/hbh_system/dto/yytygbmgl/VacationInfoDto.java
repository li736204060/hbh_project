package com.lzx.hbh_system.dto.yytygbmgl;

import com.lzx.hbh_system.bo.yytygbmgl.qjgl.VacationInfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class VacationInfoDto extends VacationInfo implements Serializable {
    private String depName; // 部门名称
    private String employTypeName; // 员工类型名称
    private String employAge; // 员工年龄
    private String gender; // 性别
    private String userName;
}
