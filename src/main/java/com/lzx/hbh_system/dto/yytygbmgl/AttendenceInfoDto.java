package com.lzx.hbh_system.dto.yytygbmgl;

import com.lzx.hbh_system.bo.yytygbmgl.cqgl.AttendanceInfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class AttendenceInfoDto extends AttendanceInfo implements Serializable {
    private String userName; // 用户昵称
}
