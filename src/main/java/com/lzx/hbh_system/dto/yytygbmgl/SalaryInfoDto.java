package com.lzx.hbh_system.dto.yytygbmgl;

import com.lzx.hbh_system.bo.yytygbmgl.gzgl.SalaryInfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class SalaryInfoDto extends SalaryInfo implements Serializable {
    private String userName; // 用户昵称
    private String depName; // 部门名称
    private String employTypeName; // 员工职称/类型
    private String employAge; // 员工年龄
    private String gender; // 员工性别
}
