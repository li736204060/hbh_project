package com.lzx.hbh_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.SysRole;
import com.lzx.hbh_system.bo.filter.Userfilter;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
    List<SysRole> selectRole_byusername(Userfilter userfilter);
    SysRole searchByUsername(@Param("userName") String Username);
    SysRole searchByRoleName(@Param("roleName") String rolename);
    List<SysRole> searchByMeanCode(@Param("MenuCode") String menuCode);
}