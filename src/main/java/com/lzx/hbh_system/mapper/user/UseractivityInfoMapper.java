package com.lzx.hbh_system.mapper.user;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.UseracivityInfoFilter;
import com.lzx.hbh_system.bo.user.UseractivityInfo;
import com.lzx.hbh_system.dto.UseractivityInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface UseractivityInfoMapper extends BaseMapper<UseractivityInfo> {
//    List<UseractivityInfo> selectAllByFilter(UseracivityInfoFilter filter);
   // 自定义SQL+自带Page插件分页
   IPage<UseractivityInfoDto> selectAllByFilter(Page<UseractivityInfoDto> page,@Param("filter") UseracivityInfoFilter filter);
}