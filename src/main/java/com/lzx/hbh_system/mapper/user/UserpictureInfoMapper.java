package com.lzx.hbh_system.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.user.UserpictureInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface UserpictureInfoMapper extends BaseMapper<UserpictureInfo> {
}