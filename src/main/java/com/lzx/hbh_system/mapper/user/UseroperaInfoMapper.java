package com.lzx.hbh_system.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.user.UseroperaInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UseroperaInfoMapper extends BaseMapper<UseroperaInfo> {
    List<UseroperaInfo> querySomeByFilter(@Param("userCode") String userCode);
}
