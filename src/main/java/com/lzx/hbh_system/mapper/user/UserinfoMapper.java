package com.lzx.hbh_system.mapper.user;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.user.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserinfoMapper extends BaseMapper<Userinfo> {
    Userinfo queryOneUserinfoFilter(@Param("userCode") String userCode);
}