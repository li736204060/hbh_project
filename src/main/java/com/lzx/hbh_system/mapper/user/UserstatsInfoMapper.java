package com.lzx.hbh_system.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.user.UserStatsInfo;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserstatsInfoMapper extends BaseMapper<UserStatsInfo> {
}
