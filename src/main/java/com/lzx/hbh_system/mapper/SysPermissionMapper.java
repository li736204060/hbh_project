package com.lzx.hbh_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.SysPermission;
import com.lzx.hbh_system.bo.filter.Userfilter;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysPermissionMapper extends BaseMapper<SysPermission> {
    List<SysPermission> selectPermission_byusername(Userfilter userfilter);
    List<SysPermission> searchAllByUsername(@Param("userName") String username);
}