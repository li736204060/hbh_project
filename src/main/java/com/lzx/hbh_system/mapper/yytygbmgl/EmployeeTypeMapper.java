package com.lzx.hbh_system.mapper.yytygbmgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeTypeFilter;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface EmployeeTypeMapper extends BaseMapper<EmployeeType> {
    IPage<EmployeeType> selectAllEmpTypePageFilter(Page<EmployeeType> page, @Param("filter") EmployeeTypeFilter filter);
}