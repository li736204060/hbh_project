package com.lzx.hbh_system.mapper.yytygbmgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.yytygbmgl.AttendenceInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.cqgl.AttendanceInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AttendanceInfoMapper extends BaseMapper<AttendanceInfo> {
    IPage<AttendanceInfo> selectAllAttendanceInfoPageFilter(Page<AttendanceInfo> page, @Param("filter") AttendenceInfoFilter filter);
}