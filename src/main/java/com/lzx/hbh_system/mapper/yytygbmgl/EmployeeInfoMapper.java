package com.lzx.hbh_system.mapper.yytygbmgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.yytygbmgl.EmployeeInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import com.lzx.hbh_system.dto.yytygbmgl.EmployeeInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmployeeInfoMapper extends BaseMapper<EmployeeInfo> {
    IPage<EmployeeInfo> selectAllEmpInfoPageFilter(Page<EmployeeInfo> page, @Param("filter") EmployeeInfoFilter filter);
    List<EmployeeInfoDto> selectAllEmpSimpleList();
}