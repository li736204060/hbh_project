package com.lzx.hbh_system.mapper.yytygbmgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.yytygbmgl.VacationInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.qjgl.VacationInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface VacationInfoMapper extends BaseMapper<VacationInfo> {
    IPage<VacationInfo> selectAllVacationInfoPageFilter(Page<VacationInfo> page, @Param("filter") VacationInfoFilter filter);

}