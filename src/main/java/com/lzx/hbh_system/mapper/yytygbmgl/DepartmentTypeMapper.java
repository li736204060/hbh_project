package com.lzx.hbh_system.mapper.yytygbmgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentTypeFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface DepartmentTypeMapper extends BaseMapper<DepartmentType> {
    IPage<DepartmentType>  selectAllDepTypePageFilter(Page<DepartmentType> page, @Param("filter") DepartmentTypeFilter filter);
}