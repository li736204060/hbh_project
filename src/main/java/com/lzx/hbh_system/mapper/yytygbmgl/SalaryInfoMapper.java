package com.lzx.hbh_system.mapper.yytygbmgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.yytygbmgl.SalaryInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.gzgl.SalaryInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SalaryInfoMapper extends BaseMapper<SalaryInfo> {
    IPage<SalaryInfo> selectAllSalaryInfoPageFilter(Page<SalaryInfo> page, @Param("filter") SalaryInfoFilter filter);

}