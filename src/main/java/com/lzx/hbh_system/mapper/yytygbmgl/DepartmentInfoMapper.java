package com.lzx.hbh_system.mapper.yytygbmgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.yytygbmgl.DepartmentInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentInfo;
import com.lzx.hbh_system.dto.yytygbmgl.DepartmentInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface DepartmentInfoMapper extends BaseMapper<DepartmentInfo> {
    IPage<DepartmentInfoDto> selectAllDepartmentInfoPageFilter(Page<DepartmentInfoDto> page, @Param("filter") DepartmentInfoFilter filter);
}