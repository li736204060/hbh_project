package com.lzx.hbh_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.SysUserRole;
import com.lzx.hbh_system.dto.RoleUserCountDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    List<RoleUserCountDto> queryALLUserforCount();
}