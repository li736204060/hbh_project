package com.lzx.hbh_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.dict.Dicts;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DictsMapper extends BaseMapper<Dicts> {
}