package com.lzx.hbh_system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.SysMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {
    public List<SysMenu>  queryMenuByRoleCode(String roleCode);
}