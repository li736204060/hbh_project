package com.lzx.hbh_system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.SysUser;
import com.lzx.hbh_system.bo.filter.Userfilter;
import com.lzx.hbh_system.bo.filter.yytygbmgl.SalaryInfoFilter;
import com.lzx.hbh_system.bo.yytygbmgl.gzgl.SalaryInfo;
import com.lzx.hbh_system.dto.UserQueryDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    SysUser selectOneBymodel(SysUser sysUser);
    SysUser searchByUserName(String userName);
    SysUser searchByUserNameAndUserPwd(@Param("userName") String username,@Param("userPwd") String pwd);

    IPage<UserQueryDto> selectAllUserInfoPage(Page<UserQueryDto> page, @Param("filter") Userfilter filter);
}