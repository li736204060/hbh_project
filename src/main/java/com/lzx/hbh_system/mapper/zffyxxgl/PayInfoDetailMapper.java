package com.lzx.hbh_system.mapper.zffyxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.zffyxxgl.PayInfoDetail;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PayInfoDetailMapper extends BaseMapper<PayInfoDetail> {

}