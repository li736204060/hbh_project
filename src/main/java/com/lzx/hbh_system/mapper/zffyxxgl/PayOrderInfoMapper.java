package com.lzx.hbh_system.mapper.zffyxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.zffyxxgl.PayOrderInfoFilter;
import com.lzx.hbh_system.bo.zffyxxgl.PayOrderInfo;
import com.lzx.hbh_system.dto.zffyxxgl.PayOrderInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PayOrderInfoMapper extends BaseMapper<PayOrderInfo> {
    IPage<PayOrderInfoDto> selectAllPayOrderInfoPageFilter(Page<PayOrderInfoDto> page, @Param("filter") PayOrderInfoFilter filter);
}