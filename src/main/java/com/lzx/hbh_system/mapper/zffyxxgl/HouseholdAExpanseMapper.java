package com.lzx.hbh_system.mapper.zffyxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.zffyxxgl.HouseholdAExpanse;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HouseholdAExpanseMapper extends BaseMapper<HouseholdAExpanse> {

}