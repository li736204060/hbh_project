package com.lzx.hbh_system.mapper.zffyxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.zffyxxgl.ExpanseInfoFilter;
import com.lzx.hbh_system.bo.zffyxxgl.ExpanseInfo;
import com.lzx.hbh_system.dto.zffyxxgl.PayInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ExpanseInfoMapper extends BaseMapper<ExpanseInfo> {
    IPage<ExpanseInfo> selectAllExpanseInfoPageFilter(Page<ExpanseInfo> page, @Param("filter") ExpanseInfoFilter filter);
    List<PayInfoDto> selectAllExpanseInfoByHouseholdCode(ExpanseInfoFilter filter);
}