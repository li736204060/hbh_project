package com.lzx.hbh_system.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.dict.DictsProvince;
import com.lzx.hbh_system.bo.user.Userinfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DictsProvinceMapper extends BaseMapper<Userinfo> {
    List<DictsProvince> queryAll();
}