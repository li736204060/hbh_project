package com.lzx.hbh_system.mapper.xqzhxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HouseholdInfoFilter;
import com.lzx.hbh_system.bo.filter.zffyxxgl.ExpanseInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import com.lzx.hbh_system.bo.zffyxxgl.ExpanseInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.HouseholdInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface HouseholdInfoMapper extends BaseMapper<HouseholdInfo> {
    IPage<HouseholdInfoDto> selectAllHouseholdInfoPageFilter(Page<HouseholdInfoDto> page, @Param("filter") HouseholdInfoFilter filter);
}