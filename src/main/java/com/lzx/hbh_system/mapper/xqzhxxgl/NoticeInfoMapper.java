package com.lzx.hbh_system.mapper.xqzhxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.NoticeInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.NoticeInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.NoticeInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface NoticeInfoMapper extends BaseMapper<NoticeInfo> {
    IPage<NoticeInfoDto> selectAllNoticeInfoPageFilter(Page<NoticeInfoDto> page, @Param("filter") NoticeInfoFilter filter);

}