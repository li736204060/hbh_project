package com.lzx.hbh_system.mapper.xqzhxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.BuildingInfoFilter;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.HeatingInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.BuildingInfoDto;
import com.lzx.hbh_system.dto.xqzhxxgl.HeatingInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface HeatingInfoMapper extends BaseMapper<HeatingInfo> {
    IPage<HeatingInfoDto> selectAllHeatingInfoPageFilter(Page<HeatingInfoDto> page, @Param("filter") HeatingInfoFilter filter);

}