package com.lzx.hbh_system.mapper.xqzhxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.BuildingInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.BuildingInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.BuildingInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BuildingInfoMapper extends BaseMapper<BuildingInfo> {
    IPage<BuildingInfoDto> selectAllBuildingInfoPageFilter(Page<BuildingInfoDto> page, @Param("filter") BuildingInfoFilter filter);

}