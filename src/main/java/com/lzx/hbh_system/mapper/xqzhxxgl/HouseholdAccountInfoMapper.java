package com.lzx.hbh_system.mapper.xqzhxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdAccountInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HouseholdAccountInfoMapper extends BaseMapper<HouseholdAccountInfo> {
}