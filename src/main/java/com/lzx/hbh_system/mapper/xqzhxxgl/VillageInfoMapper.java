package com.lzx.hbh_system.mapper.xqzhxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.VillageInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.VillageInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface VillageInfoMapper extends BaseMapper<VillageInfo> {
    IPage<VillageInfo> selectAllVillageInfoPageFilter(Page<VillageInfo> page, @Param("filter") VillageInfoFilter filter);

}