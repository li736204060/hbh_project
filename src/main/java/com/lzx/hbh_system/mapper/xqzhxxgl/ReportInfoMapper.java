package com.lzx.hbh_system.mapper.xqzhxxgl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.NoticeInfoFilter;
import com.lzx.hbh_system.bo.filter.xqzhxxgl.ReportInfoFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.ReportInfo;
import com.lzx.hbh_system.dto.xqzhxxgl.NoticeInfoDto;
import com.lzx.hbh_system.dto.xqzhxxgl.ReportInfoDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface ReportInfoMapper extends BaseMapper<ReportInfo> {
    IPage<ReportInfoDto> selectAllReportInfoPageFilter(Page<ReportInfoDto> page, @Param("filter") ReportInfoFilter filter);

}