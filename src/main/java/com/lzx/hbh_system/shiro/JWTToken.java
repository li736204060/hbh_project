package com.lzx.hbh_system.shiro;

import com.lzx.hbh_system.util.JWTUtil;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * 生成属于JWT的token放弃shiro自带的UsernamePasswordToken
 */
public class JWTToken implements AuthenticationToken {
    private String token;

    public JWTToken(String token) {
        this.token = token;
    }

    /**
     *
     * @return
     */
    @Override
    public Object getPrincipal() {
        return token;
    }

    /**
     *
     * @return
     */
    @Override
    public Object getCredentials() {
        return token;
    }
}
