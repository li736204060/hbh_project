package com.lzx.hbh_system.shiro;

import com.alibaba.druid.util.StringUtils;
import com.lzx.hbh_system.bo.SysUser;
import com.lzx.hbh_system.service.UserService;
import com.lzx.hbh_system.util.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
@Slf4j
public class JWTCredentialsMatcher implements CredentialsMatcher {
    /**
     * 使用JWTutil比对token 是否正确
     * true 登陆成功
     * false 登陆失败
     * @param authenticationToken
     * @param authenticationInfo
     * @return
     */
    @Autowired
    private UserService userService;
    @Override
    public boolean doCredentialsMatch(AuthenticationToken authenticationToken, AuthenticationInfo authenticationInfo) {
        Object tokenCredentials = authenticationToken.getCredentials();
        Object accountCredentials = authenticationInfo.getCredentials();
        if (tokenCredentials.equals(accountCredentials)) {
            return true;
        }
        log.error("tokenCredentials.equals(accountCredentials)--->false，请求token与数据库用户生成token不匹配，数据库用户信息与输入信息不匹配！");
        return false;
    }
}
