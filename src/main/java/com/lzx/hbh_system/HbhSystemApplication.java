package com.lzx.hbh_system;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
//@MapperScan("com.lzx.hbh_system.mapper")//扫描mapper接口注解为bean，让其可以自动注入使用---这里无效，暂时在各个mapper类上加@Mapper
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class HbhSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(HbhSystemApplication.class, args);
    }


}
