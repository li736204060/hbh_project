package com.lzx.hbh_system.error;

import com.lzx.hbh_system.util.responseEntity.RespOutMsgHeader;
import com.lzx.hbh_system.util.responseEntity.ResponseView;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用于处理Shiro的异常，并响应JSON数据返回给前端用于页面跳转
 */
@RestController
public class JwtFilterErrorExecptionController {
    ResponseView responseView = new ResponseView();
    @RequestMapping("/error/{code}/{msg}")
    public ResponseView handleExeption(@PathVariable("code") int code,@PathVariable("msg") String msg){
        responseView.setRespOutMsgHeader(RespOutMsgHeader.error(false, msg,code));
        return responseView;
    }
}
