package com.lzx.hbh_system.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 权限表 - 存放所有权限信息
 */
@Data
@TableName(value = "sys_permission")
public class SysPermission implements Serializable {
    /**
     * 权限编码-主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String permissionCode;
    /**
     * 数据唯一标识 雪花id
     */
    private String id;
    /**
     * 权限名称
     */
    private String permissionName;
    /**
     * 权限描述-用于UI显示
     */
    private String permissionDescription;
    /**
     *
     * 权限类被-（系统、模块、菜单、操作）
     */
    private String permissionType;
    /**
     *
     * 站点标识-（前台、后台或者服务）
     */
    private String sideType;
    /**
     *
     * 权限父ID-用于构建树
     *
     */
    private String pid;
    /**
     * 角色编号-用于角色更新方便数据维护
     */
    private String roleCode;
    /**
     * 权限地址
     */
    private String url;
    /**
     * 有效标识
     * 1 有效
     * 0 无效
     */
    private String validFlag;

}