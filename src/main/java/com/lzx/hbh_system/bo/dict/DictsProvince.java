package com.lzx.hbh_system.bo.dict;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName(value = "dicts_province")
public class DictsProvince implements Serializable {
    /**
     * 省会编号
     */
    @TableId(type = IdType.AUTO)//增长
    private String provinceId;
    /**
     * 省会名称
     */
    private String provinceName;
}
