package com.lzx.hbh_system.bo.dict;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 公用字典表
 */
@Data
@TableName(value = "dicts")
public class Dicts implements Serializable {
    /**
     * 字典编号
     */
    @TableId(type = IdType.AUTO)//增长
    private Integer id;

    private String dictType; // 字典类型

    private String parentCode; // 父字典编号

    private String code; // 字典编号

    private String value; // 字典内容

    private String level; // 排序序号

    private static final long serialVersionUID = 1L;
}