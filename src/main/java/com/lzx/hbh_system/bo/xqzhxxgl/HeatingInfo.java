package com.lzx.hbh_system.bo.xqzhxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
@Data
@TableName("heating_info")
public class HeatingInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String householdCode;

    private String heatEquipmentId;

    private String heatEquipmentSettingFlag;

    private String heatValveFlag;

    private String heatValveFlagChangeTime;

    private String heatEquipmentStatus;

    private Date heatEquipmentStatusChangeTime;

    private BigDecimal heatEquipmentHeatingCapacity;

    private BigDecimal heatTemperature;

    private BigDecimal heatArea;

    private String heatingBeginDate;

    private String valiFlag;

    private static final long serialVersionUID = 1L;

}