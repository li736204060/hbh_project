package com.lzx.hbh_system.bo.xqzhxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("notice_info")
public class NoticeInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String noticeHouseholdCode;

    private String noticeContent;

    private String noticeTime;

    private Integer noticeEffectiveDays;

    private String noticeStatus;

    private String noticeFailResion;

    private String createTime;

    private String updateTime;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}