package com.lzx.hbh_system.bo.xqzhxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName(value = "village_info")
public class VillageInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String villageName;

    private String cotyCode;

    private String createTime;

    private String updateTime;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}