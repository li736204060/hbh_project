package com.lzx.hbh_system.bo.xqzhxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName(value = "household_info")
public class HouseholdInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String householdName;

    private String householdType;

    private String unitNumber;

    private String householdAccountCode;

    private String noticeCode;

    private String houseCode;

    private String buildingCode;

    private String heatingCode;

    private String villageCode;

    private String idCard;

    private String contactPhone;

    private String householdStatus;

    private String payStatus;

    private String payType;

    private String previousStatus;

    private String createTime;

    private String updateTime;

    private String memo;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}