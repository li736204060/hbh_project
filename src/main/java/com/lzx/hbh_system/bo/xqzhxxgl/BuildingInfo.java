package com.lzx.hbh_system.bo.xqzhxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName(value = "building_info")
public class BuildingInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String buildingNumber;

    private String villageCode;

    private String createTime;

    private String updateTime;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}