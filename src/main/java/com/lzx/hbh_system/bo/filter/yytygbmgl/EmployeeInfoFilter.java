package com.lzx.hbh_system.bo.filter.yytygbmgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 员工信息 - 入参
 */
@Data
public class EmployeeInfoFilter extends BaseFilter implements Serializable {
    private String ygxx; // 员工信息 （memo、userName、age、employDepartureResion）
    private String yglxbh; // 员工类型编号
    private String qjlxbh; // 请假类型编号（1 - 其他特殊情况请假 2 - 病假 3 - 事假 4 - 丧假 5 - 产假 6 - 探亲假）
    private String bmxxbh; // 部门信息编号
    private String gzMin; // 工资最小值
    private String gzMax; // 工资最大值
    private String qjsjksBegin; // 请假开始时间的开端
    private String qjsjksEnd; // 请假开始时间的末尾
    private String qjsjjsBegin; // 请假结束时间的开端
    private String qjsjjsEnd; // 请假结束时间的末尾
    private String sfcq; // 是否出勤 1 是 0 否
    private String sfqj; // 是否请假 1 是 0 否
    private Integer gzsj; // （工作时长 = 离职时间 - 入职时间）查询-通过MySql自带函数计算
    private String ygxb; // 员工性别 ( 1 - 男 0 - 女)
    private String rzsjBegin; // 入职开始时间-范围-开端
    private String rzsjEnd; // 入职开始时间-范围-结尾
    private String lzsjBegin; // 离职时间-范围-开端
    private String lzsjEnd; // 离职时间-范围-开端
    private String ygztbh; // 员工状态编号 员工状态（00 - 在职、11 - 离职 、 01 - 请假 ）
    private String yggzlxbh; // 员工工作类型编号 工作形式（00 - 现场办公 、01 - 远程非本市办公、02 - 远程本市办公）
    private String yggzztbh; // 员工工作状态编号 当前工作状态（00 - 现场技术维护作业、01 - 办公室服务作业、02 - 上门维护作业、03 - 上门宣传指导作业、 04 - 作业完成，待作业、11 特殊情况，无作业）
    private String sfzxbs; // 是否员工注销标识 valiFlag;
    private EmployeeInfo employeeInfo;
    private List<String> empCodeList;
}
