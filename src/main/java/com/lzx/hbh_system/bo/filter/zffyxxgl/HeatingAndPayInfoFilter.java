package com.lzx.hbh_system.bo.filter.zffyxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import lombok.Data;

import java.io.Serializable;

@Data
public class HeatingAndPayInfoFilter extends BaseFilter implements Serializable {
    private String villageName; // 查询的小区名称
}
