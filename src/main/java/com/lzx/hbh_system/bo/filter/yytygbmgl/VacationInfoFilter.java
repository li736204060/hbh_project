package com.lzx.hbh_system.bo.filter.yytygbmgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.yytygbmgl.cqgl.AttendanceInfo;
import com.lzx.hbh_system.bo.yytygbmgl.qjgl.VacationInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 请假信息 - 入参
 */
@Data
public class VacationInfoFilter extends BaseFilter implements Serializable {
    /**
     * 请假信息
     */
    private String qjxx;
    /**
     * 请假开始时间 - 范围 - 开始
     */
    private String qjkssjBegin;
    /**
     * 请假开始时间 - 范围 - 结束
     */
    private String qjkssjEnd;
    /**
     * 请假结束时间 - 范围 - 开始
     */
    private String qjjssjBegin;
    /**
     * 请假结束时间 - 范围 - 结束
     */
    private String qjjssjEnd;
    /**
     * 批准时间 - 范围 - 开始
     */
    private String pzsjBegin;
    /**
     * 批准时间 - 范围 - 结束
     */
    private String pzsjEnd;
    /**
     * 员工类型编号
     */
    private String yglxbh;
    /**
     * 部门编号
     */
    private String bmbh;
    /**
     * 请假类型编号
     */
    private String qjlxbh;
    /**
     * 是否批准标识
     */
    private String sfpzbs;
    /**
     * 请假信息对象
     */
    private VacationInfo vacationInfo;
    /**
     * 请假信息Code列表
     */
    private List<String> vacationInfoCodeList;
    /**
     * 请假编号
     */
    private String qjbh;
}
