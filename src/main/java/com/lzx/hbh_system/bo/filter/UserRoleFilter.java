package com.lzx.hbh_system.bo.filter;

import lombok.Data;

import java.io.Serializable;
@Data
public class UserRoleFilter extends BaseFilter implements Serializable {
    /**
     * 用户角色编号 主键
     */
    private String userToRoleCode;
    /**
     * 角色编号
     */
    private String roleCode;
    /**
     * 用户编号
     */
    private String userCode;
    /**
     * 用户名称 - 前端传入当前登陆的用户名
     */
    private String userName;
    /**
     * 角色名称 - 前端传入当前登陆的用户的角色名称
     */
    private String roleName;
    /**
     * 是否注册时
     */
    private boolean isResisting;
}
