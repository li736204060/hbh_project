package com.lzx.hbh_system.bo.filter.zffyxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.zffyxxgl.PayInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PayInfoFilter extends BaseFilter implements Serializable {
    private String zfxx; // 支付信息（费用项目名称/费用项目单价等）
    private String zhxx; // 住户信息（住户名称/身份证号码/住户名称/备注等）
    private String fyxmbh; // 查询的费用项目编号
    private Integer yearBeginNumber; // 年度起
    private Integer yearEndNumber; // 年度止
    private String zfzt; // 支付状态编号
    private String zfsjBegin; // 支付开始
    private String zfsjEnd; // 支付时间至
    private String zhbh; // 住户编号
    private List<String> expanseIdList; // 用于支付信息的添加
    private List<String> PayInfoIdList;
    private PayInfo payInfo; // 用于修改单个
    private List<PayInfo> payInfos; // 用于修改多个
    private String sfdz; // 是否对账标识
}
