package com.lzx.hbh_system.bo.filter.yytygbmgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 部门类型 - 入参
 */
@Data
public class DepartmentTypeFilter extends BaseFilter implements Serializable {
    /**
     * 部门类型信息
     */
    private String bmlxxx;
    /**
     * 部门类型对象
     */
    private DepartmentType departmentType;
    /**
     * 部门编号列表
     */
    private List<String> depTypeList;
}
