package com.lzx.hbh_system.bo.filter.xqzhxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseholdInfo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class HouseholdInfoFilter extends BaseFilter implements Serializable {
    private String zhxx; // 住户信息 (楼号等)
    private String lhbh; // 楼号编号
    private String jfzt; // 缴费状态
    private String zhzt; // 住户状态
    private String zhlx; // 住户类型
    private String jffs; // 缴费方式
    private String wqjfzt; // 往期缴费状态
    private String sfyxbs; // 是否有效标识
    private HouseholdInfo householdInfo;
    private List<String> householdInfoIdList;
    // 用于增加住户房屋
    private String houseNumber; // 房号
    private BigDecimal houseArea; // 建筑面积
    // 用于增加供热信息
    private BigDecimal heatingArea; // 供热面积
    // 用于关联并创建住户费用项目信息
    private List<String> expanseIdList;
}
