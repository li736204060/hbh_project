package com.lzx.hbh_system.bo.filter.xqzhxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.BuildingInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 楼号信息 - 入参
 */
@Data
public class BuildingInfoFilter extends BaseFilter implements Serializable {
    private String lhxx;
    private String sfyxbs;
    private String qxbh;
    private List<String> BuildingInfoIdList;
    private BuildingInfo buildingInfo;
    private String xqbh;
}
