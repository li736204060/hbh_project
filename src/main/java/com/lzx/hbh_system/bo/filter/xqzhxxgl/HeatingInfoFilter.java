package com.lzx.hbh_system.bo.filter.xqzhxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HeatingInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class HeatingInfoFilter extends BaseFilter implements Serializable {
    private String grxx; // 供热信息（供热面积、住户名称）
    private String sbbh; // 供热设备编号ID
    private String sbazzt; // 供热设备安装状态
    private String fmzt; // 供热设备阀门状态
    private String fmgbsj; // 供热设备阀门改变时间
    private String grzlzt; // 供热设备质量状态
    private String grsbazsj; // 供热设备安装时间
    private String grsbgrl; // 供热量
    private List<String> heatingInfoIdList;
    private HeatingInfo heatingInfo;
}
