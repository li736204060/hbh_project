package com.lzx.hbh_system.bo.filter.yytygbmgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.yytygbmgl.cqgl.AttendanceInfo;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 出勤信息 - 入参
 */
@Data
public class AttendenceInfoFilter extends BaseFilter implements Serializable {
    /**
     * 出勤信息
     */
    private String cqxx;
    /**
     * 签到时间-范围-开始
     */
    private String qdsjks;
    /**
     * 签到时间-范围-结束
     */
    private String qdsjjs;
    /**
     * 出勤时间-范围-开始
     */
    private String cqsjks;
    /**
     * 出勤时间-范围-结束
     */
    private String cqsjjs;
    /**
     * 出勤信息对象
     */
    private AttendanceInfo attendanceInfo;
    /**
     * 出勤信息Code列表
     */
    private List<String> AttendanceCodeList;
    /**
     * 出勤编号
     */
    private String cqbh;
}
