package com.lzx.hbh_system.bo.filter;

import lombok.Data;

import java.io.Serializable;
@Data
public class UserstatsInfoFilter implements Serializable {
    /**
     * 统计数据获取-类型
     * 01 - 获取 （当天在线总时长 - 时间 ） 关系
     * ... 其他功能 即将上线
     */
    private String statisSearchType;
    /**
     * 需要查询统计的-用户编号
     */
    private String userCode;
}
