package com.lzx.hbh_system.bo.filter.yytygbmgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.yytygbmgl.gzgl.SalaryInfo;
import com.lzx.hbh_system.bo.yytygbmgl.qjgl.VacationInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 工资信息 - 入参
 */
@Data
public class SalaryInfoFilter extends BaseFilter implements Serializable {
    /**
     * 工资信息
     */
    private String gzxx;
    /**
     * 部门编号
     */
    private String bmbh;
    /**
     * 员工类型编号
     */
    private String yglxbh;
    /**
     * 工资信息对象
     */
    private SalaryInfo salaryInfo;
    /**
     * 工资信息Code列表
     */
    private List<String> salaryInfoCodeList;
}
