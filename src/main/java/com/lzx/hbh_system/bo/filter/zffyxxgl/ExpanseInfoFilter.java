package com.lzx.hbh_system.bo.filter.zffyxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.zffyxxgl.ExpanseInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ExpanseInfoFilter extends BaseFilter implements Serializable {
    private String fyxx; // 费用项目-信息
    private String sflx; // 费用项目-收费按类型
    private String sfms; // 费用项目-收费模式
    private String sfyxbs;
    private List<String> expanseInfoIdList;
    private ExpanseInfo expanseInfo;
    private String householdCode; // 住户编号
}
