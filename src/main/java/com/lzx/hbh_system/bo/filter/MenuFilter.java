package com.lzx.hbh_system.bo.filter;

import com.lzx.hbh_system.bo.SysMenu;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.List;

@Data
@Slf4j
public class MenuFilter extends BaseFilter implements Serializable {
    /**
     * 角色编号
     */
    private String roleCode;
    /**
     * 菜单编号
     */
    private String MenuCode;
    /**
     * 菜单名称
     */
    private String MenuName;
    /**
     * 菜单编号列表
     */
    private List<String> MenuCodeList;
    /**
     * 菜单对象
     */
    private SysMenu menuinfo;
}
