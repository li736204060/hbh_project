package com.lzx.hbh_system.bo.filter;

import lombok.Data;

import java.io.Serializable;
@Data
public class UseroperaInfoFilter extends BaseFilter implements Serializable {
    /**
     * 用户编号
     */
    private String userCode;
}
