package com.lzx.hbh_system.bo.filter.zffyxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.zffyxxgl.PayOrderInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class PayOrderInfoFilter extends BaseFilter implements Serializable {
    private String zdxx; // 账单信息
    private String zhxx; // 住户信息
    private String zhlx; // 住户类型
    private String wqjfzt; // 住户往期缴费状态
    private String zhbh; // 住户编号
    private String lhbh; // 查询的楼号编号
    private String jffs; // 缴费方式
    private String sfzf; // 是否作废账单
    private PayOrderInfo payOrderInfo;
    private List<PayOrderInfo> payOrderInfos; // 修改是否作废
}
