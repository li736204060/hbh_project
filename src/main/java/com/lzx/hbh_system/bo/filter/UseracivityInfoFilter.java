package com.lzx.hbh_system.bo.filter;

import com.lzx.hbh_system.bo.user.UseractivityInfo;
import lombok.Data;

import java.io.Serializable;

@Data
public class UseracivityInfoFilter extends BaseFilter implements Serializable {
    //查询部分
    /**
     * 用户名称
     * 用户编号
     * 留言板内容
     */
    private String queryMsg;
    // 删-增-改-部分
    /**
     * 用户留言板对象
     */
    private UseractivityInfo useractivityInfo;
    /**
     * 用户操作类型 判断
     * "like" 喜欢
     * "dislike" 不喜欢
     * "top" 置顶
     * "untop" 不置顶
     */
    private String handleType;
    /**
     * 留言板编号
     */
    private String useractivityInfoCode;
}
