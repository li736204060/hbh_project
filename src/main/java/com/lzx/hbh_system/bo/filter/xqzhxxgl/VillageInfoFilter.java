package com.lzx.hbh_system.bo.filter.xqzhxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.VillageInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 小区信息 - 入参
 */
@Data
public class VillageInfoFilter extends BaseFilter implements Serializable {
    private String xqxx;
    private String qxbh;
    private String sfyxbs;
    private List<String> villageInfoIdList;
    private VillageInfo villageInfo;
}
