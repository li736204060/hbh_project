package com.lzx.hbh_system.bo.filter.xqzhxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.ReportInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ReportInfoFilter extends BaseFilter implements Serializable {
    private String bgxx; // 报告信息（住户名称、原因、备注）
    private String bglx; // 报告类型
    private String yhjfzt; // 当前住户缴费状态
    private String wqjfzt; // 当前住户往期缴费状态
    private String xqbh; // 查询住户小区编号
    private List<String> reportInfoIdList;
    private ReportInfo reportInfo;
}
