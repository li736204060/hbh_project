package com.lzx.hbh_system.bo.filter;

import com.lzx.hbh_system.bo.SysMenu;
import com.lzx.hbh_system.bo.SysRole;
import com.lzx.hbh_system.dto.RouteDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
//添加与实体相同字段的属性时，请注意是否拷贝后置null的情况
public class RoleFilter extends BaseFilter implements Serializable {
    /**
     * 是否开启分页，开启后需要传入pageindex、和pagesize
     */
    private boolean isPage;
    /**
     * 角色名称
     */
    private String roleName;
    /**
     * 角色描述-用途等 例如：“用户创建”
     */
    private String roleDescription;
    /**
     * 父节点（上一级角色编号）例如 教师 - 某某教师
     */
    private String parentCode;
    /**
     * 有效标识
     * 1 有效
     * 0 无效
     */
    private String validFlag;
    /**
     * 批量的角色主键列表
     */
    private List<SysRole> roleList;
    /**
     * 路由菜单列表
     */
    private List<RouteDto> routes;
    /**
     * 角色对象
     */
    private SysRole role;
    /**
     *
     * 菜单编号
     */
    private String menuCode;
}
