package com.lzx.hbh_system.bo.filter.xqzhxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.HouseInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class HouseInfoFilter extends BaseFilter implements Serializable {
    private String fwxx; // 房屋信息
    private String lhxx; // 楼号信息
    private String xqbh; // 小区编号
    private String jzmj; // 建筑面积
    private String grmj; // 供热面积
    private List<String> houseInfoIdList;
    private HouseInfo houseInfo;
}
