package com.lzx.hbh_system.bo.filter;

import com.lzx.hbh_system.bo.SysMenu;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RoleAndMenuFilter extends BaseFilter implements Serializable {
    /**
     * 角色编号
     */
    private String roleCode;
    /**
     * 菜单列表
     */
    private List<SysMenu> menuList;
}
