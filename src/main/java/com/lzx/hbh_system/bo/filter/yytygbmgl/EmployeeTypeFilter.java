package com.lzx.hbh_system.bo.filter.yytygbmgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentType;
import com.lzx.hbh_system.bo.yytygbmgl.yggl.EmployeeType;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 员工类型 - 入参
 */
@Data
public class EmployeeTypeFilter extends BaseFilter implements Serializable {
    /**
     * 员工类型信息
     */
    private String yglxxx;
    /**
     * 员工类型对象
     */
    private EmployeeType employeeType;
    /**
     * 员工编号列表
     */
    private List<String> empTypeList;
}
