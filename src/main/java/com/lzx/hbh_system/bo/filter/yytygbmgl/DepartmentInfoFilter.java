package com.lzx.hbh_system.bo.filter.yytygbmgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.yytygbmgl.bmgl.DepartmentInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 部门信息查询 - 入参
 */
@Data
public class DepartmentInfoFilter extends BaseFilter implements Serializable {
    /**
     * 部门信息
     */
    private String bmxx;
    /**
     * 部门类型
     */
    private String bmlx;
    /**
     * 部门编号列表
     */
    private List<String> bmbhList;
    /**
     * 部门对象(编辑/新增)
     */
    private DepartmentInfo departmentInfo;
}
