package com.lzx.hbh_system.bo.filter;


import lombok.Builder;
import lombok.Data;

import java.io.Serializable;


public class SortFilter implements Serializable {
    private  static final String DEFALT_ORDER_FAIED = "desc";
    /**
     * 排序字段
     */
    private String sortField;
    /**
     * 排序方式-默认升序
     */
    private String sortOrder;

    public SortFilter(String sortField) {
        this.sortField = sortField;
        this.sortOrder = DEFALT_ORDER_FAIED;
    }

    public SortFilter(String sortField, String sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    public SortFilter() {
    }
}
