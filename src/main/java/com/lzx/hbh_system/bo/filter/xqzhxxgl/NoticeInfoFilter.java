package com.lzx.hbh_system.bo.filter.xqzhxxgl;

import com.lzx.hbh_system.bo.filter.BaseFilter;
import com.lzx.hbh_system.bo.xqzhxxgl.NoticeInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class NoticeInfoFilter extends BaseFilter implements Serializable {
    private String tzxx;
    private String xqbh;
    private String tzzt; // 查询的通知状态
    private String tzsjks; // 查询的通知时间范围开始
    private String tzsjjs; // 查询的通知时间范围结束
    private List<String> noticeInfoIdList;
    private NoticeInfo noticeInfo;
}
