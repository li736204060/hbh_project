package com.lzx.hbh_system.bo.filter;

import lombok.Data;

import java.io.Serializable;
@Data
public class PermissionFilter extends BaseFilter implements Serializable {
    /**
     * 是否开启分页
     */
    private boolean isPage;
}
