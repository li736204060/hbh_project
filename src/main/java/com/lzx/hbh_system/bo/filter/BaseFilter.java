package com.lzx.hbh_system.bo.filter;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * 基本入参
 * 可选分页大小 pageSize 、 pageIndex 属性进行传参
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseFilter extends SortFilter implements Serializable {
    /**
     * 当前页
     */
    private Integer pageIndex;
    /**
     * 当前页条数
     */
    private Integer pageSize;

}
