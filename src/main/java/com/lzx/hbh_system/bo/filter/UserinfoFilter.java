package com.lzx.hbh_system.bo.filter;

import com.lzx.hbh_system.bo.user.Userinfo;
import lombok.Data;

import java.io.Serializable;
@Data
public class UserinfoFilter extends BaseFilter implements Serializable {
    /**
     * 用户编号
     */
    private String userCode;
    /**
     * 用户信息对象
     */
    private Userinfo userinfo;
}
