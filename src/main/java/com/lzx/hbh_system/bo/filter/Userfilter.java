package com.lzx.hbh_system.bo.filter;

import com.lzx.hbh_system.bo.SysUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Userfilter extends BaseFilter implements Serializable {
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户密码
     */
    private String userPwd;
    /**
     * token
     */
    private String token;
    /**
     * 用户状态保存时间 - 用户在该使用期间不会掉线
     * eq：若用户不进行请求访问超过5分钟时，状态自动设置为离线。
     * 1 天 -->24 小时
     * 3 天 -->24*3 小时
     * 7 天 -->24*7 小时
     */
    private Integer stateTimeout;
    /**
     * 用户原密码-改密使用
     */
    private String originPassword;

    // 以下是用于查询
    private String yhxx; //查询账号相关信息
    private String userRoleName; // 查询用户角色名称
    private List<String> userCodeList; // 逻辑删除
    private SysUser sysUser;
}
