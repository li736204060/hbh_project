package com.lzx.hbh_system.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色-权限 关联表
 */
@Data
@TableName("sys_role_permission")
public class SysRolePermission implements Serializable {
    /**
     * 角色权限编号 -主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String rolePermissionCode;
    /**
     * 数据唯一标识 雪花id
     */
    private String id;
    /**
     * 角色编号
     */
    private String roleCode;
    /**
     * 权限编号
     */
    private String permissionCode;



}