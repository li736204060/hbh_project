package com.lzx.hbh_system.bo.zffyxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("pay_info_detail")
public class PayInfoDetail implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String payInfoCode;

    private String reconciliationFlag;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}