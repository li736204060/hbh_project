package com.lzx.hbh_system.bo.zffyxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@TableName("expanse_info")
public class ExpanseInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String expenseName;

    private String expenseType;

    private String expenseModle;

    private BigDecimal amountPrice;

    private String level;

    private String creatTime;

    private String updateTime;

    private String valiFlag;

    private static final long serialVersionUID = 1L;


}