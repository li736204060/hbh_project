package com.lzx.hbh_system.bo.zffyxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@TableName("pay_info")
public class PayInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String householdCode;

    private String expanseCode;

    private Integer yearsBegin;

    private Integer yearsEnd;

    private BigDecimal payReduction;

    private BigDecimal amountReceivable;

    private BigDecimal paidInAmount;

    private BigDecimal lateFee;

    private String payStatus;

    private String payTime;

    private String crateTime;

    private String updateTime;

    private String valiFlag;

    private static final long serialVersionUID = 1L;

}