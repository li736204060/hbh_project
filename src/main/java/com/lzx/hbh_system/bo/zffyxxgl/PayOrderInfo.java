package com.lzx.hbh_system.bo.zffyxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@TableName("pay_order_info")
public class PayOrderInfo implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String id;

    private String householdCode;

    private String orderName;

    private String orderPayType;

    private BigDecimal orderPayAmount;

    private String orderTime;

    private String operaPerson;

    private String valiFlag;

    private static final long serialVersionUID = 1L;

}