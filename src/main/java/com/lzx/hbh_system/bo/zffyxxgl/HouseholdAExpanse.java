package com.lzx.hbh_system.bo.zffyxxgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("household_a_expanse")
public class HouseholdAExpanse implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)//自增给定主键
    private Integer id;

    private String houseHoldId;

    private String expanseId;

    private String valiFlag;

    private static final long serialVersionUID = 1L;

}