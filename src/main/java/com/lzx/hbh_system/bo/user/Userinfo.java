package com.lzx.hbh_system.bo.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName(value = "userinfo")
public class Userinfo implements Serializable {
    /**
     * 用户信息表主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String infoCode;
    /**
     * 数据唯一id
     */
    private String id;
    /**
     * 用户编号
     */
    private String userCode;
    /**
     * 用户姓名
     */
    private String name;
    /**
     * 用户性别
     */
    private String gender;
    /**
     * 用户来自
     */
    private String region;
    /**
     * 兴趣爱好
     */
    private String hobby;
    /**
     * 头像地址
     */
    private String avatar;
    /**
     * 头像文件对应的Gitee的仓库sha值 - 用于更新头像或者删除地址
     */
    private String avatarGiteeSha;
    /**
     * 头像文件对应仓库的文件名称的 - 用于获取和更新或者删除文件
     */
    private String avatarFileName;
    /**
     * 联系电话
     */
    private String phone;
    /**
     * 邮箱地址
     */
    private String email;
    /**
     * 个人备注
     */
    private String memo;
    /**
     * 数据创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date creatTime;
    /**
     * 数据更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date updateTime;
    /**
     * 有效标识
     */
    private String valiFlag;


}