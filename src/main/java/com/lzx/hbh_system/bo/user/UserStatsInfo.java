package com.lzx.hbh_system.bo.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName(value = "userstats_info")
public class UserStatsInfo implements Serializable {
    /**
     * 用户信息统计表主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String userstatsCode;
    /**
     * 数据唯一标识
     */
    private String id;
    /**
     * 操作记录名称
     */
    private String userCode;
    /**
     * 当天在线时长
     */
    private Integer inlineTime;
    /**
     * 统计时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date createTime;
    /**
     * 平均工作时间
     */
    private String averageWorkTime;
    /**
     * 有效标识
     */
    private String valiFlag;


}