package com.lzx.hbh_system.bo.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户操作-日志记录
 */
@Data
@TableName(value = "useropera_info")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UseroperaInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户操作信息表主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String operaCode;
    /**
     * 数据唯一标识
     */
    private String id;
    /**
     * 用户关联编号-
     */
    private String userCode;
    /**
     * 用户账户名称-
     */
    private String userName;
    /**
     * 操作记录名称
     */
    private String operaTitle;
    /**
     * 操作类型代号 （01 - 增 02 -删  03 -修改）
     */
    private String operaType;
    /**
     * 数据类型 1 操作类型 2 异常类型  0 所有类型
     */
    private Integer type;
    /**
     * 操作记录内容-描述
     */
    private String operaContent;
    /**
     * 类路径
     */
    private String classPath;
    /**
     * 请求方式
     */
    private String requestMethodType;
    /**
     * 执行Handle方法名称
     */
    private String requestMethodName;
    /**
     * 请求URL
     */
    private String requestPath;
    /**
     * 请求参数
     */
    private String requestParams;
    /**
     * 请求IP地址
     */
    private String requestIp;
    /**
     * 操作开始时间（记录开始时间）
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date operaTime;
    /**
     * 操作结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date operaEndTime;
    /**
     * 操作耗费时间
     */
    private Long consumingTime;
    /**
     * 操作结果
     */
    private String operaResult;
    /**
     * 操作记录错误信息--描述
     */
    private String errorMsg;
    /**
     * 异常详情信息 堆栈信息
     */
    private String exDetail;
    /**
     * 异常描述 e.getMessage
     */
    private String exDesc;
    /**
     * 数据创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date creatTime;
    /**
     * 数据更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss" )
    private Date updateTime;
    /**
     * 有效标识
     */
    private String valiFlag;



}