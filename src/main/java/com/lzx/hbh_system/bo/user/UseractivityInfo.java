package com.lzx.hbh_system.bo.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("useractivity_info")
public class UseractivityInfo implements Serializable {
    /**
     * 留言板信息表主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String activityCode;
    /**
     * 数据唯一标识
     */
    private String id;
    /**
     * 留言板-关联用户编号
     */
    private String activityUserCode;
    /**
     * 留言板内容
     */
    private String activityContent;
    /**
     * 留言板记录时间
     */
    private String createTime;
    /**
     * 留言板内容修改时间
     */
    private String updateTime;
    /**
     * 是否置顶标志 查询更具 该字段排序 数字越大越优先
     */
    private String firstFlag;
    /**
     * 不喜欢该条记录的点击数
     */
    private Integer dislikeCount;
    /**
     * 喜欢该条记录的点击数
     */
    private Integer likeCount;
    /**
     * 有效标识
     */
    private String valiFlag;
}
