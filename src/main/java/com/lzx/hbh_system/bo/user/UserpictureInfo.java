package com.lzx.hbh_system.bo.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("userpicture_info")
public class UserpictureInfo implements Serializable {
    /**
     * 用户图片信息表主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String pictureCode;
    /**
     * 数据唯一标识
     */
    private String id;
    /**
     * 关联用户编号
     */
    private String userCode;
    /**
     * 用户上传图片名称
     */
    private String pictureName;
    /**
     * 用户上传图片地址
     */
    private String pictureUrl;
    /**
     * 数据有效性
     */
    private String valiFlag;
}
