package com.lzx.hbh_system.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户信息表 存放所有用户信息
 */
@Data
@TableName("sys_user")
public class SysUser implements Serializable {
    /**
     * 用户编号-主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String userCode;
    /**
     * 数据唯一标识 雪花id
     */
    private String id;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户密码
     */
    private String userPwd;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 角色编号-用于个人信息展示等
     */
    private String roleCode;
    /**
     * 用户状态标识
     * 1 正常
     * 0 禁用-冻结
     * 01 欠费（账单未结）
     * 11 审核中
     * 00 注销-不再使用
     */
    private String status;

    /**
     * 注册时间
     */
    private String registTime;

    /**
     * 最近登陆时间
     */
    private String logintime;
    /**
     * 最近登陆IP地址：
     */
    private String ipAddress;


    /**
     * 有效标识
     * 1 有效
     * 0 无效
     */
    private String validFlag;

}