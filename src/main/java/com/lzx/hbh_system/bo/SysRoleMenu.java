package com.lzx.hbh_system.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "sys_role_menu")
public class SysRoleMenu {
    /**
     * 角色菜单关系主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String roleMenuCode;
    /**
     * 角色编号
     */
    private String roleCode;
    /**
     * 菜单编号
     */
    private String menuCode;
    /**
     * 有效标识
     */
    private String valiFlag;

}