package com.lzx.hbh_system.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户-角色 关联表
 */
@Data
@TableName("sys_user_role")
public class SysUserRole implements Serializable {
    /**
     * 用户角色编号 主键
     */
    @TableId(type = IdType.INPUT)//手动填充
    private String userToRoleCode;
    /**
     * 数据唯一标识
     */
    private String id;
    /**
     * 角色编号
     */
    private String roleCode;
    /**
     * 用户编号
     */
    private String userCode;


}