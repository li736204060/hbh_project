package com.lzx.hbh_system.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色表 存放所有角色信息
 */
@Data
@TableName("sys_role")
public class SysRole implements Serializable {
    /**
     * 角色编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String roleCode;
    /**
     * 数据唯一标识 雪花id
     */
    private String id;
    /**
     * 角色名称 -用于权限校验 例如：”user:create“
     */
    private String roleName;
    /**
     * 角色描述-用途等 例如：“用户创建”
     */
    private String roleDescription;
    /**
     * 父节点（上一级角色编号）例如 教师 - 某某教师
     */
    private String parentCode;
    /**
     * 有效标识
     * 1 有效
     * 0 无效
     */
    private String validFlag;


}