package com.lzx.hbh_system.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName(value = "sys_menu")
public class SysMenu implements Serializable {
    /**
     * 菜单主键
     */
    @TableId(type = IdType.INPUT)//手动给定主键
    private String menuCode;
    /**
     * 唯一标识
     */
    private String id;
    /**
     * 父菜单主键
     */
    private String parentCode;
    /**
     * 菜单页面名称
     */
    private String menuName;
    /**
     * 菜单展示名称
     */
    private String title;
    /**
     * 页面访问地址
     */
    private String url;
    /**
     * 菜单图标名称
     */
    private String icon;
    /**
     * 菜单层级 大的在外层
     */
    private Integer level;
    /**
     * 菜单排序序号 大的优先
     */
    private Integer sort;
    /**
     * 创建时间
     */
    private Date creatTime;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 菜单是否隐藏
     */
    private String hidden;
    /**
     * 是否有效标识
     */
    private String valiFlag;
    /**
     * 是否系统菜单
     */
    private String isSysFlag;


}