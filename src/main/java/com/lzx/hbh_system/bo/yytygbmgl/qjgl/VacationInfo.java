package com.lzx.hbh_system.bo.yytygbmgl.qjgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName("vacation_info")
public class VacationInfo implements Serializable {
    /**
     * 假期编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String vacateCode;

    private String id;

    private String empCode;

    private String vacateType; // 请假类型（1 - 其他特殊情况请假 2 - 病假 3 - 事假 4 - 丧假 5 - 产假 6 - 探亲假）

    private Date vacateBeginTime;

    private Date vacateEndTime;

    private String vacateResion;

    private String isApproveFlag;

    private String approverName;

    private String approveTime;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}