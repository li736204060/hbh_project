package com.lzx.hbh_system.bo.yytygbmgl.gzgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
@Data
@TableName("salary_info")
public class SalaryInfo implements Serializable {
    /**
     * 工资编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String salaryCode;

    private String id;

    private String empCode;

    private BigDecimal basicSalary;

    private BigDecimal allInsurance;

    private BigDecimal awardSalary;

    private BigDecimal finedSalary;

    private BigDecimal finalSalary;

    private String finedResion;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}