package com.lzx.hbh_system.bo.yytygbmgl.cqgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
@Data
@TableName("attendance_info")
public class AttendanceInfo implements Serializable {
    /**
     * 出勤编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String attendCode;

    private String id;

    private String empCode;

    private String attendRecodeTime;

    private Date attendBeginTime;

    private Date attendEndTime;

    private String memo;

    private String valiFlag;

    private static final long serialVersionUID = 1L;

}