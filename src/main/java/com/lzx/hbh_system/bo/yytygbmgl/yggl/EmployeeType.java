package com.lzx.hbh_system.bo.yytygbmgl.yggl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("employee_type")
public class EmployeeType implements Serializable {
    /**
     * 员工类型编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String employTypeCode;

    private String id;

    private String employTypeName;

    private String createTime;

    private String updateTime;

    private String memo;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}