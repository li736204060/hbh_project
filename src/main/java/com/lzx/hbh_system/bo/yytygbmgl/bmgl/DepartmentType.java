package com.lzx.hbh_system.bo.yytygbmgl.bmgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("department_type")
public class DepartmentType implements Serializable {
    /**
     * 部门类型编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String depTypeCode;

    private String id;

    private String depTypeName;

    private String creatTime;

    private String updateTime;

    private String memo;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}