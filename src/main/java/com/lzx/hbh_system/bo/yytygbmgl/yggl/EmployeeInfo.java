package com.lzx.hbh_system.bo.yytygbmgl.yggl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("employee_info")
public class EmployeeInfo implements Serializable {
    /**
     * 员工编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String employCode;

    private String id;

    private String employTypeCode;

    private String userCode;

    private String depCode;

    private String employVacateCode;

    private String salaryCode;

    private String attendCode;

    private Integer employAge;

    private Integer employGender;

    private String employEntryTime;

    private String employDepartureTime;

    private String employDepartureResion;

    private String employStatus;

    private String employWorkType;

    private String employWorkStatus;

    private String createTime;

    private String updateTime;

    private String memo;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}