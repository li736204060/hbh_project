package com.lzx.hbh_system.bo.yytygbmgl.bmgl;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
@Data
@TableName("department_info")
public class DepartmentInfo implements Serializable {
    /**
     * 部门编号-主键
     */
    @TableId(type = IdType.INPUT)//手动填
    private String depCode;

    private String id;

    private String depTypeCode;

    private String depName;

    private String depAddress;

    private String memo;

    private String valiFlag;

    private static final long serialVersionUID = 1L;
}